--------------------------------------------------------------------------------
--  Sogla                                                                     --
--                                                                            --
--  sogla-opengl.adb                                                          --
--                                                                            --
--  Contributors:                                                             --
--      Tetsumi <tetsumi@vmail.me>                                              --
--                                                                            --
--                                                                            --
--  Copyright © 2013 Tetsumi                                                   --
--                                                                            --
--  Licensed under the Apache License, Version 2.0 (the "License"); you may   --
--  not use this file except in compliance with the License. You may obtain   --
--  a copy of the License at                                                  --
--                                                                            --
--  http://www.apache.org/licenses/LICENSE-2.0.html                           --
--  http://www.apache.org/licenses/LICENSE-2.0.txt                            --
--                                                                            --
--  Unless required by applicable law or agreed to in writing, software       --
--  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT --
--  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the  --
--  License for the specific language governing permissions and limitations   --
--  under the License.                                                        --
--                                                                            --
--------------------------------------------------------------------------------

with 
	Ada.Unchecked_Conversion,
	System.Address_Image,
	Interfaces.C,
	Interfaces.C.Extensions,
	Interfaces.C.Strings,
	Sogla.Glfw,
	Sogla.Log_IO;

use 
	Sogla.Glfw;

package body Sogla.OpenGL is
	----------------
	-- Initialize --
	----------------
	function Initialize return Boolean
	is
		use Interfaces.C;
		use Interfaces.C.Strings;
		use System;

		Ext_Index : GLuint := 0;
		Number_of_extensions : aliased GLint;
		Gl_Boolean : aliased GLboolean;

		procedure Set_Address (Virtual_Func  : System.Address;
							   Target_Name   : String)
		is
			c_string        : chars_ptr := New_String(Target_Name);
			func_address    : System.Address := glfwGetProcAddress(c_string);
			address_gate    : System.Address;
			for address_gate'Address use Virtual_Func;
		begin
			address_gate := func_address;

			Interfaces.C.Strings.Free(c_string);
			Sogla.Log_IO.Put("      " & Target_Name & ": ");

			if (func_address /= System.Null_Address) then
				Sogla.Log_IO.Put("0x" & System.Address_Image(func_address));
			else
				Sogla.Log_IO.Put("Not Found !!!");
			end if;

			Sogla.Log_IO.New_Line;
		end Set_Address;
	begin

		if (Sogla.Log_IO.Is_Initialized = False) then
			return false;
		end if;

		glGetBooleanv(GL_DOUBLEBUFFER, Gl_Boolean'Access);
		glGetIntegerv(GL_NUM_EXTENSIONS, Number_of_extensions'Access);
		glGetIntegerv(GL_MAJOR_VERSION, Version_Major'Access);
		glGetIntegerv(GL_MINOR_VERSION, Version_Minor'Access);

		Sogla.Log_IO.Put_Line("--- OpenGL Informations:");
		Sogla.Log_IO.Put_Line("      Vendor: " & Get_GL_String(GL_VENDOR));
		Sogla.Log_IO.Put_Line("      Renderer: " & Get_GL_String(GL_RENDERER));
		Sogla.Log_IO.Put_Line("      Version: " & Get_GL_String(GL_VERSION));
		Sogla.Log_IO.Put_Line("      GLSL Version: " & Get_GL_String(GL_SHADING_LANGUAGE_VERSION));
		Sogla.Log_IO.Put_Line("      Extensions: " & Number_of_extensions'Img);

		Sogla.Log_IO.Put("      Double Buffer: ");

		if (Gl_Boolean = GL_TRUE) then
			Sogla.Log_IO.Put_Line("Yes");
		else
			Sogla.Log_IO.Put_Line("No");
		end if;

		Sogla.Log_IO.New_Line;
		Sogla.Log_IO.Put_Line("--- Loading OpenGL Functions:");
		Set_Address(glBlendColor'Address,               "glBlendColor");
		Set_Address(glBlendEquation'Address,            "glBlendEquation");
		Set_Address(glDrawRangeElements'Address,        "glDrawRangeElements");
		Set_Address(glTexImage3D'Address,               "glTexImage3D");
		Set_Address(glTexSubImage3D'Address,            "glTexSubImage3D");
		Set_Address(glCopyTexSubImage3D'Address,        "glCopyTexSubImage3D");
		Set_Address(glActiveTexture'Address,            "glActiveTexture");
		Set_Address(glSampleCoverage'Address,           "glSampleCoverage");
		Set_Address(glCompressedTexImage3D'Address,     "glCompressedTexImage3D");
		Set_Address(glCompressedTexImage2D'Address,     "glCompressedTexImage2D");
		Set_Address(glCompressedTexImage1D'Address,     "glCompressedTexImage1D");
		Set_Address(glCompressedTexSubImage3D'Address,  "glCompressedTexSubImage3D");
		Set_Address(glCompressedTexSubImage2D'Address,  "glCompressedTexSubImage2D");
		Set_Address(glCompressedTexSubImage1D'Address,  "glCompressedTexSubImage1D");
		Set_Address(glGetCompressedTexImage'Address,    "glGetCompressedTexImage");
		Set_Address(glBlendFuncSeparate'Address,        "glBlendFuncSeparate");
		Set_Address(glMultiDrawArrays'Address,          "glMultiDrawArrays");
		Set_Address(glMultiDrawElements'Address,        "glMultiDrawElements");
		Set_Address(glPointParameterf'Address,          "glPointParameterf");
		Set_Address(glPointParameterfv'Address,         "glPointParameterfv");
		Set_Address(glPointParameteri'Address,          "glPointParameteri");
		Set_Address(glPointParameteriv'Address,         "glPointParameteriv");
		Set_Address(glGenQueries'Address,               "glGenQueries");
		Set_Address(glDeleteQueries'Address,            "glDeleteQueries");
		Set_Address(glIsQuery'Address,                  "glIsQuery");
		Set_Address(glBeginQuery'Address,               "glBeginQuery");
		Set_Address(glEndQuery'Address,                 "glEndQuery");
		Set_Address(glGetQueryiv'Address,               "glGetQueryiv");
		Set_Address(glGetQueryObjectiv'Address,         "glGetQueryObjectiv");
		Set_Address(glGetQueryObjectuiv'Address,        "glGetQueryObjectuiv");
		Set_Address(glBindBuffer'Address,               "glBindBuffer");
		Set_Address(glDeleteBuffers'Address,            "glDeleteBuffers");
		Set_Address(glGenBuffers'Address,               "glGenBuffers");
		Set_Address(glIsBuffer'Address,                 "glIsBuffer");
		Set_Address(glBufferData'Address,               "glBufferData");
		Set_Address(glBufferSubData'Address,            "glBufferSubData");
		Set_Address(glGetBufferSubData'Address,         "glGetBufferSubData");
		Set_Address(glMapBuffer'Address,                "glMapBuffer");
		Set_Address(glUnmapBuffer'Address,              "glUnmapBuffer");
		Set_Address(glGetBufferParameteriv'Address,     "glGetBufferParameteriv");
		Set_Address(glGetBufferPointerv'Address,        "glGetBufferPointerv");
		Set_Address(glBlendEquationSeparate'Address,    "glBlendEquationSeparate");
		Set_Address(glDrawBuffers'Address,              "glDrawBuffers");
		Set_Address(glStencilOpSeparate'Address,        "glStencilOpSeparate");
		Set_Address(glStencilFuncSeparate'Address,      "glStencilFuncSeparate");
		Set_Address(glStencilMaskSeparate'Address,      "glStencilMaskSeparate");
		Set_Address(glAttachShader'Address,             "glAttachShader");
		Set_Address(glBindAttribLocation'Address,       "glBindAttribLocation");
		Set_Address(glCompileShader'Address,            "glCompileShader");
		Set_Address(glCreateProgram'Address,            "glCreateProgram");
		Set_Address(glCreateShader'Address,             "glCreateShader");
		Set_Address(glDeleteProgram'Address,            "glDeleteProgram");
		Set_Address(glDeleteShader'Address,             "glDeleteShader");
		Set_Address(glDetachShader'Address,             "glDetachShader");
		Set_Address(glDisableVertexAttribArray'Address, "glDisableVertexAttribArray");
		Set_Address(glEnableVertexAttribArray'Address,  "glEnableVertexAttribArray");
		Set_Address(glGetActiveAttrib'Address,          "glGetActiveAttrib");
		Set_Address(glGetActiveUniform'Address,         "glGetActiveUniform");
		Set_Address(glGetAttachedShaders'Address,       "glGetAttachedShaders");
		Set_Address(glGetAttribLocation'Address,        "glGetAttribLocation");
		Set_Address(glGetProgramiv'Address,             "glGetProgramiv");
		Set_Address(glGetProgramInfoLog'Address,        "glGetProgramInfoLog");
		Set_Address(glGetShaderiv'Address,              "glGetShaderiv");
		Set_Address(glGetShaderInfoLog'Address,         "glGetShaderInfoLog");
		Set_Address(glGetShaderSource'Address,          "glGetShaderSource");
		Set_Address(glGetUniformLocation'Address,       "glGetUniformLocation");
		Set_Address(glGetUniformfv'Address,             "glGetUniformfv");
		Set_Address(glGetUniformiv'Address,             "glGetUniformiv");
		Set_Address(glGetVertexAttribdv'Address,        "glGetVertexAttribdv");
		Set_Address(glGetVertexAttribfv'Address,        "glGetVertexAttribfv");
		Set_Address(glGetVertexAttribiv'Address,        "glGetVertexAttribiv");
		Set_Address(glGetVertexAttribPointerv'Address,  "glGetVertexAttribPointerv");
		Set_Address(glIsProgram'Address,                "glIsProgram");
		Set_Address(glIsShader'Address,                 "glIsShader");
		Set_Address(glLinkProgram'Address,              "glLinkProgram");
		Set_Address(glShaderSource'Address,             "glShaderSource");
		Set_Address(glUseProgram'Address,               "glUseProgram");
		Set_Address(glUniform1f'Address,                "glUniform1f");
		Set_Address(glUniform2f'Address,                "glUniform2f");
		Set_Address(glUniform3f'Address,                "glUniform3f");
		Set_Address(glUniform4f'Address,                "glUniform4f");
		Set_Address(glUniform1i'Address,                "glUniform1i");
		Set_Address(glUniform2i'Address,                "glUniform2i");
		Set_Address(glUniform3i'Address,                "glUniform3i");
		Set_Address(glUniform4i'Address,                "glUniform4i");
		Set_Address(glUniform1fv'Address,               "glUniform1fv");
		Set_Address(glUniform2fv'Address,               "glUniform2fv");
		Set_Address(glUniform3fv'Address,               "glUniform3fv");
		Set_Address(glUniform4fv'Address,               "glUniform4fv");
		Set_Address(glUniform1iv'Address,               "glUniform1iv");
		Set_Address(glUniform2iv'Address,               "glUniform2iv");
		Set_Address(glUniform3iv'Address,               "glUniform3iv");
		Set_Address(glUniform4iv'Address,               "glUniform4iv");
		Set_Address(glUniformMatrix2fv'Address,         "glUniformMatrix2fv");
		Set_Address(glUniformMatrix3fv'Address,         "glUniformMatrix3fv");
		Set_Address(glUniformMatrix4fv'Address,         "glUniformMatrix4fv");
		Set_Address(glValidateProgram'Address,          "glValidateProgram");
		Set_Address(glVertexAttrib1d'Address,           "glVertexAttrib1d");
		Set_Address(glVertexAttrib1dv'Address,          "glVertexAttrib1dv");
		Set_Address(glVertexAttrib1f'Address,           "glVertexAttrib1f");
		Set_Address(glVertexAttrib1fv'Address,          "glVertexAttrib1fv");
		Set_Address(glVertexAttrib1s'Address,           "glVertexAttrib1s");
		Set_Address(glVertexAttrib1sv'Address,          "glVertexAttrib1sv");
		Set_Address(glVertexAttrib2d'Address,           "glVertexAttrib2d");
		Set_Address(glVertexAttrib2dv'Address,          "glVertexAttrib2dv");
		Set_Address(glVertexAttrib2f'Address,           "glVertexAttrib2f");
		Set_Address(glVertexAttrib2fv'Address,          "glVertexAttrib2fv");
		Set_Address(glVertexAttrib2s'Address,           "glVertexAttrib2s");
		Set_Address(glVertexAttrib2sv'Address,          "glVertexAttrib2sv");
		Set_Address(glVertexAttrib3d'Address,           "glVertexAttrib3d");
		Set_Address(glVertexAttrib3dv'Address,          "glVertexAttrib3dv");
		Set_Address(glVertexAttrib3f'Address,           "glVertexAttrib3f");
		Set_Address(glVertexAttrib3fv'Address,          "glVertexAttrib3fv");
		Set_Address(glVertexAttrib3s'Address,           "glVertexAttrib3s");
		Set_Address(glVertexAttrib3sv'Address,          "glVertexAttrib3sv");
		Set_Address(glVertexAttrib4Nbv'Address,         "glVertexAttrib4Nbv");
		Set_Address(glVertexAttrib4Niv'Address,         "glVertexAttrib4Niv");
		Set_Address(glVertexAttrib4Nsv'Address,         "glVertexAttrib4Nsv");
		Set_Address(glVertexAttrib4Nub'Address,         "glVertexAttrib4Nub");
		Set_Address(glVertexAttrib4Nubv'Address,        "glVertexAttrib4Nubv");
		Set_Address(glVertexAttrib4Nuiv'Address,        "glVertexAttrib4Nuiv");
		Set_Address(glVertexAttrib4Nusv'Address,        "glVertexAttrib4Nusv");
		Set_Address(glVertexAttrib4bv'Address,          "glVertexAttrib4bv");
		Set_Address(glVertexAttrib4d'Address,           "glVertexAttrib4d");
		Set_Address(glVertexAttrib4dv'Address,          "glVertexAttrib4dv");
		Set_Address(glVertexAttrib4f'Address,           "glVertexAttrib4f");
		Set_Address(glVertexAttrib4fv'Address,          "glVertexAttrib4fv");
		Set_Address(glVertexAttrib4iv'Address,          "glVertexAttrib4iv");
		Set_Address(glVertexAttrib4s'Address,           "glVertexAttrib4s");
		Set_Address(glVertexAttrib4sv'Address,          "glVertexAttrib4sv");
		Set_Address(glVertexAttrib4ubv'Address,         "glVertexAttrib4ubv");
		Set_Address(glVertexAttrib4uiv'Address,         "glVertexAttrib4uiv");
		Set_Address(glVertexAttrib4usv'Address,         "glVertexAttrib4usv");
		Set_Address(glVertexAttribPointer'Address,      "glVertexAttribPointer");
		Set_Address(glUniformMatrix2x3fv'Address,       "glUniformMatrix2x3fv");
		Set_Address(glUniformMatrix3x2fv'Address,       "glUniformMatrix3x2fv");
		Set_Address(glUniformMatrix2x4fv'Address,       "glUniformMatrix2x4fv");
		Set_Address(glUniformMatrix4x2fv'Address,       "glUniformMatrix4x2fv");
		Set_Address(glUniformMatrix3x4fv'Address,       "glUniformMatrix3x4fv");
		Set_Address(glUniformMatrix4x3fv'Address,       "glUniformMatrix4x3fv");
		Set_Address(glColorMaski'Address,               "glColorMaski");
		Set_Address(glGetBooleani_v'Address,            "glGetBooleani_v");
		Set_Address(glGetIntegeri_v'Address,            "glGetIntegeri_v");
		Set_Address(glEnablei'Address,                  "glEnablei");
		Set_Address(glDisablei'Address,                 "glDisablei");
		Set_Address(glIsEnabledi'Address,               "glIsEnabledi");
		Set_Address(glBeginTransformFeedback'Address,   "glBeginTransformFeedback");
		Set_Address(glEndTransformFeedback'Address,     "glEndTransformFeedback");
		Set_Address(glBindBufferRange'Address,          "glBindBufferRange");
		Set_Address(glBindBufferBase'Address,           "glBindBufferBase");
		Set_Address(glTransformFeedbackVaryings'Address,   "glTransformFeedbackVaryings");
		Set_Address(glGetTransformFeedbackVarying'Address, "glGetTransformFeedbackVarying");
		Set_Address(glClampColor'Address,               "glClampColor");
		Set_Address(glBeginConditionalRender'Address,   "glBeginConditionalRender");
		Set_Address(glEndConditionalRender'Address,     "glEndConditionalRender");
		Set_Address(glVertexAttribIPointer'Address,     "glVertexAttribIPointer");
		Set_Address(glGetVertexAttribIiv'Address,       "glGetVertexAttribIiv");
		Set_Address(glGetVertexAttribIuiv'Address,      "glGetVertexAttribIuiv");
		Set_Address(glVertexAttribI1i'Address,          "glVertexAttribI1i");
		Set_Address(glVertexAttribI2i'Address,          "glVertexAttribI2i");
		Set_Address(glVertexAttribI3i'Address,          "glVertexAttribI3i");
		Set_Address(glVertexAttribI4i'Address,          "glVertexAttribI4i");
		Set_Address(glVertexAttribI1ui'Address,         "glVertexAttribI1ui");
		Set_Address(glVertexAttribI2ui'Address,         "glVertexAttribI2ui");
		Set_Address(glVertexAttribI3ui'Address,         "glVertexAttribI3ui");
		Set_Address(glVertexAttribI4ui'Address,         "glVertexAttribI4ui");
		Set_Address(glVertexAttribI1iv'Address,         "glVertexAttribI1iv");
		Set_Address(glVertexAttribI2iv'Address,         "glVertexAttribI2iv");
		Set_Address(glVertexAttribI3iv'Address,         "glVertexAttribI3iv");
		Set_Address(glVertexAttribI4iv'Address,         "glVertexAttribI4iv");
		Set_Address(glVertexAttribI1uiv'Address,        "glVertexAttribI1uiv");
		Set_Address(glVertexAttribI2uiv'Address,        "glVertexAttribI2uiv");
		Set_Address(glVertexAttribI3uiv'Address,        "glVertexAttribI3uiv");
		Set_Address(glVertexAttribI4uiv'Address,        "glVertexAttribI4uiv");
		Set_Address(glVertexAttribI4bv'Address,         "glVertexAttribI4bv");
		Set_Address(glVertexAttribI4sv'Address,         "glVertexAttribI4sv");
		Set_Address(glVertexAttribI4ubv'Address,        "glVertexAttribI4ubv");
		Set_Address(glVertexAttribI4usv'Address,        "glVertexAttribI4usv");
		Set_Address(glGetUniformuiv'Address,            "glGetUniformuiv");
		Set_Address(glBindFragDataLocation'Address,     "glBindFragDataLocation");
		Set_Address(glGetFragDataLocation'Address,      "glGetFragDataLocation");
		Set_Address(glUniform1ui'Address,               "glUniform1ui");
		Set_Address(glUniform2ui'Address,               "glUniform2ui");
		Set_Address(glUniform3ui'Address,               "glUniform3ui");
		Set_Address(glUniform4ui'Address,               "glUniform4ui");
		Set_Address(glUniform1uiv'Address,              "glUniform1uiv");
		Set_Address(glUniform2uiv'Address,              "glUniform2uiv");
		Set_Address(glUniform3uiv'Address,              "glUniform3uiv");
		Set_Address(glUniform4uiv'Address,              "glUniform4uiv");
		Set_Address(glTexParameterIiv'Address,          "glTexParameterIiv");
		Set_Address(glTexParameterIuiv'Address,         "glTexParameterIuiv");
		Set_Address(glGetTexParameterIiv'Address,       "glGetTexParameterIiv");
		Set_Address(glGetTexParameterIuiv'Address,      "glGetTexParameterIuiv");
		Set_Address(glClearBufferiv'Address,            "glClearBufferiv");
		Set_Address(glClearBufferuiv'Address,           "glClearBufferuiv");
		Set_Address(glClearBufferfv'Address,            "glClearBufferfv");
		Set_Address(glClearBufferfi'Address,            "glClearBufferfi");
		Set_Address(glGetStringi'Address,               "glGetStringi");
		Set_Address(glDrawArraysInstanced'Address,      "glDrawArraysInstanced");
		Set_Address(glDrawElementsInstanced'Address,    "glDrawElementsInstanced");
		Set_Address(glTexBuffer'Address,                "glTexBuffer");
		Set_Address(glPrimitiveRestartIndex'Address,    "glPrimitiveRestartIndex");
		Set_Address(glGetInteger64i_v'Address,          "glGetInteger64i_v");
		Set_Address(glGetBufferParameteri64v'Address,   "glGetBufferParameteri64v");
		Set_Address(glFramebufferTexture'Address,       "glFramebufferTexture");
		Set_Address(glVertexAttribDivisor'Address,      "glVertexAttribDivisor");
		Set_Address(glMinSampleShading'Address,         "glMinSampleShading");
		Set_Address(glBlendEquationi'Address,           "glBlendEquationi");
		Set_Address(glBlendEquationSeparatei'Address,   "glBlendEquationSeparatei");
		Set_Address(glBlendFunci'Address,               "glBlendFunci");
		Set_Address(glBlendFuncSeparatei'Address,       "glBlendFuncSeparatei");
		Set_Address(glIsRenderbuffer'Address,           "glIsRenderbuffer");
		Set_Address(glBindRenderbuffer'Address,         "glBindRenderbuffer");
		Set_Address(glDeleteRenderbuffers'Address,      "glDeleteRenderbuffers");
		Set_Address(glGenRenderbuffers'Address,         "glGenRenderbuffers");
		Set_Address(glRenderbufferStorage'Address,      "glRenderbufferStorage");
		Set_Address(glGetRenderbufferParameteriv'Address, "glGetRenderbufferParameteriv");
		Set_Address(glIsFramebuffer'Address,            "glIsFramebuffer");
		Set_Address(glBindFramebuffer'Address,          "glBindFramebuffer");
		Set_Address(glDeleteFramebuffers'Address,       "glDeleteFramebuffers");
		Set_Address(glGenFramebuffers'Address,          "glGenFramebuffers");
		Set_Address(glCheckFramebufferStatus'Address,   "glCheckFramebufferStatus");
		Set_Address(glFramebufferTexture1D'Address,     "glFramebufferTexture1D");
		Set_Address(glFramebufferTexture2D'Address,     "glFramebufferTexture2D");
		Set_Address(glFramebufferTexture3D'Address,     "glFramebufferTexture3D");
		Set_Address(glFramebufferRenderbuffer'Address,  "glFramebufferRenderbuffer");
		Set_Address(glGetFramebufferAttachmentParameteriv'Address, "glGetFramebufferAttachmentParameteriv");
		Set_Address(glGenerateMipmap'Address,           "glGenerateMipmap");
		Set_Address(glBlitFramebuffer'Address,          "glBlitFramebuffer");
		Set_Address(glRenderbufferStorageMultisample'Address, "glRenderbufferStorageMultisample");
		Set_Address(glFramebufferTextureLayer'Address,  "glFramebufferTextureLayer");
		Set_Address(glMapBufferRange'Address,           "glMapBufferRange");
		Set_Address(glFlushMappedBufferRange'Address,   "glFlushMappedBufferRange");
		Set_Address(glBindVertexArray'Address,          "glBindVertexArray");
		Set_Address(glDeleteVertexArrays'Address,       "glDeleteVertexArrays");
		Set_Address(glGenVertexArrays'Address,          "glGenVertexArrays");
		Set_Address(glIsVertexArray'Address,            "glIsVertexArray");
		Set_Address(glGetUniformIndices'Address,        "glGetUniformIndices");
		Set_Address(glGetActiveUniformsiv'Address,      "glGetActiveUniformsiv");
		Set_Address(glGetActiveUniformName'Address,     "glGetActiveUniformName");
		Set_Address(glGetUniformBlockIndex'Address,     "glGetUniformBlockIndex");
		Set_Address(glGetActiveUniformBlockiv'Address,  "glGetActiveUniformBlockiv");
		Set_Address(glGetActiveUniformBlockName'Address,"glGetActiveUniformBlockName");
		Set_Address(glUniformBlockBinding'Address,      "glUniformBlockBinding");
		Set_Address(glCopyBufferSubData'Address,        "glCopyBufferSubData");
		Set_Address(glDrawElementsBaseVertex'Address,   "glDrawElementsBaseVertex");
		Set_Address(glDrawRangeElementsBaseVertex'Address, "glDrawRangeElementsBaseVertex");
		Set_Address(glDrawElementsInstancedBaseVertex'Address, "glDrawElementsInstancedBaseVertex");
		Set_Address(glMultiDrawElementsBaseVertex'Address, "glMultiDrawElementsBaseVertex");
		Set_Address(glProvokingVertex'Address,          "glProvokingVertex");
		Set_Address(glFenceSync'Address,                "glFenceSync");
		Set_Address(glIsSync'Address,                   "glIsSync");
		Set_Address(glDeleteSync'Address,               "glDeleteSync");
		Set_Address(glClientWaitSync'Address,           "glClientWaitSync");
		Set_Address(glWaitSync'Address,                 "glWaitSync");
		Set_Address(glGetInteger64v'Address,            "glGetInteger64v");
		Set_Address(glGetSynciv'Address,                "glGetSynciv");
		Set_Address(glTexImage2DMultisample'Address,    "glTexImage2DMultisample");
		Set_Address(glTexImage3DMultisample'Address,    "glTexImage3DMultisample");
		Set_Address(glGetMultisamplefv'Address,         "glGetMultisamplefv");
		Set_Address(glSampleMaski'Address,              "glSampleMaski");
		Set_Address(glNamedStringARB'Address,           "glNamedStringARB");
		Set_Address(glDeleteNamedStringARB'Address,     "glDeleteNamedStringARB");
		Set_Address(glCompileShaderIncludeARB'Address,  "glCompileShaderIncludeARB");
		Set_Address(glIsNamedStringARB'Address,         "glIsNamedStringARB");
		Set_Address(glGetNamedStringARB'Address,        "glGetNamedStringARB");
		Set_Address(glGetNamedStringivARB'Address,      "glGetNamedStringivARB");
		Set_Address(glBindFragDataLocationIndexed'Address, "glBindFragDataLocationIndexed");
		Set_Address(glGetFragDataIndex'Address,         "glGetFragDataIndex");
		Set_Address(glGenSamplers'Address,              "glGenSamplers");
		Set_Address(glDeleteSamplers'Address,           "glDeleteSamplers");
		Set_Address(glIsSampler'Address,                "glIsSampler");
		Set_Address(glBindSampler'Address,              "glBindSampler");
		Set_Address(glSamplerParameteri'Address,        "glSamplerParameteri");
		Set_Address(glSamplerParameteriv'Address,       "glSamplerParameteriv");
		Set_Address(glSamplerParameterf'Address,        "glSamplerParameterf");
		Set_Address(glSamplerParameterfv'Address,       "glSamplerParameterfv");
		Set_Address(glSamplerParameterIiv'Address,      "glSamplerParameterIiv");
		Set_Address(glSamplerParameterIuiv'Address,     "glSamplerParameterIuiv");
		Set_Address(glGetSamplerParameteriv'Address,    "glGetSamplerParameteriv");
		Set_Address(glGetSamplerParameterIiv'Address,   "glGetSamplerParameterIiv");
		Set_Address(glGetSamplerParameterfv'Address,    "glGetSamplerParameterfv");
		Set_Address(glGetSamplerParameterIuiv'Address,  "glGetSamplerParameterIuiv");
		Set_Address(glQueryCounter'Address,             "glQueryCounter");
		Set_Address(glGetQueryObjecti64v'Address,       "glGetQueryObjecti64v");
		Set_Address(glGetQueryObjectui64v'Address,      "glGetQueryObjectui64v");
		Set_Address(glVertexP2ui'Address,               "glVertexP2ui");
		Set_Address(glVertexP2uiv'Address,              "glVertexP2uiv");
		Set_Address(glVertexP3ui'Address,               "glVertexP3ui");
		Set_Address(glVertexP3uiv'Address,              "glVertexP3uiv");
		Set_Address(glVertexP4ui'Address,               "glVertexP4ui");
		Set_Address(glVertexP4uiv'Address,              "glVertexP4uiv");
		Set_Address(glTexCoordP1ui'Address,             "glTexCoordP1ui");
		Set_Address(glTexCoordP1uiv'Address,            "glTexCoordP1uiv");
		Set_Address(glTexCoordP2ui'Address,             "glTexCoordP2ui");
		Set_Address(glTexCoordP2uiv'Address,            "glTexCoordP2uiv");
		Set_Address(glTexCoordP3ui'Address,             "glTexCoordP3ui");
		Set_Address(glTexCoordP3uiv'Address,            "glTexCoordP3uiv");
		Set_Address(glTexCoordP4ui'Address,             "glTexCoordP4ui");
		Set_Address(glTexCoordP4uiv'Address,            "glTexCoordP4uiv");
		Set_Address(glMultiTexCoordP1ui'Address,        "glMultiTexCoordP1ui");
		Set_Address(glMultiTexCoordP1uiv'Address,       "glMultiTexCoordP1uiv");
		Set_Address(glMultiTexCoordP2ui'Address,        "glMultiTexCoordP2ui");
		Set_Address(glMultiTexCoordP2uiv'Address,       "glMultiTexCoordP2uiv");
		Set_Address(glMultiTexCoordP3ui'Address,        "glMultiTexCoordP3ui");
		Set_Address(glMultiTexCoordP3uiv'Address,       "glMultiTexCoordP3uiv");
		Set_Address(glMultiTexCoordP4ui'Address,        "glMultiTexCoordP4ui");
		Set_Address(glMultiTexCoordP4uiv'Address,       "glMultiTexCoordP4uiv");
		Set_Address(glNormalP3ui'Address,               "glNormalP3ui");
		Set_Address(glNormalP3uiv'Address,              "glNormalP3uiv");
		Set_Address(glColorP3ui'Address,                "glColorP3ui");
		Set_Address(glColorP3uiv'Address,               "glColorP3uiv");
		Set_Address(glColorP4ui'Address,                "glColorP4ui");
		Set_Address(glColorP4uiv'Address,               "glColorP4uiv");
		Set_Address(glSecondaryColorP3ui'Address,       "glSecondaryColorP3ui");
		Set_Address(glSecondaryColorP3uiv'Address,      "glSecondaryColorP3uiv");
		Set_Address(glVertexAttribP1ui'Address,         "glVertexAttribP1ui");
		Set_Address(glVertexAttribP1uiv'Address,        "glVertexAttribP1uiv");
		Set_Address(glVertexAttribP2ui'Address,         "glVertexAttribP2ui");
		Set_Address(glVertexAttribP2uiv'Address,        "glVertexAttribP2uiv");
		Set_Address(glVertexAttribP3ui'Address,         "glVertexAttribP3ui");
		Set_Address(glVertexAttribP3uiv'Address,        "glVertexAttribP3uiv");
		Set_Address(glVertexAttribP4ui'Address,         "glVertexAttribP4ui");
		Set_Address(glVertexAttribP4uiv'Address,        "glVertexAttribP4uiv");
		Set_Address(glDrawArraysIndirect'Address,       "glDrawArraysIndirect");
		Set_Address(glDrawElementsIndirect'Address,     "glDrawElementsIndirect");
		Set_Address(glUniform1d'Address,                "glUniform1d");
		Set_Address(glUniform2d'Address,                "glUniform2d");
		Set_Address(glUniform3d'Address,                "glUniform3d");
		Set_Address(glUniform4d'Address,                "glUniform4d");
		Set_Address(glUniform1dv'Address,               "glUniform1dv");
		Set_Address(glUniform2dv'Address,               "glUniform2dv");
		Set_Address(glUniform3dv'Address,               "glUniform3dv");
		Set_Address(glUniform4dv'Address,               "glUniform4dv");
		Set_Address(glUniformMatrix2dv'Address,         "glUniformMatrix2dv");
		Set_Address(glUniformMatrix3dv'Address,         "glUniformMatrix3dv");
		Set_Address(glUniformMatrix4dv'Address,         "glUniformMatrix4dv");
		Set_Address(glUniformMatrix2x3dv'Address,       "glUniformMatrix2x3dv");
		Set_Address(glUniformMatrix2x4dv'Address,       "glUniformMatrix2x4dv");
		Set_Address(glUniformMatrix3x2dv'Address,       "glUniformMatrix3x2dv");
		Set_Address(glUniformMatrix3x4dv'Address,       "glUniformMatrix3x4dv");
		Set_Address(glUniformMatrix4x2dv'Address,       "glUniformMatrix4x2dv");
		Set_Address(glUniformMatrix4x3dv'Address,       "glUniformMatrix4x3dv");
		Set_Address(glGetUniformdv'Address,             "glGetUniformdv");
		Set_Address(glGetSubroutineUniformLocation'Address, "glGetSubroutineUniformLocation");
		Set_Address(glGetSubroutineIndex'Address,       "glGetSubroutineIndex");
		Set_Address(glGetActiveSubroutineUniformiv'Address, "glGetActiveSubroutineUniformiv");
		Set_Address(glGetActiveSubroutineUniformName'Address, "glGetActiveSubroutineUniformName");
		Set_Address(glGetActiveSubroutineName'Address,  "glGetActiveSubroutineName");
		Set_Address(glUniformSubroutinesuiv'Address,    "glUniformSubroutinesuiv");
		Set_Address(glGetUniformSubroutineuiv'Address,  "glGetUniformSubroutineuiv");
		Set_Address(glGetProgramStageiv'Address,        "glGetProgramStageiv");
		Set_Address(glPatchParameteri'Address,          "glPatchParameteri");
		Set_Address(glPatchParameterfv'Address,         "glPatchParameterfv");
		Set_Address(glBindTransformFeedback'Address,    "glBindTransformFeedback");
		Set_Address(glDeleteTransformFeedbacks'Address, "glDeleteTransformFeedbacks");
		Set_Address(glGenTransformFeedbacks'Address,    "glGenTransformFeedbacks");
		Set_Address(glIsTransformFeedback'Address,      "glIsTransformFeedback");
		Set_Address(glPauseTransformFeedback'Address,   "glPauseTransformFeedback");
		Set_Address(glResumeTransformFeedback'Address,  "glResumeTransformFeedback");
		Set_Address(glDrawTransformFeedback'Address,    "glDrawTransformFeedback");
		Set_Address(glBindTransformFeedback'Address,    "glBindTransformFeedback");
		Set_Address(glDeleteTransformFeedbacks'Address, "glDeleteTransformFeedbacks");
		Set_Address(glGenTransformFeedbacks'Address,    "glGenTransformFeedbacks");
		Set_Address(glIsTransformFeedback'Address,      "glIsTransformFeedback");
		Set_Address(glReleaseShaderCompiler'Address,    "glReleaseShaderCompiler");
		Set_Address(glShaderBinary'Address,             "glShaderBinary");
		Set_Address(glGetShaderPrecisionFormat'Address, "glGetShaderPrecisionFormat");
		Set_Address(glDepthRangef'Address,              "glDepthRangef");
		Set_Address(glClearDepthf'Address,              "glClearDepthf");
		Set_Address(glGetProgramBinary'Address,         "glGetProgramBinary");
		Set_Address(glProgramBinary'Address,            "glProgramBinary");
		Set_Address(glProgramParameteri'Address,        "glProgramParameteri");
		Set_Address(glUseProgramStages'Address,         "glUseProgramStages");
		Set_Address(glActiveShaderProgram'Address,      "glActiveShaderProgram");
		Set_Address(glCreateShaderProgramv'Address,     "glCreateShaderProgramv");
		Set_Address(glBindProgramPipeline'Address,      "glBindProgramPipeline");
		Set_Address(glDeleteProgramPipelines'Address,   "glDeleteProgramPipelines");
		Set_Address(glGenProgramPipelines'Address,      "glGenProgramPipelines");
		Set_Address(glIsProgramPipeline'Address,        "glIsProgramPipeline");
		Set_Address(glGetProgramPipelineiv'Address,     "glGetProgramPipelineiv");
		Set_Address(glProgramUniform1i'Address,         "glProgramUniform1i");
		Set_Address(glProgramUniform1iv'Address,        "glProgramUniform1iv");
		Set_Address(glProgramUniform1f'Address,         "glProgramUniform1f");
		Set_Address(glProgramUniform1fv'Address,        "glProgramUniform1fv");
		Set_Address(glProgramUniform1d'Address,         "glProgramUniform1d");
		Set_Address(glProgramUniform1dv'Address,        "glProgramUniform1dv");
		Set_Address(glProgramUniform1ui'Address,        "glProgramUniform1ui");
		Set_Address(glProgramUniform1uiv'Address,       "glProgramUniform1uiv");
		Set_Address(glProgramUniform2i'Address,         "glProgramUniform2i");
		Set_Address(glProgramUniform2iv'Address,        "glProgramUniform2iv");
		Set_Address(glProgramUniform2f'Address,         "glProgramUniform2f");
		Set_Address(glProgramUniform2fv'Address,        "glProgramUniform2fv");
		Set_Address(glProgramUniform2d'Address,         "glProgramUniform2d");
		Set_Address(glProgramUniform2dv'Address,        "glProgramUniform2dv");
		Set_Address(glProgramUniform2ui'Address,        "glProgramUniform2ui");
		Set_Address(glProgramUniform2uiv'Address,       "glProgramUniform2uiv");
		Set_Address(glProgramUniform3i'Address,         "glProgramUniform3i");
		Set_Address(glProgramUniform3iv'Address,        "glProgramUniform3iv");
		Set_Address(glProgramUniform3f'Address,         "glProgramUniform3f");
		Set_Address(glProgramUniform3fv'Address,        "glProgramUniform3fv");
		Set_Address(glProgramUniform3d'Address,         "glProgramUniform3d");
		Set_Address(glProgramUniform3dv'Address,        "glProgramUniform3dv");
		Set_Address(glProgramUniform3ui'Address,        "glProgramUniform3ui");
		Set_Address(glProgramUniform3uiv'Address,       "glProgramUniform3uiv");
		Set_Address(glProgramUniform4i'Address,         "glProgramUniform4i");
		Set_Address(glProgramUniform4iv'Address,        "glProgramUniform4iv");
		Set_Address(glProgramUniform4f'Address,         "glProgramUniform4f");
		Set_Address(glProgramUniform4fv'Address,        "glProgramUniform4fv");
		Set_Address(glProgramUniform4d'Address,         "glProgramUniform4d");
		Set_Address(glProgramUniform4dv'Address,        "glProgramUniform4dv");
		Set_Address(glProgramUniform4ui'Address,        "glProgramUniform4ui");
		Set_Address(glProgramUniform4uiv'Address,       "glProgramUniform4uiv");
		Set_Address(glProgramUniformMatrix2fv'Address,  "glProgramUniformMatrix2fv");
		Set_Address(glProgramUniformMatrix3fv'Address,  "glProgramUniformMatrix3fv");
		Set_Address(glProgramUniformMatrix4fv'Address,  "glProgramUniformMatrix4fv");
		Set_Address(glProgramUniformMatrix2dv'Address,  "glProgramUniformMatrix2dv");
		Set_Address(glProgramUniformMatrix3dv'Address,  "glProgramUniformMatrix3dv");
		Set_Address(glProgramUniformMatrix4dv'Address,  "glProgramUniformMatrix4dv");
		Set_Address(glProgramUniformMatrix2x3fv'Address,"glProgramUniformMatrix2x3fv");
		Set_Address(glProgramUniformMatrix3x2fv'Address,"glProgramUniformMatrix3x2fv");
		Set_Address(glProgramUniformMatrix2x4fv'Address,"glProgramUniformMatrix2x4fv");
		Set_Address(glProgramUniformMatrix4x2fv'Address,"glProgramUniformMatrix4x2fv");
		Set_Address(glProgramUniformMatrix3x4fv'Address,"glProgramUniformMatrix3x4fv");
		Set_Address(glProgramUniformMatrix4x3fv'Address,"glProgramUniformMatrix4x3fv");
		Set_Address(glProgramUniformMatrix2x3dv'Address,"glProgramUniformMatrix2x3dv");
		Set_Address(glProgramUniformMatrix3x2dv'Address,"glProgramUniformMatrix3x2dv");
		Set_Address(glProgramUniformMatrix2x4dv'Address,"glProgramUniformMatrix2x4dv");
		Set_Address(glProgramUniformMatrix4x2dv'Address,"glProgramUniformMatrix4x2dv");
		Set_Address(glProgramUniformMatrix3x4dv'Address,"glProgramUniformMatrix3x4dv");
		Set_Address(glProgramUniformMatrix4x3dv'Address,"glProgramUniformMatrix4x3dv");
		Set_Address(glValidateProgramPipeline'Address,  "glValidateProgramPipeline");
		Set_Address(glGetProgramPipelineInfoLog'Address,"glGetProgramPipelineInfoLog");
		Set_Address(glVertexAttribL1d'Address,          "glVertexAttribL1d");
		Set_Address(glVertexAttribL2d'Address,          "glVertexAttribL2d");
		Set_Address(glVertexAttribL3d'Address,          "glVertexAttribL3d");
		Set_Address(glVertexAttribL4d'Address,          "glVertexAttribL4d");
		Set_Address(glVertexAttribL1dv'Address,         "glVertexAttribL1dv");
		Set_Address(glVertexAttribL2dv'Address,         "glVertexAttribL2dv");
		Set_Address(glVertexAttribL3dv'Address,         "glVertexAttribL3dv");
		Set_Address(glVertexAttribL4dv'Address,         "glVertexAttribL4dv");
		Set_Address(glVertexAttribLPointer'Address,     "glVertexAttribLPointer");
		Set_Address(glGetVertexAttribLdv'Address,       "glGetVertexAttribLdv");
		Set_Address(glViewportArrayv'Address,           "glViewportArrayv");
		Set_Address(glViewportIndexedf'Address,         "glViewportIndexedf");
		Set_Address(glViewportIndexedfv'Address,        "glViewportIndexedfv");
		Set_Address(glScissorArrayv'Address,            "glScissorArrayv");
		Set_Address(glScissorIndexed'Address,           "glScissorIndexed");
		Set_Address(glScissorIndexedv'Address,          "glScissorIndexedv");
		Set_Address(glDepthRangeArrayv'Address,         "glDepthRangeArrayv");
		Set_Address(glDepthRangeIndexed'Address,        "glDepthRangeIndexed");
		Set_Address(glGetFloati_v'Address,              "glGetFloati_v");
		Set_Address(glGetDoublei_v'Address,             "glGetDoublei_v");
		Set_Address(glCreateSyncFromCLeventARB'Address, "glCreateSyncFromCLeventARB");
		Set_Address(glGetGraphicsResetStatusARB'Address,"glGetGraphicsResetStatusARB");
		Set_Address(glGetnMapdvARB'Address,             "glGetnMapdvARB");
		Set_Address(glGetnMapfvARB'Address,             "glGetnMapfvARB");
		Set_Address(glGetnMapivARB'Address,             "glGetnMapivARB");
		Set_Address(glGetnPixelMapfvARB'Address,        "glGetnPixelMapfvARB");
		Set_Address(glGetnPixelMapuivARB'Address,       "glGetnPixelMapuivARB");
		Set_Address(glGetnPixelMapusvARB'Address,       "glGetnPixelMapusvARB");
		Set_Address(glGetnPolygonStippleARB'Address,    "glGetnPolygonStippleARB");
		Set_Address(glGetnColorTableARB'Address,        "glGetnColorTableARB");
		Set_Address(glGetnConvolutionFilterARB'Address, "glGetnConvolutionFilterARB");
		Set_Address(glGetnSeparableFilterARB'Address,   "glGetnSeparableFilterARB");
		Set_Address(glGetnHistogramARB'Address,         "glGetnHistogramARB");
		Set_Address(glGetnMinmaxARB'Address,            "glGetnMinmaxARB");
		Set_Address(glGetnTexImageARB'Address,          "glGetnTexImageARB");
		Set_Address(glReadnPixelsARB'Address,           "glReadnPixelsARB");
		Set_Address(glGetnCompressedTexImageARB'Address,"glGetnCompressedTexImageARB");
		Set_Address(glGetnUniformfvARB'Address,         "glGetnUniformfvARB");
		Set_Address(glGetnUniformivARB'Address,         "glGetnUniformivARB");
		Set_Address(glGetnUniformuivARB'Address,        "glGetnUniformuivARB");
		Set_Address(glGetnUniformdvARB'Address,         "glGetnUniformdvARB");
		Set_Address(glDrawArraysInstancedBaseInstance'Address, "glDrawArraysInstancedBaseInstance");
		Set_Address(glDrawElementsInstancedBaseInstance'Address, "glDrawElementsInstancedBaseInstance");
		Set_Address(glDrawElementsInstancedBaseVertexBaseInstance'Address, "glDrawElementsInstancedBaseVertexBaseInstance");
		Set_Address(glDrawTransformFeedbackInstanced'Address, "glDrawTransformFeedbackInstanced");
		Set_Address(glDrawTransformFeedbackStreamInstanced'Address, "glDrawTransformFeedbackStreamInstanced");
		Set_Address(glGetInternalformativ'Address,      "glGetInternalformativ");
		Set_Address(glGetActiveAtomicCounterBufferiv'Address, "glGetActiveAtomicCounterBufferiv");
		Set_Address(glBindImageTexture'Address,         "glBindImageTexture");
		Set_Address(glMemoryBarrier'Address,            "glMemoryBarrier");
		Set_Address(glTexStorage1D'Address,             "glTexStorage1D");
		Set_Address(glTexStorage2D'Address,             "glTexStorage2D");
		Set_Address(glTexStorage3D'Address,             "glTexStorage3D");
		Set_Address(glDebugMessageControl'Address,      "glDebugMessageControl");
		Set_Address(glDebugMessageInsert'Address,       "glDebugMessageInsert");
		Set_Address(glDebugMessageCallback'Address,     "glDebugMessageCallback");
		Set_Address(glGetDebugMessageLog'Address,       "glGetDebugMessageLog");
		Set_Address(glPushDebugGroup'Address,           "glPushDebugGroup");
		Set_Address(glPopDebugGroup'Address,            "glPopDebugGroup");
		Set_Address(glObjectLabel'Address,              "glObjectLabel");
		Set_Address(glGetObjectLabel'Address,           "glGetObjectLabel");
		Set_Address(glObjectPtrLabel'Address,           "glObjectPtrLabel");
		Set_Address(glGetObjectPtrLabel'Address,        "glGetObjectPtrLabel");
		Set_Address(glClearBufferData'Address,          "glClearBufferData");
		Set_Address(glClearBufferSubData'Address,       "glClearBufferSubData");
		Set_Address(glDispatchCompute'Address,          "glDispatchCompute");
		Set_Address(glDispatchComputeIndirect'Address,  "glDispatchComputeIndirect");
		Set_Address(glCopyImageSubData'Address,         "glCopyImageSubData");
		Set_Address(glTextureView'Address,              "glTextureView");
		Set_Address(glBindVertexBuffer'Address,         "glBindVertexBuffer");
		Set_Address(glVertexAttribFormat'Address,       "glVertexAttribFormat");
		Set_Address(glVertexAttribIFormat'Address,      "glVertexAttribIFormat");
		Set_Address(glVertexAttribLFormat'Address,      "glVertexAttribLFormat");
		Set_Address(glVertexAttribBinding'Address,      "glVertexAttribBinding");
		Set_Address(glVertexBindingDivisor'Address,     "glVertexBindingDivisor");
		Set_Address(glFramebufferParameteri'Address,    "glFramebufferParameteri");
		Set_Address(glGetFramebufferParameteriv'Address,"glGetFramebufferParameteriv");
		Set_Address(glGetInternalformati64v'Address,    "glGetInternalformati64v");
		Set_Address(glInvalidateTexSubImage'Address,    "glInvalidateTexSubImage");
		Set_Address(glInvalidateTexImage'Address,       "glInvalidateTexImage");
		Set_Address(glInvalidateBufferSubData'Address,  "glInvalidateBufferSubData");
		Set_Address(glInvalidateBufferData'Address,     "glInvalidateBufferData");
		Set_Address(glInvalidateFramebuffer'Address,    "glInvalidateFramebuffer");
		Set_Address(glInvalidateSubFramebuffer'Address, "glInvalidateSubFramebuffer");
		Set_Address(glMultiDrawArraysIndirect'Address,  "glMultiDrawArraysIndirect");
		Set_Address(glMultiDrawElementsIndirect'Address,"glMultiDrawElementsIndirect");
		Set_Address(glGetProgramInterfaceiv'Address,    "glGetProgramInterfaceiv");
		Set_Address(glGetProgramResourceIndex'Address,  "glGetProgramResourceIndex");
		Set_Address(glGetProgramResourceName'Address,   "glGetProgramResourceName");
		Set_Address(glGetProgramResourceiv'Address,     "glGetProgramResourceiv");
		Set_Address(glGetProgramResourceLocation'Address, "glGetProgramResourceLocation");
		Set_Address(glGetProgramResourceLocationIndex'Address, "glGetProgramResourceLocationIndex");
		Set_Address(glShaderStorageBlockBinding'Address,"glShaderStorageBlockBinding");
		Set_Address(glTexBufferRange'Address,           "glTexBufferRange");
		Set_Address(glTexStorage2DMultisample'Address,  "glTexStorage2DMultisample");
		Set_Address(glTexStorage3DMultisample'Address,  "glTexStorage3DMultisample");
		Set_Address(glBufferStorage'Address, "glBufferStorage");
		Set_Address(glClearTexImage'Address, "glClearTexImage");
		Set_Address(glClearTexSubImage'Address, "glClearTexSubImage");
		Set_Address(glBindBuffersBase'Address, "glBindBuffersBase");
		Set_Address(glBindBuffersRange'Address, "glBindBuffersRange");
		Set_Address(glBindTextures'Address, "glBindTextures");
		Set_Address(glBindSamplers'Address, "glBindSamplers");
		Set_Address(glBindImageTextures'Address, "glBindImageTextures");
		Set_Address(glBindVertexBuffers'Address, "glBindVertexBuffers");
		Sogla.Log_IO.New_Line;
		Sogla.Log_IO.Put_Line("--- Available OpenGL Extensions:");

		for Ext_Index in 0..Number_of_extensions loop
			Sogla.Log_IO.Put_Line("      " & Get_GL_Stringi(GL_EXTENSIONS, GLuint(Ext_Index)));
		end loop;

		Sogla.Log_IO.New_Line;

		return true;
	end Initialize;

	---------------------------------------------------------
	-- Get_GL_String                                       --
	-- A helper for converting glGetString to a Ada String --
	---------------------------------------------------------
	function Get_GL_String (name : GLenum) return String
	is
		use Interfaces.C.Strings;

		function to_char_ptr is new Ada.Unchecked_Conversion(a_GLubyte,
															 chars_ptr);

		C_Gl_String : chars_ptr := to_char_ptr(glGetString(name));
	begin
		return Value(C_Gl_String);

		-- Tetsumi <tetsumi@vmail.me>:  the c string must not be freed. glGetString
		--          return a static string pointer.
	end Get_GL_String;

	----------------------------------------------------------
	-- Get_GL_Stringi                                       --
	-- A helper for converting glGetStringi to a Ada String --
	----------------------------------------------------------
	function Get_GL_Stringi (name : GLenum;
							 index : GLuint)
							 return String
	is
		use Interfaces.C.Strings;
		use Interfaces.C;

		String_Pointer : chars_ptr := glGetStringi(name, index);
	begin
		if (String_Pointer = Null_Ptr) then
			return "";
		end if;

		return Value(String_Pointer);

		-- Tetsumi <tetsumi@vmail.me>:  the c string must not be freed. glGetStringi
		--          return a static string pointer.
	end Get_GL_Stringi;

	----------------------
	-- To_GLchar_Access --
	----------------------
	function To_GLchar_Access (Ada_String : String)
							   return access GLchar
	is
		use Interfaces.C.Strings;
		use Interfaces.C;

		glchar_str : GLchar := To_C(Ada_String);
	begin
		return glchar_str'Unrestricted_Access;
	end To_GLchar_Access;

	-----------------------------------------------
	-- Raise_Last_Error                          --
	-- Convert a OpenGL error to a Ada exception --
	-----------------------------------------------
	procedure Raise_Last_Error
	is
		GL_Error_Code : GLenum := glGetError;
	begin
		case GL_Error_Code is
			when GL_NO_ERROR =>
				return;
			when GL_INVALID_ENUM =>
				raise OpenGL.Invalid_Enum;
			when GL_INVALID_VALUE =>
				raise OpenGL.Invalid_Value;
			when GL_INVALID_OPERATION =>
				raise OpenGL.Invalid_Operation;
			when GL_OUT_OF_MEMORY =>
				raise OpenGL.Out_Of_Memory;
			when GL_INVALID_FRAMEBUFFER_OPERATION =>
				raise OpenGL.Invalid_Framebuffer_Operation;
			when others =>
				raise OpenGL.Undefined_Error;
		end case;
	end Raise_Last_Error;


end Sogla.OpenGL;
