--------------------------------------------------------------------------------
--  Sogla                                                                     --
--                                                                            --
--  sogla-log_io.adb                                                          --
--                                                                            --
--  Contributors:                                                             --
--      Tetsumi <tetsumi@vmail.me>                                              --
--                                                                            --
--                                                                            --
--  Copyright © 2013 Tetsumi                                                   --
--                                                                            --
--  Licensed under the Apache License, Version 2.0 (the "License"); you may   --
--  not use this file except in compliance with the License. You may obtain   --
--  a copy of the License at                                                  --
--                                                                            --
--  http://www.apache.org/licenses/LICENSE-2.0.html                           --
--  http://www.apache.org/licenses/LICENSE-2.0.txt                            --
--                                                                            --
--  Unless required by applicable law or agreed to in writing, software       --
--  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT --
--  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the  --
--  License for the specific language governing permissions and limitations   --
--  under the License.                                                        --
--                                                                            --
--------------------------------------------------------------------------------

with
	GNAT.Time_Stamp,
	GNAT.Compiler_Version,
	Sogla.Glfw,
	Ada.Text_IO,
	Ada.Integer_Text_IO,
	Interfaces.C,
	Interfaces.C.Strings;

use
	Interfaces.C,
	Interfaces.C.Strings;

package body Sogla.Log_IO is
	---------
	-- Put --
	---------
	procedure Put
	    (Item : Character)
	is 		
	begin -- Put
		if Active then
			Ada.Text_IO.Put(Log_File, Item);
		end if;
	end Put;

	---------
	-- Put --
	---------
	procedure Put
	    (Item : String)
	is 		
	begin -- Put
		if Active then
			Ada.Text_IO.Put(Log_File, Item);
		end if;
	end Put;

	--------------
	-- Put_Line --
	--------------
	procedure Put_Line
	    (Item : String)
	is 		
	begin -- Put
		if Active then
			Ada.Text_IO.Put_Line(Log_File, Item);
		end if;
	end Put_Line;

	--------------
	-- New_Line --
	--------------
	procedure New_Line
	    (Spacing : in Ada.Text_IO.Positive_Count := 1)
	is 		
	begin -- Put
		if Active then
			Ada.Text_IO.New_Line(Log_File, Spacing);
		end if;
	end New_Line;

	----------------
	-- Initialize --
	----------------
	procedure Initialize 
	is
		package CVer is new GNAT.Compiler_Version;
		package CIO is new Ada.Text_IO.Integer_IO(Interfaces.C.int);

		GLFW_Major : aliased Interfaces.C.int := 0;
		GLFW_Minor : aliased Interfaces.C.int := 0;
		GLFW_Rev   : aliased Interfaces.C.int := 0;
	begin -- Initialize
		Ada.Text_IO.Create(Log_File, Ada.Text_IO.Out_File, Log_File_Name);

		Sogla.Glfw.glfwGetVersion(GLFW_Major'Access, GLFW_Minor'Access, GLFW_Rev'Access);

		if (GLFW_Major < 2 or GLFW_Minor < 7) then
			raise Constraint_Error with "Need GLFW version 2.7.x";
		end if;
		
		Sogla.Log_IO.Put_Line("--------------------------------");
		Sogla.Log_IO.Put_Line("Sogla Version " & Sogla.Version_String);
		Sogla.Log_IO.Put_Line("--------------------------------");
		Sogla.Log_IO.New_Line(2);
		Sogla.Log_IO.Put_Line("Compiled with GNAT " & CVer.Version);
		Sogla.Log_IO.New_Line;

#if BUILD_DEBUG then
		Sogla.Log_IO.Put_Line("Build mode: Debug");
#else
		Sogla.Log_IO.Put_Line("Build mode: Release");
#end if;

		Sogla.Log_IO.Put_Line("Log started at: " & GNAT.Time_Stamp.Current_Time);
		Sogla.Log_IO.Put("GLFW Version: ");
		CIO.Put(Sogla.Log_IO.Log_File, GLFW_Major, 0);
		Sogla.Log_IO.Put(".");
		CIO.Put(Sogla.Log_IO.Log_File, GLFW_Minor, 0);
		Sogla.Log_IO.Put(".");
		CIO.Put(Sogla.Log_IO.Log_File, GLFW_Rev, 0);
		Sogla.Log_IO.New_Line(2);

		Initialized := True;
	end Initialize;

	--------------
	-- Finalize --
	--------------
	procedure Finalize 
	is 
	begin -- Finalize
		Ada.Text_IO.Flush(Log_File);
		Ada.Text_IO.Close(Log_File);
		Initialized := False;
	end Finalize;

	--------------------
	-- Is_Initialized --
	--------------------
	function Is_Initialized
	return
		Boolean
	is
	begin -- Is_Initialized
		return Initialized;
	end Is_Initialized;
end Sogla.Log_IO;
