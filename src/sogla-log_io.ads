--------------------------------------------------------------------------------
--  Sogla                                                                     --
--                                                                            --
--  sogla-log_io.ads                                                          --
--                                                                            --
--  Contributors:                                                             --
--      Tetsumi <tetsumi@vmail.me>                                              --
--                                                                            --
--                                                                            --
--  Copyright © 2013 Tetsumi                                                   --
--                                                                            --
--  Licensed under the Apache License, Version 2.0 (the "License"); you may   --
--  not use this file except in compliance with the License. You may obtain   --
--  a copy of the License at                                                  --
--                                                                            --
--  http://www.apache.org/licenses/LICENSE-2.0.html                           --
--  http://www.apache.org/licenses/LICENSE-2.0.txt                            --
--                                                                            --
--  Unless required by applicable law or agreed to in writing, software       --
--  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT --
--  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the  --
--  License for the specific language governing permissions and limitations   --
--  under the License.                                                        --
--                                                                            --
--------------------------------------------------------------------------------

with
	Ada.Text_IO;

package Sogla.Log_IO is
	Log_File_Name : constant String := "Log.txt";

	Log_File : Ada.Text_IO.File_Type;

	Active : Boolean := True;

	procedure Initialize;
	procedure Finalize;
	procedure Put (Item : in  Character);
	procedure Put (Item : in  String);
	procedure Put_Line (Item : in  String);
	procedure New_Line (Spacing : in Ada.Text_IO.Positive_Count := 1);
	function Is_Initialized return Boolean;
private
	Initialized : Boolean := False;
end Sogla.Log_IO;
