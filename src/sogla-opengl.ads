--------------------------------------------------------------------------------
--  Sogla                                                                     --
--                                                                            --
--  sogla-opengl.ads                                                          --
--                                                                            --
--  Contributors:                                                             --
--      Tetsumi <tetsumi@vmail.me>                                              --
--                                                                            --
--                                                                            --
--  Copyright © 2013 Tetsumi                                                   --
--                                                                            --
--  Licensed under the Apache License, Version 2.0 (the "License"); you may   --
--  not use this file except in compliance with the License. You may obtain   --
--  a copy of the License at                                                  --
--                                                                            --
--  http://www.apache.org/licenses/LICENSE-2.0.html                           --
--  http://www.apache.org/licenses/LICENSE-2.0.txt                            --
--                                                                            --
--  Unless required by applicable law or agreed to in writing, software       --
--  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT --
--  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the  --
--  License for the specific language governing permissions and limitations   --
--  under the License.                                                        --
--                                                                            --
--------------------------------------------------------------------------------

-- http://www.opengl.org/registry/api/GL/glcorearb.h

with 
	Interfaces.C,
	Interfaces.C.Extensions,
	Interfaces.C.Strings,
	System;


package Sogla.OpenGL is

	-------------------
	-- Base GL types --
	-------------------
	subtype GLenum              is Interfaces.C.unsigned;
	subtype GLboolean           is Interfaces.C.unsigned_char;
	subtype GLbitfield          is Interfaces.C.unsigned;
	subtype GLbyte              is Interfaces.C.signed_char;
	subtype GLshort             is Short_Integer;
	subtype GLint               is Integer;
	subtype GLsizei             is Integer;
	subtype GLubyte             is Interfaces.C.unsigned_char;
	subtype GLushort            is Interfaces.C.unsigned_short;
	subtype GLuint              is Interfaces.C.unsigned;
	subtype GLhalf              is Interfaces.C.unsigned_short;
	subtype GLfloat             is Float;
	subtype GLclampf            is Float;
	subtype GLdouble            is Long_Float;
	subtype GLclampd            is Long_Float;
	subtype GLvoid              is System.Address; -- same as GLvoid*
	subtype GLchar              is Interfaces.C.char_array;
	subtype GLintptr            is Interfaces.C.ptrdiff_t;
	subtype GLsizeiptr          is Interfaces.C.ptrdiff_t;
	subtype GLintptrARB         is Interfaces.C.ptrdiff_t;
	subtype GLsizeiptrARB       is Interfaces.C.ptrdiff_t;
	subtype GLcharARB           is Interfaces.C.char;
	subtype GLhandleARB         is Interfaces.C.unsigned;
	subtype GLhalfARB           is Interfaces.C.unsigned_short;
	subtype GLhalfNV            is Interfaces.C.unsigned_short;
	subtype GLint64EXT          is Interfaces.Integer_64;
	subtype GLuint64EXT         is Interfaces.Unsigned_64;
	subtype GLint64             is Interfaces.Integer_64;
	subtype GLuint64            is Interfaces.Unsigned_64;
	subtype GLsync              is System.Address;
	subtype GLvdpauSurfaceNV    is Interfaces.C.ptrdiff_t;

	-------------------------
	-- Callback prototypes --
	-------------------------
	type GLDEBUGPROCARB is access procedure (source : GLenum;
											 typee : GLenum;
											 id : GLuint;
											 severity : GLenum;
											 length : GLsizei;
											 message : access constant GLchar;
											 userParam : GLvoid);

	type GLDEBUGPROCAMD is access procedure (id : GLuint;
											 category : GLenum;
											 severity : GLenum;
											 length : GLsizei;
											 message : access constant GLchar;
											 userParam : GLvoid);
	------------------------------
	-- Ada binding custom types --
	------------------------------
	type a_GLubyte is access GLubyte;

	----------------------------------
	-- Ada binding custom constants --
	----------------------------------
	NULL_VOID : constant System.Address := System.Null_Address;

	------------------------------
	-- Ada binding custom procs --
	------------------------------
	function Initialize return Boolean;
	function Get_GL_String (name : GLenum) return String;

	function Get_GL_Stringi (name : GLenum;
							 index : GLuint)
							 return String;

	function To_GLchar_Access (Ada_String : String)
							   return access GLchar;

	procedure Raise_Last_Error;

	--------------------------------
	-- Ada binding custom globals --
	--------------------------------
	Version_Major : aliased GLint;
	-- Current OpenGL Major version.

	Version_Minor : aliased GLint;
	-- Current OpenGL Minor version.

	-----------------------------------
	-- Ada binding custom exceptions --
	-----------------------------------
	Invalid_Enum                  : exception;
	Invalid_Value                 : exception;
	Invalid_Operation             : exception;
	Out_Of_Memory                 : exception;
	Invalid_Framebuffer_Operation : exception;
	Undefined_Error               : exception;

	----------------
	-- OpenGL 1.1 --
	----------------
	GL_DEPTH_BUFFER_BIT    : constant := 16#00000100#;
	GL_STENCIL_BUFFER_BIT  : constant := 16#00000400#;
	GL_COLOR_BUFFER_BIT    : constant := 16#00004000#;
	GL_FALSE    : constant := 0;
	GL_TRUE     : constant := 1;
	GL_POINTS         : constant := 16#0000#;
	GL_LINES          : constant := 16#0001#;
	GL_LINE_LOOP      : constant := 16#0002#;
	GL_LINE_STRIP     : constant := 16#0003#;
	GL_TRIANGLES      : constant := 16#0004#;
	GL_TRIANGLE_STRIP : constant := 16#0005#;
	GL_TRIANGLE_FAN   : constant := 16#0006#;
	GL_QUADS          : constant := 16#0007#;
	GL_NEVER    : constant := 16#0200#;
	GL_LESS     : constant := 16#0201#;
	GL_EQUAL    : constant := 16#0202#;
	GL_LEQUAL   : constant := 16#0203#;
	GL_GREATER  : constant := 16#0204#;
	GL_NOTEQUAL : constant := 16#0205#;
	GL_GEQUAL   : constant := 16#0206#;
	GL_ALWAYS   : constant := 16#0207#;
	GL_ZERO                : constant := 0;
	GL_ONE                 : constant := 1;
	GL_SRC_COLOR           : constant := 16#0300#;
	GL_ONE_MINUS_SRC_COLOR : constant := 16#0301#;
	GL_SRC_ALPHA           : constant := 16#0302#;
	GL_ONE_MINUS_SRC_ALPHA : constant := 16#0303#;
	GL_DST_ALPHA           : constant := 16#0304#;
	GL_ONE_MINUS_DST_ALPHA : constant := 16#0305#;
	GL_DST_COLOR           : constant := 16#0306#;
	GL_ONE_MINUS_DST_COLOR : constant := 16#0307#;
	GL_SRC_ALPHA_SATURATE  : constant := 16#0308#;
	GL_NONE           : constant := 0;
	GL_FRONT_LEFT     : constant := 16#0400#;
	GL_FRONT_RIGHT    : constant := 16#0401#;
	GL_BACK_LEFT      : constant := 16#0402#;
	GL_BACK_RIGHT     : constant := 16#0403#;
	GL_FRONT          : constant := 16#0404#;
	GL_BACK           : constant := 16#0405#;
	GL_LEFT           : constant := 16#0406#;
	GL_RIGHT          : constant := 16#0407#;
	GL_FRONT_AND_BACK : constant := 16#0408#;
	GL_NO_ERROR          : constant := 0;
	GL_INVALID_ENUM      : constant := 16#0500#;
	GL_INVALID_VALUE     : constant := 16#0501#;
	GL_INVALID_OPERATION : constant := 16#0502#;
	GL_OUT_OF_MEMORY     : constant := 16#0505#;
	GL_CW      : constant := 16#0900#;
	GL_CCW     : constant := 16#0901#;
	GL_POINT_SIZE              : constant := 16#0B11#;
	GL_POINT_SIZE_RANGE        : constant := 16#0B12#;
	GL_POINT_SIZE_GRANULARITY  : constant := 16#0B13#;
	GL_LINE_SMOOTH             : constant := 16#0B20#;
	GL_LINE_WIDTH              : constant := 16#0B21#;
	GL_LINE_WIDTH_RANGE        : constant := 16#0B22#;
	GL_LINE_WIDTH_GRANULARITY  : constant := 16#0B23#;
	GL_POLYGON_SMOOTH          : constant := 16#0B41#;
	GL_CULL_FACE               : constant := 16#0B44#;
	GL_CULL_FACE_MODE          : constant := 16#0B45#;
	GL_FRONT_FACE              : constant := 16#0B46#;
	GL_DEPTH_RANGE             : constant := 16#0B70#;
	GL_DEPTH_TEST              : constant := 16#0B71#;
	GL_DEPTH_WRITEMASK         : constant := 16#0B72#;
	GL_DEPTH_CLEAR_VALUE       : constant := 16#0B73#;
	GL_DEPTH_FUNC              : constant := 16#0B74#;
	GL_STENCIL_TEST            : constant := 16#0B90#;
	GL_STENCIL_CLEAR_VALUE     : constant := 16#0B91#;
	GL_STENCIL_FUNC            : constant := 16#0B92#;
	GL_STENCIL_VALUE_MASK      : constant := 16#0B93#;
	GL_STENCIL_FAIL            : constant := 16#0B94#;
	GL_STENCIL_PASS_DEPTH_FAIL : constant := 16#0B95#;
	GL_STENCIL_PASS_DEPTH_PASS : constant := 16#0B96#;
	GL_STENCIL_REF             : constant := 16#0B97#;
	GL_STENCIL_WRITEMASK       : constant := 16#0B98#;
	GL_VIEWPORT                : constant := 16#0BA2#;
	GL_DITHER                  : constant := 16#0BD0#;
	GL_BLEND_DST               : constant := 16#0BE0#;
	GL_BLEND_SRC               : constant := 16#0BE1#;
	GL_BLEND                   : constant := 16#0BE2#;
	GL_LOGIC_OP_MODE           : constant := 16#0BF0#;
	GL_COLOR_LOGIC_OP          : constant := 16#0BF2#;
	GL_DRAW_BUFFER             : constant := 16#0C01#;
	GL_READ_BUFFER             : constant := 16#0C02#;
	GL_SCISSOR_BOX             : constant := 16#0C10#;
	GL_SCISSOR_TEST            : constant := 16#0C11#;
	GL_COLOR_CLEAR_VALUE       : constant := 16#0C22#;
	GL_COLOR_WRITEMASK         : constant := 16#0C23#;
	GL_DOUBLEBUFFER            : constant := 16#0C32#;
	GL_STEREO                  : constant := 16#0C33#;
	GL_LINE_SMOOTH_HINT        : constant := 16#0C52#;
	GL_POLYGON_SMOOTH_HINT     : constant := 16#0C53#;
	GL_UNPACK_SWAP_BYTES       : constant := 16#0CF0#;
	GL_UNPACK_LSB_FIRST        : constant := 16#0CF1#;
	GL_UNPACK_ROW_LENGTH       : constant := 16#0CF2#;
	GL_UNPACK_SKIP_ROWS        : constant := 16#0CF3#;
	GL_UNPACK_SKIP_PIXELS      : constant := 16#0CF4#;
	GL_UNPACK_ALIGNMENT        : constant := 16#0CF5#;
	GL_PACK_SWAP_BYTES         : constant := 16#0D00#;
	GL_PACK_LSB_FIRST          : constant := 16#0D01#;
	GL_PACK_ROW_LENGTH         : constant := 16#0D02#;
	GL_PACK_SKIP_ROWS          : constant := 16#0D03#;
	GL_PACK_SKIP_PIXELS        : constant := 16#0D04#;
	GL_PACK_ALIGNMENT          : constant := 16#0D05#;
	GL_MAX_TEXTURE_SIZE        : constant := 16#0D33#;
	GL_MAX_VIEWPORT_DIMS       : constant := 16#0D3A#;
	GL_SUBPIXEL_BITS           : constant := 16#0D50#;
	GL_TEXTURE_1D              : constant := 16#0DE0#;
	GL_TEXTURE_2D              : constant := 16#0DE1#;
	GL_POLYGON_OFFSET_UNITS    : constant := 16#2A00#;
	GL_POLYGON_OFFSET_POINT    : constant := 16#2A01#;
	GL_POLYGON_OFFSET_LINE     : constant := 16#2A02#;
	GL_POLYGON_OFFSET_FILL     : constant := 16#8037#;
	GL_POLYGON_OFFSET_FACTOR   : constant := 16#8038#;
	GL_TEXTURE_BINDING_1D      : constant := 16#8068#;
	GL_TEXTURE_BINDING_2D      : constant := 16#8069#;
	GL_TEXTURE_WIDTH           : constant := 16#1000#;
	GL_TEXTURE_HEIGHT          : constant := 16#1001#;
	GL_TEXTURE_INTERNAL_FORMAT : constant := 16#1003#;
	GL_TEXTURE_BORDER_COLOR    : constant := 16#1004#;
	GL_TEXTURE_RED_SIZE        : constant := 16#805C#;
	GL_TEXTURE_GREEN_SIZE      : constant := 16#805D#;
	GL_TEXTURE_BLUE_SIZE       : constant := 16#805E#;
	GL_TEXTURE_ALPHA_SIZE      : constant := 16#805F#;
	GL_DONT_CARE              : constant := 16#1100#;
	GL_FASTEST                : constant := 16#1101#;
	GL_NICEST                 : constant := 16#1102#;
	GL_BYTE                   : constant := 16#1400#;
	GL_UNSIGNED_BYTE          : constant := 16#1401#;
	GL_SHORT                  : constant := 16#1402#;
	GL_UNSIGNED_SHORT         : constant := 16#1403#;
	GL_INT                    : constant := 16#1404#;
	GL_UNSIGNED_INT           : constant := 16#1405#;
	GL_FLOAT                  : constant := 16#1406#;
	GL_DOUBLE                 : constant := 16#140A#;
	GL_CLEAR                  : constant := 16#1500#;
	GL_AND                    : constant := 16#1501#;
	GL_AND_REVERSE            : constant := 16#1502#;
	GL_COPY                   : constant := 16#1503#;
	GL_AND_INVERTED           : constant := 16#1504#;
	GL_NOOP                   : constant := 16#1505#;
	GL_XOR                    : constant := 16#1506#;
	GL_OR                     : constant := 16#1507#;
	GL_NOR                    : constant := 16#1508#;
	GL_EQUIV                  : constant := 16#1509#;
	GL_INVERT                 : constant := 16#150A#;
	GL_OR_REVERSE             : constant := 16#150B#;
	GL_COPY_INVERTED          : constant := 16#150C#;
	GL_OR_INVERTED            : constant := 16#150D#;
	GL_NAND                   : constant := 16#150E#;
	GL_SET                    : constant := 16#150F#;
	GL_TEXTURE                : constant := 16#1702#;
	GL_COLOR                  : constant := 16#1800#;
	GL_DEPTH                  : constant := 16#1801#;
	GL_STENCIL                : constant := 16#1802#;
	GL_STENCIL_INDEX          : constant := 16#1901#;
	GL_DEPTH_COMPONENT        : constant := 16#1902#;
	GL_RED                    : constant := 16#1903#;
	GL_GREEN                  : constant := 16#1904#;
	GL_BLUE                   : constant := 16#1905#;
	GL_ALPHA                  : constant := 16#1906#;
	GL_RGB                    : constant := 16#1907#;
	GL_RGBA                   : constant := 16#1908#;
	GL_POINT                  : constant := 16#1B00#;
	GL_LINE                   : constant := 16#1B01#;
	GL_FILL                   : constant := 16#1B02#;
	GL_KEEP                   : constant := 16#1E00#;
	GL_REPLACE                : constant := 16#1E01#;
	GL_INCR                   : constant := 16#1E02#;
	GL_DECR                   : constant := 16#1E03#;
	GL_VENDOR                 : constant := 16#1F00#;
	GL_RENDERER               : constant := 16#1F01#;
	GL_VERSION                : constant := 16#1F02#;
	GL_EXTENSIONS             : constant := 16#1F03#;
	GL_NEAREST                : constant := 16#2600#;
	GL_LINEAR                 : constant := 16#2601#;
	GL_NEAREST_MIPMAP_NEAREST : constant := 16#2700#;
	GL_LINEAR_MIPMAP_NEAREST  : constant := 16#2701#;
	GL_NEAREST_MIPMAP_LINEAR  : constant := 16#2702#;
	GL_LINEAR_MIPMAP_LINEAR   : constant := 16#2703#;
	GL_TEXTURE_MAG_FILTER     : constant := 16#2800#;
	GL_TEXTURE_MIN_FILTER     : constant := 16#2801#;
	GL_TEXTURE_WRAP_S         : constant := 16#2802#;
	GL_TEXTURE_WRAP_T         : constant := 16#2803#;
	GL_PROXY_TEXTURE_1D       : constant := 16#8063#;
	GL_PROXY_TEXTURE_2D       : constant := 16#8064#;
	GL_REPEAT                 : constant := 16#2901#;
	GL_R3_G3_B2               : constant := 16#2A10#;
	GL_RGB4                   : constant := 16#804F#;
	GL_RGB5                   : constant := 16#8050#;
	GL_RGB8                   : constant := 16#8051#;
	GL_RGB10                  : constant := 16#8052#;
	GL_RGB12                  : constant := 16#8053#;
	GL_RGB16                  : constant := 16#8054#;
	GL_RGBA2                  : constant := 16#8055#;
	GL_RGBA4                  : constant := 16#8056#;
	GL_RGB5_A1                : constant := 16#8057#;
	GL_RGBA8                  : constant := 16#8058#;
	GL_RGB10_A2               : constant := 16#8059#;
	GL_RGBA12                 : constant := 16#805A#;
	GL_RGBA16                 : constant := 16#805B#;
	GL_VERTEX_ARRAY           : constant := 16#8074#;

	----------------
	-- OpenGL 1.2 --
	----------------
	GL_UNSIGNED_BYTE_3_3_2           : constant := 16#8032#;
	GL_UNSIGNED_SHORT_4_4_4_4        : constant := 16#8033#;
	GL_UNSIGNED_SHORT_5_5_5_1        : constant := 16#8034#;
	GL_UNSIGNED_INT_8_8_8_8          : constant := 16#8035#;
	GL_UNSIGNED_INT_10_10_10_2       : constant := 16#8036#;
	GL_TEXTURE_BINDING_3D            : constant := 16#806A#;
	GL_PACK_SKIP_IMAGES              : constant := 16#806B#;
	GL_PACK_IMAGE_HEIGHT             : constant := 16#806C#;
	GL_UNPACK_SKIP_IMAGES            : constant := 16#806D#;
	GL_UNPACK_IMAGE_HEIGHT           : constant := 16#806E#;
	GL_TEXTURE_3D                    : constant := 16#806F#;
	GL_PROXY_TEXTURE_3D              : constant := 16#8070#;
	GL_TEXTURE_DEPTH                 : constant := 16#8071#;
	GL_TEXTURE_WRAP_R                : constant := 16#8072#;
	GL_MAX_3D_TEXTURE_SIZE           : constant := 16#8073#;
	GL_UNSIGNED_BYTE_2_3_3_REV       : constant := 16#8362#;
	GL_UNSIGNED_SHORT_5_6_5          : constant := 16#8363#;
	GL_UNSIGNED_SHORT_5_6_5_REV      : constant := 16#8364#;
	GL_UNSIGNED_SHORT_4_4_4_4_REV    : constant := 16#8365#;
	GL_UNSIGNED_SHORT_1_5_5_5_REV    : constant := 16#8366#;
	GL_UNSIGNED_INT_8_8_8_8_REV      : constant := 16#8367#;
	GL_UNSIGNED_INT_2_10_10_10_REV   : constant := 16#8368#;
	GL_BGR                           : constant := 16#80E0#;
	GL_BGRA                          : constant := 16#80E1#;
	GL_MAX_ELEMENTS_VERTICES         : constant := 16#80E8#;
	GL_MAX_ELEMENTS_INDICES          : constant := 16#80E9#;
	GL_CLAMP_TO_EDGE                 : constant := 16#812F#;
	GL_TEXTURE_MIN_LOD               : constant := 16#813A#;
	GL_TEXTURE_MAX_LOD               : constant := 16#813B#;
	GL_TEXTURE_BASE_LEVEL            : constant := 16#813C#;
	GL_TEXTURE_MAX_LEVEL             : constant := 16#813D#;
	GL_SMOOTH_POINT_SIZE_RANGE       : constant := 16#0B12#;
	GL_SMOOTH_POINT_SIZE_GRANULARITY : constant := 16#0B13#;
	GL_SMOOTH_LINE_WIDTH_RANGE       : constant := 16#0B22#;
	GL_SMOOTH_LINE_WIDTH_GRANULARITY : constant := 16#0B23#;
	GL_ALIASED_LINE_WIDTH_RANGE      : constant := 16#846E#;

	----------------
	-- OpenGL 1.3 --
	----------------
	GL_TEXTURE0                       : constant := 16#84C0#;
	GL_TEXTURE1                       : constant := 16#84C1#;
	GL_TEXTURE2                       : constant := 16#84C2#;
	GL_TEXTURE3                       : constant := 16#84C3#;
	GL_TEXTURE4                       : constant := 16#84C4#;
	GL_TEXTURE5                       : constant := 16#84C5#;
	GL_TEXTURE6                       : constant := 16#84C6#;
	GL_TEXTURE7                       : constant := 16#84C7#;
	GL_TEXTURE8                       : constant := 16#84C8#;
	GL_TEXTURE9                       : constant := 16#84C9#;
	GL_TEXTURE10                      : constant := 16#84CA#;
	GL_TEXTURE11                      : constant := 16#84CB#;
	GL_TEXTURE12                      : constant := 16#84CC#;
	GL_TEXTURE13                      : constant := 16#84CD#;
	GL_TEXTURE14                      : constant := 16#84CE#;
	GL_TEXTURE15                      : constant := 16#84CF#;
	GL_TEXTURE16                      : constant := 16#84D0#;
	GL_TEXTURE17                      : constant := 16#84D1#;
	GL_TEXTURE18                      : constant := 16#84D2#;
	GL_TEXTURE19                      : constant := 16#84D3#;
	GL_TEXTURE20                      : constant := 16#84D4#;
	GL_TEXTURE21                      : constant := 16#84D5#;
	GL_TEXTURE22                      : constant := 16#84D6#;
	GL_TEXTURE23                      : constant := 16#84D7#;
	GL_TEXTURE24                      : constant := 16#84D8#;
	GL_TEXTURE25                      : constant := 16#84D9#;
	GL_TEXTURE26                      : constant := 16#84DA#;
	GL_TEXTURE27                      : constant := 16#84DB#;
	GL_TEXTURE28                      : constant := 16#84DC#;
	GL_TEXTURE29                      : constant := 16#84DD#;
	GL_TEXTURE30                      : constant := 16#84DE#;
	GL_TEXTURE31                      : constant := 16#84DF#;
	GL_ACTIVE_TEXTURE                 : constant := 16#84E0#;
	GL_MULTISAMPLE                    : constant := 16#809D#;
	GL_SAMPLE_ALPHA_TO_COVERAGE       : constant := 16#809E#;
	GL_SAMPLE_ALPHA_TO_ONE            : constant := 16#809F#;
	GL_SAMPLE_COVERAGE                : constant := 16#80A0#;
	GL_SAMPLE_BUFFERS                 : constant := 16#80A8#;
	GL_SAMPLES                        : constant := 16#80A9#;
	GL_SAMPLE_COVERAGE_VALUE          : constant := 16#80AA#;
	GL_SAMPLE_COVERAGE_INVERT         : constant := 16#80AB#;
	GL_TEXTURE_CUBE_MAP               : constant := 16#8513#;
	GL_TEXTURE_BINDING_CUBE_MAP       : constant := 16#8514#;
	GL_TEXTURE_CUBE_MAP_POSITIVE_X    : constant := 16#8515#;
	GL_TEXTURE_CUBE_MAP_NEGATIVE_X    : constant := 16#8516#;
	GL_TEXTURE_CUBE_MAP_POSITIVE_Y    : constant := 16#8517#;
	GL_TEXTURE_CUBE_MAP_NEGATIVE_Y    : constant := 16#8518#;
	GL_TEXTURE_CUBE_MAP_POSITIVE_Z    : constant := 16#8519#;
	GL_TEXTURE_CUBE_MAP_NEGATIVE_Z    : constant := 16#851A#;
	GL_PROXY_TEXTURE_CUBE_MAP         : constant := 16#851B#;
	GL_MAX_CUBE_MAP_TEXTURE_SIZE      : constant := 16#851C#;
	GL_COMPRESSED_RGB                 : constant := 16#84ED#;
	GL_COMPRESSED_RGBA                : constant := 16#84EE#;
	GL_TEXTURE_COMPRESSION_HINT       : constant := 16#84EF#;
	GL_TEXTURE_COMPRESSED_IMAGE_SIZE  : constant := 16#86A0#;
	GL_TEXTURE_COMPRESSED             : constant := 16#86A1#;
	GL_NUM_COMPRESSED_TEXTURE_FORMATS : constant := 16#86A2#;
	GL_COMPRESSED_TEXTURE_FORMATS     : constant := 16#86A3#;
	GL_CLAMP_TO_BORDER                : constant := 16#812D#;

	----------------
	-- OpenGL 1.4 --
	----------------
	GL_BLEND_DST_RGB              : constant := 16#80C8#;
	GL_BLEND_SRC_RGB              : constant := 16#80C9#;
	GL_BLEND_DST_ALPHA            : constant := 16#80CA#;
	GL_BLEND_SRC_ALPHA            : constant := 16#80CB#;
	GL_POINT_FADE_THRESHOLD_SIZE  : constant := 16#8128#;
	GL_DEPTH_COMPONENT16          : constant := 16#81A5#;
	GL_DEPTH_COMPONENT24          : constant := 16#81A6#;
	GL_DEPTH_COMPONENT32          : constant := 16#81A7#;
	GL_MIRRORED_REPEAT            : constant := 16#8370#;
	GL_MAX_TEXTURE_LOD_BIAS       : constant := 16#84FD#;
	GL_TEXTURE_LOD_BIAS           : constant := 16#8501#;
	GL_INCR_WRAP                  : constant := 16#8507#;
	GL_DECR_WRAP                  : constant := 16#8508#;
	GL_TEXTURE_DEPTH_SIZE         : constant := 16#884A#;
	GL_TEXTURE_COMPARE_MODE       : constant := 16#884C#;
	GL_TEXTURE_COMPARE_FUNC       : constant := 16#884D#;
	GL_CONSTANT_COLOR             : constant := 16#8001#;
	GL_ONE_MINUS_CONSTANT_COLOR   : constant := 16#8002#;
	GL_CONSTANT_ALPHA             : constant := 16#8003#;
	GL_ONE_MINUS_CONSTANT_ALPHA   : constant := 16#8004#;
	GL_FUNC_ADD                   : constant := 16#8006#;
	GL_MIN                        : constant := 16#8007#;
	GL_MAX                        : constant := 16#8008#;
	GL_FUNC_SUBTRACT              : constant := 16#800A#;
	GL_FUNC_REVERSE_SUBTRACT      : constant := 16#800B#;
	----------------
	-- OpenGL 1.5 --
	----------------
	GL_BUFFER_SIZE                     : constant := 16#8764#;
	GL_BUFFER_USAGE                    : constant := 16#8765#;
	GL_QUERY_COUNTER_BITS              : constant := 16#8864#;
	GL_CURRENT_QUERY                   : constant := 16#8865#;
	GL_QUERY_RESULT                    : constant := 16#8866#;
	GL_QUERY_RESULT_AVAILABLE          : constant := 16#8867#;
	GL_ARRAY_BUFFER                    : constant := 16#8892#;
	GL_ELEMENT_ARRAY_BUFFER            : constant := 16#8893#;
	GL_ARRAY_BUFFER_BINDING            : constant := 16#8894#;
	GL_ELEMENT_ARRAY_BUFFER_BINDING    : constant := 16#8895#;
	GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING  : constant := 16#889F#;
	GL_READ_ONLY                       : constant := 16#88B8#;
	GL_WRITE_ONLY                      : constant := 16#88B9#;
	GL_READ_WRITE                      : constant := 16#88BA#;
	GL_BUFFER_ACCESS                   : constant := 16#88BB#;
	GL_BUFFER_MAPPED                   : constant := 16#88BC#;
	GL_BUFFER_MAP_POINTER              : constant := 16#88BD#;
	GL_STREAM_DRAW                     : constant := 16#88E0#;
	GL_STREAM_READ                     : constant := 16#88E1#;
	GL_STREAM_COPY                     : constant := 16#88E2#;
	GL_STATIC_DRAW                     : constant := 16#88E4#;
	GL_STATIC_READ                     : constant := 16#88E5#;
	GL_STATIC_COPY                     : constant := 16#88E6#;
	GL_DYNAMIC_DRAW                    : constant := 16#88E8#;
	GL_DYNAMIC_READ                    : constant := 16#88E9#;
	GL_DYNAMIC_COPY                    : constant := 16#88EA#;
	GL_SAMPLES_PASSED                  : constant := 16#8914#;
	GL_SRC1_ALPHA                      : constant := 16#8589#;

	----------------
	-- OpenGL 2.0 --
	----------------
	GL_BLEND_EQUATION_RGB              : constant := 16#8009#;
	GL_VERTEX_ATTRIB_ARRAY_ENABLED     : constant := 16#8622#;
	GL_VERTEX_ATTRIB_ARRAY_SIZE        : constant := 16#8623#;
	GL_VERTEX_ATTRIB_ARRAY_STRIDE      : constant := 16#8624#;
	GL_VERTEX_ATTRIB_ARRAY_TYPE        : constant := 16#8625#;
	GL_CURRENT_VERTEX_ATTRIB           : constant := 16#8626#;
	GL_VERTEX_PROGRAM_POINT_SIZE       : constant := 16#8642#;
	GL_VERTEX_ATTRIB_ARRAY_POINTER     : constant := 16#8645#;
	GL_STENCIL_BACK_FUNC               : constant := 16#8800#;
	GL_STENCIL_BACK_FAIL               : constant := 16#8801#;
	GL_STENCIL_BACK_PASS_DEPTH_FAIL    : constant := 16#8802#;
	GL_STENCIL_BACK_PASS_DEPTH_PASS    : constant := 16#8803#;
	GL_MAX_DRAW_BUFFERS                : constant := 16#8824#;
	GL_DRAW_BUFFER0                    : constant := 16#8825#;
	GL_DRAW_BUFFER1                    : constant := 16#8826#;
	GL_DRAW_BUFFER2                    : constant := 16#8827#;
	GL_DRAW_BUFFER3                    : constant := 16#8828#;
	GL_DRAW_BUFFER4                    : constant := 16#8829#;
	GL_DRAW_BUFFER5                    : constant := 16#882A#;
	GL_DRAW_BUFFER6                    : constant := 16#882B#;
	GL_DRAW_BUFFER7                    : constant := 16#882C#;
	GL_DRAW_BUFFER8                    : constant := 16#882D#;
	GL_DRAW_BUFFER9                    : constant := 16#882E#;
	GL_DRAW_BUFFER10                   : constant := 16#882F#;
	GL_DRAW_BUFFER11                   : constant := 16#8830#;
	GL_DRAW_BUFFER12                   : constant := 16#8831#;
	GL_DRAW_BUFFER13                   : constant := 16#8832#;
	GL_DRAW_BUFFER14                   : constant := 16#8833#;
	GL_DRAW_BUFFER15                   : constant := 16#8834#;
	GL_BLEND_EQUATION_ALPHA            : constant := 16#883D#;
	GL_MAX_VERTEX_ATTRIBS              : constant := 16#8869#;
	GL_VERTEX_ATTRIB_ARRAY_NORMALIZED  : constant := 16#886A#;
	GL_MAX_TEXTURE_IMAGE_UNITS         : constant := 16#8872#;
	GL_FRAGMENT_SHADER                 : constant := 16#8B30#;
	GL_VERTEX_SHADER                   : constant := 16#8B31#;
	GL_MAX_FRAGMENT_UNIFORM_COMPONENTS : constant := 16#8B49#;
	GL_MAX_VERTEX_UNIFORM_COMPONENTS   : constant := 16#8B4A#;
	GL_MAX_VARYING_FLOATS              : constant := 16#8B4B#;
	GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS  : constant := 16#8B4C#;
	GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS  : constant := 16#8B4D#;
	GL_SHADER_TYPE                     : constant := 16#8B4F#;
	GL_FLOAT_VEC2                      : constant := 16#8B50#;
	GL_FLOAT_VEC3                      : constant := 16#8B51#;
	GL_FLOAT_VEC4                      : constant := 16#8B52#;
	GL_INT_VEC2                        : constant := 16#8B53#;
	GL_INT_VEC3                        : constant := 16#8B54#;
	GL_INT_VEC4                        : constant := 16#8B55#;
	GL_BOOL                            : constant := 16#8B56#;
	GL_BOOL_VEC2                       : constant := 16#8B57#;
	GL_BOOL_VEC3                       : constant := 16#8B58#;
	GL_BOOL_VEC4                       : constant := 16#8B59#;
	GL_FLOAT_MAT2                      : constant := 16#8B5A#;
	GL_FLOAT_MAT3                      : constant := 16#8B5B#;
	GL_FLOAT_MAT4                      : constant := 16#8B5C#;
	GL_SAMPLER_1D                      : constant := 16#8B5D#;
	GL_SAMPLER_2D                      : constant := 16#8B5E#;
	GL_SAMPLER_3D                      : constant := 16#8B5F#;
	GL_SAMPLER_CUBE                    : constant := 16#8B60#;
	GL_SAMPLER_1D_SHADOW               : constant := 16#8B61#;
	GL_SAMPLER_2D_SHADOW               : constant := 16#8B62#;
	GL_DELETE_STATUS                   : constant := 16#8B80#;
	GL_COMPILE_STATUS                  : constant := 16#8B81#;
	GL_LINK_STATUS                     : constant := 16#8B82#;
	GL_VALIDATE_STATUS                 : constant := 16#8B83#;
	GL_INFO_LOG_LENGTH                 : constant := 16#8B84#;
	GL_ATTACHED_SHADERS                : constant := 16#8B85#;
	GL_ACTIVE_UNIFORMS                 : constant := 16#8B86#;
	GL_ACTIVE_UNIFORM_MAX_LENGTH       : constant := 16#8B87#;
	GL_SHADER_SOURCE_LENGTH            : constant := 16#8B88#;
	GL_ACTIVE_ATTRIBUTES               : constant := 16#8B89#;
	GL_ACTIVE_ATTRIBUTE_MAX_LENGTH     : constant := 16#8B8A#;
	GL_FRAGMENT_SHADER_DERIVATIVE_HINT : constant := 16#8B8B#;
	GL_SHADING_LANGUAGE_VERSION        : constant := 16#8B8C#;
	GL_CURRENT_PROGRAM                 : constant := 16#8B8D#;
	GL_POINT_SPRITE_COORD_ORIGIN       : constant := 16#8CA0#;
	GL_LOWER_LEFT                      : constant := 16#8CA1#;
	GL_UPPER_LEFT                      : constant := 16#8CA2#;
	GL_STENCIL_BACK_REF                : constant := 16#8CA3#;
	GL_STENCIL_BACK_VALUE_MASK         : constant := 16#8CA4#;
	GL_STENCIL_BACK_WRITEMASK          : constant := 16#8CA5#;

	----------------
	-- OpenGL 2.1 --
	----------------
	GL_PIXEL_PACK_BUFFER               : constant := 16#88EB#;
	GL_PIXEL_UNPACK_BUFFER             : constant := 16#88EC#;
	GL_PIXEL_PACK_BUFFER_BINDING       : constant := 16#88ED#;
	GL_PIXEL_UNPACK_BUFFER_BINDING     : constant := 16#88EF#;
	GL_FLOAT_MAT2x3                    : constant := 16#8B65#;
	GL_FLOAT_MAT2x4                    : constant := 16#8B66#;
	GL_FLOAT_MAT3x2                    : constant := 16#8B67#;
	GL_FLOAT_MAT3x4                    : constant := 16#8B68#;
	GL_FLOAT_MAT4x2                    : constant := 16#8B69#;
	GL_FLOAT_MAT4x3                    : constant := 16#8B6A#;
	GL_SRGB                            : constant := 16#8C40#;
	GL_SRGB8                           : constant := 16#8C41#;
	GL_SRGB_ALPHA                      : constant := 16#8C42#;
	GL_SRGB8_ALPHA8                    : constant := 16#8C43#;
	GL_COMPRESSED_SRGB                 : constant := 16#8C48#;
	GL_COMPRESSED_SRGB_ALPHA           : constant := 16#8C49#;

	----------------
	-- OpenGL 3.0 --
	----------------
	GL_COMPARE_REF_TO_TEXTURE          : constant := 16#884E#;
	GL_CLIP_DISTANCE0                  : constant := 16#3000#;
	GL_CLIP_DISTANCE1                  : constant := 16#3001#;
	GL_CLIP_DISTANCE2                  : constant := 16#3002#;
	GL_CLIP_DISTANCE3                  : constant := 16#3003#;
	GL_CLIP_DISTANCE4                  : constant := 16#3004#;
	GL_CLIP_DISTANCE5                  : constant := 16#3005#;
	GL_CLIP_DISTANCE6                  : constant := 16#3006#;
	GL_CLIP_DISTANCE7                  : constant := 16#3007#;
	GL_MAX_CLIP_DISTANCES              : constant := 16#0D32#;
	GL_MAJOR_VERSION                   : constant := 16#821B#;
	GL_MINOR_VERSION                   : constant := 16#821C#;
	GL_NUM_EXTENSIONS                  : constant := 16#821D#;
	GL_CONTEXT_FLAGS                   : constant := 16#821E#;
	GL_COMPRESSED_RED                  : constant := 16#8225#;
	GL_COMPRESSED_RG                   : constant := 16#8226#;
	GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT  : constant := 16#0001#;
	GL_RGBA32F                         : constant := 16#8814#;
	GL_RGB32F                          : constant := 16#8815#;
	GL_RGBA16F                         : constant := 16#881A#;
	GL_RGB16F                          : constant := 16#881B#;
	GL_VERTEX_ATTRIB_ARRAY_INTEGER     : constant := 16#88FD#;
	GL_MAX_ARRAY_TEXTURE_LAYERS        : constant := 16#88FF#;
	GL_MIN_PROGRAM_TEXEL_OFFSET        : constant := 16#8904#;
	GL_MAX_PROGRAM_TEXEL_OFFSET        : constant := 16#8905#;
	GL_CLAMP_READ_COLOR                : constant := 16#891C#;
	GL_FIXED_ONLY                      : constant := 16#891D#;
	GL_MAX_VARYING_COMPONENTS          : constant := 16#8B4B#;
	GL_TEXTURE_1D_ARRAY                : constant := 16#8C18#;
	GL_PROXY_TEXTURE_1D_ARRAY          : constant := 16#8C19#;
	GL_TEXTURE_2D_ARRAY                : constant := 16#8C1A#;
	GL_PROXY_TEXTURE_2D_ARRAY          : constant := 16#8C1B#;
	GL_TEXTURE_BINDING_1D_ARRAY        : constant := 16#8C1C#;
	GL_TEXTURE_BINDING_2D_ARRAY        : constant := 16#8C1D#;
	GL_R11F_G11F_B10F                  : constant := 16#8C3A#;
	GL_UNSIGNED_INT_10F_11F_11F_REV    : constant := 16#8C3B#;
	GL_RGB9_E5                         : constant := 16#8C3D#;
	GL_UNSIGNED_INT_5_9_9_9_REV        : constant := 16#8C3E#;
	GL_TEXTURE_SHARED_SIZE             : constant := 16#8C3F#;
	GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH  : constant := 16#8C76#;
	GL_TRANSFORM_FEEDBACK_BUFFER_MODE  : constant := 16#8C7F#;
	GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS  : constant := 16#8C80#;
	GL_TRANSFORM_FEEDBACK_VARYINGS     : constant := 16#8C83#;
	GL_TRANSFORM_FEEDBACK_BUFFER_START  : constant := 16#8C84#;
	GL_TRANSFORM_FEEDBACK_BUFFER_SIZE  : constant := 16#8C85#;
	GL_PRIMITIVES_GENERATED            : constant := 16#8C87#;
	GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN  : constant := 16#8C88#;
	GL_RASTERIZER_DISCARD              : constant := 16#8C89#;
	GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS  : constant := 16#8C8A#;
	GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS  : constant := 16#8C8B#;
	GL_INTERLEAVED_ATTRIBS             : constant := 16#8C8C#;
	GL_SEPARATE_ATTRIBS                : constant := 16#8C8D#;
	GL_TRANSFORM_FEEDBACK_BUFFER       : constant := 16#8C8E#;
	GL_TRANSFORM_FEEDBACK_BUFFER_BINDING  : constant := 16#8C8F#;
	GL_RGBA32UI                        : constant := 16#8D70#;
	GL_RGB32UI                         : constant := 16#8D71#;
	GL_RGBA16UI                        : constant := 16#8D76#;
	GL_RGB16UI                         : constant := 16#8D77#;
	GL_RGBA8UI                         : constant := 16#8D7C#;
	GL_RGB8UI                          : constant := 16#8D7D#;
	GL_RGBA32I                         : constant := 16#8D82#;
	GL_RGB32I                          : constant := 16#8D83#;
	GL_RGBA16I                         : constant := 16#8D88#;
	GL_RGB16I                          : constant := 16#8D89#;
	GL_RGBA8I                          : constant := 16#8D8E#;
	GL_RGB8I                           : constant := 16#8D8F#;
	GL_RED_INTEGER                     : constant := 16#8D94#;
	GL_GREEN_INTEGER                   : constant := 16#8D95#;
	GL_BLUE_INTEGER                    : constant := 16#8D96#;
	GL_RGB_INTEGER                     : constant := 16#8D98#;
	GL_RGBA_INTEGER                    : constant := 16#8D99#;
	GL_BGR_INTEGER                     : constant := 16#8D9A#;
	GL_BGRA_INTEGER                    : constant := 16#8D9B#;
	GL_SAMPLER_1D_ARRAY                : constant := 16#8DC0#;
	GL_SAMPLER_2D_ARRAY                : constant := 16#8DC1#;
	GL_SAMPLER_1D_ARRAY_SHADOW         : constant := 16#8DC3#;
	GL_SAMPLER_2D_ARRAY_SHADOW         : constant := 16#8DC4#;
	GL_SAMPLER_CUBE_SHADOW             : constant := 16#8DC5#;
	GL_UNSIGNED_INT_VEC2               : constant := 16#8DC6#;
	GL_UNSIGNED_INT_VEC3               : constant := 16#8DC7#;
	GL_UNSIGNED_INT_VEC4               : constant := 16#8DC8#;
	GL_INT_SAMPLER_1D                  : constant := 16#8DC9#;
	GL_INT_SAMPLER_2D                  : constant := 16#8DCA#;
	GL_INT_SAMPLER_3D                  : constant := 16#8DCB#;
	GL_INT_SAMPLER_CUBE                : constant := 16#8DCC#;
	GL_INT_SAMPLER_1D_ARRAY            : constant := 16#8DCE#;
	GL_INT_SAMPLER_2D_ARRAY            : constant := 16#8DCF#;
	GL_UNSIGNED_INT_SAMPLER_1D         : constant := 16#8DD1#;
	GL_UNSIGNED_INT_SAMPLER_2D         : constant := 16#8DD2#;
	GL_UNSIGNED_INT_SAMPLER_3D         : constant := 16#8DD3#;
	GL_UNSIGNED_INT_SAMPLER_CUBE       : constant := 16#8DD4#;
	GL_UNSIGNED_INT_SAMPLER_1D_ARRAY   : constant := 16#8DD6#;
	GL_UNSIGNED_INT_SAMPLER_2D_ARRAY   : constant := 16#8DD7#;
	GL_QUERY_WAIT                      : constant := 16#8E13#;
	GL_QUERY_NO_WAIT                   : constant := 16#8E14#;
	GL_QUERY_BY_REGION_WAIT            : constant := 16#8E15#;
	GL_QUERY_BY_REGION_NO_WAIT         : constant := 16#8E16#;
	GL_BUFFER_ACCESS_FLAGS             : constant := 16#911F#;
	GL_BUFFER_MAP_LENGTH               : constant := 16#9120#;
	GL_BUFFER_MAP_OFFSET               : constant := 16#9121#;
	GL_DEPTH_COMPONENT32F              : constant := 16#8CAC#;
	GL_DEPTH32F_STENCIL8               : constant := 16#8CAD#;
	GL_FLOAT_32_UNSIGNED_INT_24_8_REV  : constant := 16#8DAD#;
	GL_INVALID_FRAMEBUFFER_OPERATION            : constant := 16#0506#;
	GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING    : constant := 16#8210#;
	GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE    : constant := 16#8211#;
	GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE          : constant := 16#8212#;
	GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE        : constant := 16#8213#;
	GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE         : constant := 16#8214#;
	GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE        : constant := 16#8215#;
	GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE        : constant := 16#8216#;
	GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE      : constant := 16#8217#;
	GL_FRAMEBUFFER_DEFAULT             : constant := 16#8218#;
	GL_FRAMEBUFFER_UNDEFINED           : constant := 16#8219#;
	GL_DEPTH_STENCIL_ATTACHMENT        : constant := 16#821A#;
	GL_MAX_RENDERBUFFER_SIZE           : constant := 16#84E8#;
	GL_DEPTH_STENCIL                   : constant := 16#84F9#;
	GL_UNSIGNED_INT_24_8               : constant := 16#84FA#;
	GL_DEPTH24_STENCIL8                : constant := 16#88F0#;
	GL_TEXTURE_STENCIL_SIZE            : constant := 16#88F1#;
	GL_TEXTURE_RED_TYPE                : constant := 16#8C10#;
	GL_TEXTURE_GREEN_TYPE              : constant := 16#8C11#;
	GL_TEXTURE_BLUE_TYPE               : constant := 16#8C12#;
	GL_TEXTURE_ALPHA_TYPE              : constant := 16#8C13#;
	GL_TEXTURE_DEPTH_TYPE              : constant := 16#8C16#;
	GL_UNSIGNED_NORMALIZED             : constant := 16#8C17#;
	GL_FRAMEBUFFER_BINDING             : constant := 16#8CA6#;
	GL_DRAW_FRAMEBUFFER_BINDING        : constant := GL_FRAMEBUFFER_BINDING;
	GL_RENDERBUFFER_BINDING            : constant := 16#8CA7#;
	GL_READ_FRAMEBUFFER                : constant := 16#8CA8#;
	GL_DRAW_FRAMEBUFFER                : constant := 16#8CA9#;
	GL_READ_FRAMEBUFFER_BINDING        : constant := 16#8CAA#;
	GL_RENDERBUFFER_SAMPLES            : constant := 16#8CAB#;
	GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE  : constant := 16#8CD0#;
	GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME  : constant := 16#8CD1#;
	GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL  : constant := 16#8CD2#;
	GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE  : constant := 16#8CD3#;
	GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER  : constant := 16#8CD4#;
	GL_FRAMEBUFFER_COMPLETE            : constant := 16#8CD5#;
	GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT  : constant := 16#8CD6#;
	GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT  : constant := 16#8CD7#;
	GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER  : constant := 16#8CDB#;
	GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER  : constant := 16#8CDC#;
	GL_FRAMEBUFFER_UNSUPPORTED         : constant := 16#8CDD#;
	GL_MAX_COLOR_ATTACHMENTS           : constant := 16#8CDF#;
	GL_COLOR_ATTACHMENT0               : constant := 16#8CE0#;
	GL_COLOR_ATTACHMENT1               : constant := 16#8CE1#;
	GL_COLOR_ATTACHMENT2               : constant := 16#8CE2#;
	GL_COLOR_ATTACHMENT3               : constant := 16#8CE3#;
	GL_COLOR_ATTACHMENT4               : constant := 16#8CE4#;
	GL_COLOR_ATTACHMENT5               : constant := 16#8CE5#;
	GL_COLOR_ATTACHMENT6               : constant := 16#8CE6#;
	GL_COLOR_ATTACHMENT7               : constant := 16#8CE7#;
	GL_COLOR_ATTACHMENT8               : constant := 16#8CE8#;
	GL_COLOR_ATTACHMENT9               : constant := 16#8CE9#;
	GL_COLOR_ATTACHMENT10              : constant := 16#8CEA#;
	GL_COLOR_ATTACHMENT11              : constant := 16#8CEB#;
	GL_COLOR_ATTACHMENT12              : constant := 16#8CEC#;
	GL_COLOR_ATTACHMENT13              : constant := 16#8CED#;
	GL_COLOR_ATTACHMENT14              : constant := 16#8CEE#;
	GL_COLOR_ATTACHMENT15              : constant := 16#8CEF#;
	GL_DEPTH_ATTACHMENT                : constant := 16#8D00#;
	GL_STENCIL_ATTACHMENT              : constant := 16#8D20#;
	GL_FRAMEBUFFER                     : constant := 16#8D40#;
	GL_RENDERBUFFER                    : constant := 16#8D41#;
	GL_RENDERBUFFER_WIDTH              : constant := 16#8D42#;
	GL_RENDERBUFFER_HEIGHT             : constant := 16#8D43#;
	GL_RENDERBUFFER_INTERNAL_FORMAT    : constant := 16#8D44#;
	GL_STENCIL_INDEX1                  : constant := 16#8D46#;
	GL_STENCIL_INDEX4                  : constant := 16#8D47#;
	GL_STENCIL_INDEX8                  : constant := 16#8D48#;
	GL_STENCIL_INDEX16                 : constant := 16#8D49#;
	GL_RENDERBUFFER_RED_SIZE           : constant := 16#8D50#;
	GL_RENDERBUFFER_GREEN_SIZE         : constant := 16#8D51#;
	GL_RENDERBUFFER_BLUE_SIZE          : constant := 16#8D52#;
	GL_RENDERBUFFER_ALPHA_SIZE         : constant := 16#8D53#;
	GL_RENDERBUFFER_DEPTH_SIZE         : constant := 16#8D54#;
	GL_RENDERBUFFER_STENCIL_SIZE       : constant := 16#8D55#;
	GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE  : constant := 16#8D56#;
	GL_MAX_SAMPLES                     : constant := 16#8D57#;
	GL_FRAMEBUFFER_SRGB                : constant := 16#8DB9#;
	GL_HALF_FLOAT                      : constant := 16#140B#;
	GL_MAP_READ_BIT                    : constant := 16#0001#;
	GL_MAP_WRITE_BIT                   : constant := 16#0002#;
	GL_MAP_INVALIDATE_RANGE_BIT        : constant := 16#0004#;
	GL_MAP_INVALIDATE_BUFFER_BIT       : constant := 16#0008#;
	GL_MAP_FLUSH_EXPLICIT_BIT          : constant := 16#0010#;
	GL_MAP_UNSYNCHRONIZED_BIT          : constant := 16#0020#;
	GL_COMPRESSED_RED_RGTC1            : constant := 16#8DBB#;
	GL_COMPRESSED_SIGNED_RED_RGTC1     : constant := 16#8DBC#;
	GL_COMPRESSED_RG_RGTC2             : constant := 16#8DBD#;
	GL_COMPRESSED_SIGNED_RG_RGTC2      : constant := 16#8DBE#;
	GL_RG                              : constant := 16#8227#;
	GL_RG_INTEGER                      : constant := 16#8228#;
	GL_R8                              : constant := 16#8229#;
	GL_R16                             : constant := 16#822A#;
	GL_RG8                             : constant := 16#822B#;
	GL_RG16                            : constant := 16#822C#;
	GL_R16F                            : constant := 16#822D#;
	GL_R32F                            : constant := 16#822E#;
	GL_RG16F                           : constant := 16#822F#;
	GL_RG32F                           : constant := 16#8230#;
	GL_R8I                             : constant := 16#8231#;
	GL_R8UI                            : constant := 16#8232#;
	GL_R16I                            : constant := 16#8233#;
	GL_R16UI                           : constant := 16#8234#;
	GL_R32I                            : constant := 16#8235#;
	GL_R32UI                           : constant := 16#8236#;
	GL_RG8I                            : constant := 16#8237#;
	GL_RG8UI                           : constant := 16#8238#;
	GL_RG16I                           : constant := 16#8239#;
	GL_RG16UI                          : constant := 16#823A#;
	GL_RG32I                           : constant := 16#823B#;
	GL_RG32UI                          : constant := 16#823C#;
	GL_VERTEX_ARRAY_BINDING            : constant := 16#85B5#;

	----------------
	-- OpenGL 3.1 --
	----------------
	GL_SAMPLER_2D_RECT                 : constant := 16#8B63#;
	GL_SAMPLER_2D_RECT_SHADOW          : constant := 16#8B64#;
	GL_SAMPLER_BUFFER                  : constant := 16#8DC2#;
	GL_INT_SAMPLER_2D_RECT             : constant := 16#8DCD#;
	GL_INT_SAMPLER_BUFFER              : constant := 16#8DD0#;
	GL_UNSIGNED_INT_SAMPLER_2D_RECT    : constant := 16#8DD5#;
	GL_UNSIGNED_INT_SAMPLER_BUFFER     : constant := 16#8DD8#;
	GL_TEXTURE_BUFFER                  : constant := 16#8C2A#;
	GL_MAX_TEXTURE_BUFFER_SIZE         : constant := 16#8C2B#;
	GL_TEXTURE_BINDING_BUFFER          : constant := 16#8C2C#;
	GL_TEXTURE_BUFFER_DATA_STORE_BINDING  : constant := 16#8C2D#;
	GL_TEXTURE_BUFFER_FORMAT           : constant := 16#8C2E#;
	GL_TEXTURE_RECTANGLE               : constant := 16#84F5#;
	GL_TEXTURE_BINDING_RECTANGLE       : constant := 16#84F6#;
	GL_PROXY_TEXTURE_RECTANGLE         : constant := 16#84F7#;
	GL_MAX_RECTANGLE_TEXTURE_SIZE      : constant := 16#84F8#;
	GL_RED_SNORM                       : constant := 16#8F90#;
	GL_RG_SNORM                        : constant := 16#8F91#;
	GL_RGB_SNORM                       : constant := 16#8F92#;
	GL_RGBA_SNORM                      : constant := 16#8F93#;
	GL_R8_SNORM                        : constant := 16#8F94#;
	GL_RG8_SNORM                       : constant := 16#8F95#;
	GL_RGB8_SNORM                      : constant := 16#8F96#;
	GL_RGBA8_SNORM                     : constant := 16#8F97#;
	GL_R16_SNORM                       : constant := 16#8F98#;
	GL_RG16_SNORM                      : constant := 16#8F99#;
	GL_RGB16_SNORM                     : constant := 16#8F9A#;
	GL_RGBA16_SNORM                    : constant := 16#8F9B#;
	GL_SIGNED_NORMALIZED               : constant := 16#8F9C#;
	GL_PRIMITIVE_RESTART               : constant := 16#8F9D#;
	GL_PRIMITIVE_RESTART_INDEX         : constant := 16#8F9E#;
	GL_COPY_READ_BUFFER                : constant := 16#8F36#;
	GL_COPY_WRITE_BUFFER               : constant := 16#8F37#;
	GL_UNIFORM_BUFFER                  : constant := 16#8A11#;
	GL_UNIFORM_BUFFER_BINDING          : constant := 16#8A28#;
	GL_UNIFORM_BUFFER_START            : constant := 16#8A29#;
	GL_UNIFORM_BUFFER_SIZE             : constant := 16#8A2A#;
	GL_MAX_VERTEX_UNIFORM_BLOCKS       : constant := 16#8A2B#;
	GL_MAX_FRAGMENT_UNIFORM_BLOCKS     : constant := 16#8A2D#;
	GL_MAX_COMBINED_UNIFORM_BLOCKS     : constant := 16#8A2E#;
	GL_MAX_UNIFORM_BUFFER_BINDINGS     : constant := 16#8A2F#;
	GL_MAX_UNIFORM_BLOCK_SIZE          : constant := 16#8A30#;
	GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS  : constant := 16#8A31#;
	GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS  : constant := 16#8A33#;
	GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT  : constant := 16#8A34#;
	GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH  : constant := 16#8A35#;
	GL_ACTIVE_UNIFORM_BLOCKS           : constant := 16#8A36#;
	GL_UNIFORM_TYPE                    : constant := 16#8A37#;
	GL_UNIFORM_SIZE                    : constant := 16#8A38#;
	GL_UNIFORM_NAME_LENGTH             : constant := 16#8A39#;
	GL_UNIFORM_BLOCK_INDEX             : constant := 16#8A3A#;
	GL_UNIFORM_OFFSET                  : constant := 16#8A3B#;
	GL_UNIFORM_ARRAY_STRIDE            : constant := 16#8A3C#;
	GL_UNIFORM_MATRIX_STRIDE           : constant := 16#8A3D#;
	GL_UNIFORM_IS_ROW_MAJOR            : constant := 16#8A3E#;
	GL_UNIFORM_BLOCK_BINDING           : constant := 16#8A3F#;
	GL_UNIFORM_BLOCK_DATA_SIZE         : constant := 16#8A40#;
	GL_UNIFORM_BLOCK_NAME_LENGTH       : constant := 16#8A41#;
	GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS   : constant := 16#8A42#;
	GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES  : constant := 16#8A43#;
	GL_UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER  : constant := 16#8A44#;
	GL_UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER  : constant := 16#8A46#;
	GL_INVALID_INDEX                   : constant := 16#FFFFFFFF#;

	----------------
	-- OpenGL 3.2 --
	----------------
	GL_CONTEXT_CORE_PROFILE_BIT        : constant := 16#00000001#;
	GL_CONTEXT_COMPATIBILITY_PROFILE_BIT  : constant := 16#00000002#;
	GL_LINES_ADJACENCY                 : constant := 16#000A#;
	GL_LINE_STRIP_ADJACENCY            : constant := 16#000B#;
	GL_TRIANGLES_ADJACENCY             : constant := 16#000C#;
	GL_TRIANGLE_STRIP_ADJACENCY        : constant := 16#000D#;
	GL_PROGRAM_POINT_SIZE              : constant := 16#8642#;
	GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS  : constant := 16#8C29#;
	GL_FRAMEBUFFER_ATTACHMENT_LAYERED  : constant := 16#8DA7#;
	GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS  : constant := 16#8DA8#;
	GL_GEOMETRY_SHADER                 : constant := 16#8DD9#;
	GL_GEOMETRY_VERTICES_OUT           : constant := 16#8916#;
	GL_GEOMETRY_INPUT_TYPE             : constant := 16#8917#;
	GL_GEOMETRY_OUTPUT_TYPE            : constant := 16#8918#;
	GL_MAX_GEOMETRY_UNIFORM_COMPONENTS  : constant := 16#8DDF#;
	GL_MAX_GEOMETRY_OUTPUT_VERTICES    : constant := 16#8DE0#;
	GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS  : constant := 16#8DE1#;
	GL_MAX_VERTEX_OUTPUT_COMPONENTS    : constant := 16#9122#;
	GL_MAX_GEOMETRY_INPUT_COMPONENTS   : constant := 16#9123#;
	GL_MAX_GEOMETRY_OUTPUT_COMPONENTS  : constant := 16#9124#;
	GL_MAX_FRAGMENT_INPUT_COMPONENTS   : constant := 16#9125#;
	GL_CONTEXT_PROFILE_MASK            : constant := 16#9126#;
	GL_DEPTH_CLAMP                     : constant := 16#864F#;
	GL_QUADS_FOLLOW_PROVOKING_VERTEX_CONVENTION  : constant := 16#8E4C#;
	GL_FIRST_VERTEX_CONVENTION         : constant := 16#8E4D#;
	GL_LAST_VERTEX_CONVENTION          : constant := 16#8E4E#;
	GL_PROVOKING_VERTEX                : constant := 16#8E4F#;
	GL_TEXTURE_CUBE_MAP_SEAMLESS       : constant := 16#884F#;
	GL_MAX_SERVER_WAIT_TIMEOUT         : constant := 16#9111#;
	GL_OBJECT_TYPE                     : constant := 16#9112#;
	GL_SYNC_CONDITION                  : constant := 16#9113#;
	GL_SYNC_STATUS                     : constant := 16#9114#;
	GL_SYNC_FLAGS                      : constant := 16#9115#;
	GL_SYNC_FENCE                      : constant := 16#9116#;
	GL_SYNC_GPU_COMMANDS_COMPLETE      : constant := 16#9117#;
	GL_UNSIGNALED                      : constant := 16#9118#;
	GL_SIGNALED                        : constant := 16#9119#;
	GL_ALREADY_SIGNALED                : constant := 16#911A#;
	GL_TIMEOUT_EXPIRED                 : constant := 16#911B#;
	GL_CONDITION_SATISFIED             : constant := 16#911C#;
	GL_WAIT_FAILED                     : constant := 16#911D#;
	GL_SYNC_FLUSH_COMMANDS_BIT         : constant := 16#00000001#;
	GL_TIMEOUT_IGNORED                 : constant := 16#FFFFFFFFFFFFFFFF#;
	GL_SAMPLE_POSITION                 : constant := 16#8E50#;
	GL_SAMPLE_MASK                     : constant := 16#8E51#;
	GL_SAMPLE_MASK_VALUE               : constant := 16#8E52#;
	GL_MAX_SAMPLE_MASK_WORDS           : constant := 16#8E59#;
	GL_TEXTURE_2D_MULTISAMPLE          : constant := 16#9100#;
	GL_PROXY_TEXTURE_2D_MULTISAMPLE    : constant := 16#9101#;
	GL_TEXTURE_2D_MULTISAMPLE_ARRAY    : constant := 16#9102#;
	GL_PROXY_TEXTURE_2D_MULTISAMPLE_ARRAY  : constant := 16#9103#;
	GL_TEXTURE_BINDING_2D_MULTISAMPLE  : constant := 16#9104#;
	GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY  : constant := 16#9105#;
	GL_TEXTURE_SAMPLES                 : constant := 16#9106#;
	GL_TEXTURE_FIXED_SAMPLE_LOCATIONS  : constant := 16#9107#;
	GL_SAMPLER_2D_MULTISAMPLE          : constant := 16#9108#;
	GL_INT_SAMPLER_2D_MULTISAMPLE      : constant := 16#9109#;
	GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE  : constant := 16#910A#;
	GL_SAMPLER_2D_MULTISAMPLE_ARRAY    : constant := 16#910B#;
	GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY  : constant := 16#910C#;
	GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY  : constant := 16#910D#;
	GL_MAX_COLOR_TEXTURE_SAMPLES       : constant := 16#910E#;
	GL_MAX_DEPTH_TEXTURE_SAMPLES       : constant := 16#910F#;
	GL_MAX_INTEGER_SAMPLES             : constant := 16#9110#;

	----------------
	-- OpenGL 3.3 --
	----------------
	GL_VERTEX_ATTRIB_ARRAY_DIVISOR     : constant := 16#88FE#;
	GL_SRC1_COLOR                      : constant := 16#88F9#;
	GL_ONE_MINUS_SRC1_COLOR            : constant := 16#88FA#;
	GL_ONE_MINUS_SRC1_ALPHA            : constant := 16#88FB#;
	GL_MAX_DUAL_SOURCE_DRAW_BUFFERS    : constant := 16#88FC#;
	GL_ANY_SAMPLES_PASSED              : constant := 16#8C2F#;
	GL_SAMPLER_BINDING                 : constant := 16#8919#;
	GL_RGB10_A2UI                      : constant := 16#906F#;
	GL_TEXTURE_SWIZZLE_R               : constant := 16#8E42#;
	GL_TEXTURE_SWIZZLE_G               : constant := 16#8E43#;
	GL_TEXTURE_SWIZZLE_B               : constant := 16#8E44#;
	GL_TEXTURE_SWIZZLE_A               : constant := 16#8E45#;
	GL_TEXTURE_SWIZZLE_RGBA            : constant := 16#8E46#;
	GL_TIME_ELAPSED                    : constant := 16#88BF#;
	GL_TIMESTAMP                       : constant := 16#8E28#;
	GL_INT_2_10_10_10_REV              : constant := 16#8D9F#;

	----------------
	-- OpenGL 4.0 --
	----------------
	GL_SAMPLE_SHADING                  : constant := 16#8C36#;
	GL_MIN_SAMPLE_SHADING_VALUE        : constant := 16#8C37#;
	GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET  : constant := 16#8E5E#;
	GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET  : constant := 16#8E5F#;
	GL_TEXTURE_CUBE_MAP_ARRAY          : constant := 16#9009#;
	GL_TEXTURE_BINDING_CUBE_MAP_ARRAY  : constant := 16#900A#;
	GL_PROXY_TEXTURE_CUBE_MAP_ARRAY    : constant := 16#900B#;
	GL_SAMPLER_CUBE_MAP_ARRAY          : constant := 16#900C#;
	GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW   : constant := 16#900D#;
	GL_INT_SAMPLER_CUBE_MAP_ARRAY      : constant := 16#900E#;
	GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY  : constant := 16#900F#;
	GL_DRAW_INDIRECT_BUFFER            : constant := 16#8F3F#;
	GL_DRAW_INDIRECT_BUFFER_BINDING    : constant := 16#8F43#;
	GL_GEOMETRY_SHADER_INVOCATIONS          : constant := 16#887F#;
	GL_MAX_GEOMETRY_SHADER_INVOCATIONS      : constant := 16#8E5A#;
	GL_MIN_FRAGMENT_INTERPOLATION_OFFSET    : constant := 16#8E5B#;
	GL_MAX_FRAGMENT_INTERPOLATION_OFFSET    : constant := 16#8E5C#;
	GL_FRAGMENT_INTERPOLATION_OFFSET_BITS   : constant := 16#8E5D#;
	GL_MAX_TRANSFORM_FEEDBACK_BUFFERS  : constant := 16#8E70#;
	GL_MAX_VERTEX_STREAMS              : constant := 16#8E71#;
	GL_DOUBLE_VEC2                     : constant := 16#8FFC#;
	GL_DOUBLE_VEC3                     : constant := 16#8FFD#;
	GL_DOUBLE_VEC4                     : constant := 16#8FFE#;
	GL_DOUBLE_MAT2                     : constant := 16#8F46#;
	GL_DOUBLE_MAT3                     : constant := 16#8F47#;
	GL_DOUBLE_MAT4                     : constant := 16#8F48#;
	GL_DOUBLE_MAT2x3                   : constant := 16#8F49#;
	GL_DOUBLE_MAT2x4                   : constant := 16#8F4A#;
	GL_DOUBLE_MAT3x2                   : constant := 16#8F4B#;
	GL_DOUBLE_MAT3x4                   : constant := 16#8F4C#;
	GL_DOUBLE_MAT4x2                   : constant := 16#8F4D#;
	GL_DOUBLE_MAT4x3                   : constant := 16#8F4E#;
	GL_ACTIVE_SUBROUTINES              : constant := 16#8DE5#;
	GL_ACTIVE_SUBROUTINE_UNIFORMS      : constant := 16#8DE6#;
	GL_ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS  : constant := 16#8E47#;
	GL_ACTIVE_SUBROUTINE_MAX_LENGTH    : constant := 16#8E48#;
	GL_ACTIVE_SUBROUTINE_UNIFORM_MAX_LENGTH  : constant := 16#8E49#;
	GL_MAX_SUBROUTINES                 : constant := 16#8DE7#;
	GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS  : constant := 16#8DE8#;
	GL_NUM_COMPATIBLE_SUBROUTINES      : constant := 16#8E4A#;
	GL_COMPATIBLE_SUBROUTINES          : constant := 16#8E4B#;
	GL_PATCHES                         : constant := 16#000E#;
	GL_PATCH_VERTICES                  : constant := 16#8E72#;
	GL_PATCH_DEFAULT_INNER_LEVEL       : constant := 16#8E73#;
	GL_PATCH_DEFAULT_OUTER_LEVEL       : constant := 16#8E74#;
	GL_TESS_CONTROL_OUTPUT_VERTICES    : constant := 16#8E75#;
	GL_TESS_GEN_MODE                   : constant := 16#8E76#;
	GL_TESS_GEN_SPACING                : constant := 16#8E77#;
	GL_TESS_GEN_VERTEX_ORDER           : constant := 16#8E78#;
	GL_TESS_GEN_POINT_MODE             : constant := 16#8E79#;
	GL_ISOLINES                        : constant := 16#8E7A#;
	GL_FRACTIONAL_ODD                  : constant := 16#8E7B#;
	GL_FRACTIONAL_EVEN                 : constant := 16#8E7C#;
	GL_MAX_PATCH_VERTICES              : constant := 16#8E7D#;
	GL_MAX_TESS_GEN_LEVEL              : constant := 16#8E7E#;
	GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS  : constant := 16#8E7F#;
	GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS  : constant := 16#8E80#;
	GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS  : constant := 16#8E81#;
	GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS  : constant := 16#8E82#;
	GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS  : constant := 16#8E83#;
	GL_MAX_TESS_PATCH_COMPONENTS       : constant := 16#8E84#;
	GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS  : constant := 16#8E85#;
	GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS  : constant := 16#8E86#;
	GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS  : constant := 16#8E89#;
	GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS  : constant := 16#8E8A#;
	GL_MAX_TESS_CONTROL_INPUT_COMPONENTS  : constant := 16#886C#;
	GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS  : constant := 16#886D#;
	GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS  : constant := 16#8E1E#;
	GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS  : constant := 16#8E1F#;
	GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_CONTROL_SHADER  : constant := 16#84F0#;
	GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_EVALUATION_SHADER  : constant := 16#84F1#;
	GL_TESS_EVALUATION_SHADER          : constant := 16#8E87#;
	GL_TESS_CONTROL_SHADER             : constant := 16#8E88#;
	GL_TRANSFORM_FEEDBACK               : constant := 16#8E22#;
	GL_TRANSFORM_FEEDBACK_BUFFER_PAUSED : constant := 16#8E23#;
	GL_TRANSFORM_FEEDBACK_BUFFER_ACTIVE : constant := 16#8E24#;
	GL_TRANSFORM_FEEDBACK_BINDING       : constant := 16#8E25#;

	----------------
	-- OpenGL 4.1 --
	----------------
	GL_FIXED                           : constant := 16#140C#;
	GL_IMPLEMENTATION_COLOR_READ_TYPE  : constant := 16#8B9A#;
	GL_IMPLEMENTATION_COLOR_READ_FORMAT  : constant := 16#8B9B#;
	GL_LOW_FLOAT                       : constant := 16#8DF0#;
	GL_MEDIUM_FLOAT                    : constant := 16#8DF1#;
	GL_HIGH_FLOAT                      : constant := 16#8DF2#;
	GL_LOW_INT                         : constant := 16#8DF3#;
	GL_MEDIUM_INT                      : constant := 16#8DF4#;
	GL_HIGH_INT                        : constant := 16#8DF5#;
	GL_SHADER_COMPILER                 : constant := 16#8DFA#;
	GL_NUM_SHADER_BINARY_FORMATS       : constant := 16#8DF9#;
	GL_MAX_VERTEX_UNIFORM_VECTORS      : constant := 16#8DFB#;
	GL_MAX_VARYING_VECTORS             : constant := 16#8DFC#;
	GL_MAX_FRAGMENT_UNIFORM_VECTORS    : constant := 16#8DFD#;
	GL_RGB565                          : constant := 16#8D62#;
	GL_PROGRAM_BINARY_RETRIEVABLE_HINT : constant := 16#8257#;
	GL_PROGRAM_BINARY_LENGTH           : constant := 16#8741#;
	GL_NUM_PROGRAM_BINARY_FORMATS      : constant := 16#87FE#;
	GL_PROGRAM_BINARY_FORMATS          : constant := 16#87FF#;
	GL_VERTEX_SHADER_BIT               : constant := 16#00000001#;
	GL_FRAGMENT_SHADER_BIT             : constant := 16#00000002#;
	GL_GEOMETRY_SHADER_BIT             : constant := 16#00000004#;
	GL_TESS_CONTROL_SHADER_BIT         : constant := 16#00000008#;
	GL_TESS_EVALUATION_SHADER_BIT      : constant := 16#00000010#;
	GL_ALL_SHADER_BITS                 : constant := 16#FFFFFFFF#;
	GL_PROGRAM_SEPARABLE               : constant := 16#8258#;
	GL_ACTIVE_PROGRAM                  : constant := 16#8259#;
	GL_PROGRAM_PIPELINE_BINDING        : constant := 16#825A#;
	GL_MAX_VIEWPORTS                    : constant := 16#825B#;
	GL_VIEWPORT_SUBPIXEL_BITS           : constant := 16#825C#;
	GL_VIEWPORT_BOUNDS_RANGE            : constant := 16#825D#;
	GL_LAYER_PROVOKING_VERTEX           : constant := 16#825E#;
	GL_VIEWPORT_INDEX_PROVOKING_VERTEX  : constant := 16#825F#;
	GL_UNDEFINED_VERTEX                 : constant := 16#8260#;

	----------------
	-- OpenGL 4.2 --
	----------------
	GL_UNPACK_COMPRESSED_BLOCK_WIDTH   : constant := 16#9127#;
	GL_UNPACK_COMPRESSED_BLOCK_HEIGHT  : constant := 16#9128#;
	GL_UNPACK_COMPRESSED_BLOCK_DEPTH   : constant := 16#9129#;
	GL_UNPACK_COMPRESSED_BLOCK_SIZE    : constant := 16#912A#;
	GL_PACK_COMPRESSED_BLOCK_WIDTH     : constant := 16#912B#;
	GL_PACK_COMPRESSED_BLOCK_HEIGHT    : constant := 16#912C#;
	GL_PACK_COMPRESSED_BLOCK_DEPTH     : constant := 16#912D#;
	GL_PACK_COMPRESSED_BLOCK_SIZE      : constant := 16#912E#;
	GL_NUM_SAMPLE_COUNTS               : constant := 16#9380#;
	GL_MIN_MAP_BUFFER_ALIGNMENT        : constant := 16#90BC#;
	GL_ATOMIC_COUNTER_BUFFER           : constant := 16#92C0#;
	GL_ATOMIC_COUNTER_BUFFER_BINDING   : constant := 16#92C1#;
	GL_ATOMIC_COUNTER_BUFFER_START     : constant := 16#92C2#;
	GL_ATOMIC_COUNTER_BUFFER_SIZE      : constant := 16#92C3#;
	GL_ATOMIC_COUNTER_BUFFER_DATA_SIZE  : constant := 16#92C4#;
	GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTERS  : constant := 16#92C5#;
	GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTER_INDICES  : constant := 16#92C6#;
	GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_VERTEX_SHADER  : constant := 16#92C7#;
	GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_CONTROL_SHADER  : constant := 16#92C8#;
	GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_EVALUATION_SHADER  : constant := 16#92C9#;
	GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_GEOMETRY_SHADER  : constant := 16#92CA#;
	GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_FRAGMENT_SHADER  : constant := 16#92CB#;
	GL_MAX_VERTEX_ATOMIC_COUNTER_BUFFERS  : constant := 16#92CC#;
	GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS  : constant := 16#92CD#;
	GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS  : constant := 16#92CE#;
	GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS  : constant := 16#92CF#;
	GL_MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS  : constant := 16#92D0#;
	GL_MAX_COMBINED_ATOMIC_COUNTER_BUFFERS  : constant := 16#92D1#;
	GL_MAX_VERTEX_ATOMIC_COUNTERS      : constant := 16#92D2#;
	GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS  : constant := 16#92D3#;
	GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS  : constant := 16#92D4#;
	GL_MAX_GEOMETRY_ATOMIC_COUNTERS    : constant := 16#92D5#;
	GL_MAX_FRAGMENT_ATOMIC_COUNTERS    : constant := 16#92D6#;
	GL_MAX_COMBINED_ATOMIC_COUNTERS    : constant := 16#92D7#;
	GL_MAX_ATOMIC_COUNTER_BUFFER_SIZE  : constant := 16#92D8#;
	GL_MAX_ATOMIC_COUNTER_BUFFER_BINDINGS  : constant := 16#92DC#;
	GL_ACTIVE_ATOMIC_COUNTER_BUFFERS   : constant := 16#92D9#;
	GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX  : constant := 16#92DA#;
	GL_UNSIGNED_INT_ATOMIC_COUNTER     : constant := 16#92DB#;
	GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT  : constant := 16#00000001#;
	GL_ELEMENT_ARRAY_BARRIER_BIT       : constant := 16#00000002#;
	GL_UNIFORM_BARRIER_BIT             : constant := 16#00000004#;
	GL_TEXTURE_FETCH_BARRIER_BIT       : constant := 16#00000008#;
	GL_SHADER_IMAGE_ACCESS_BARRIER_BIT  : constant := 16#00000020#;
	GL_COMMAND_BARRIER_BIT             : constant := 16#00000040#;
	GL_PIXEL_BUFFER_BARRIER_BIT        : constant := 16#00000080#;
	GL_TEXTURE_UPDATE_BARRIER_BIT      : constant := 16#00000100#;
	GL_BUFFER_UPDATE_BARRIER_BIT       : constant := 16#00000200#;
	GL_FRAMEBUFFER_BARRIER_BIT         : constant := 16#00000400#;
	GL_TRANSFORM_FEEDBACK_BARRIER_BIT  : constant := 16#00000800#;
	GL_ATOMIC_COUNTER_BARRIER_BIT      : constant := 16#00001000#;
	GL_ALL_BARRIER_BITS                : constant := 16#FFFFFFFF#;
	GL_MAX_IMAGE_UNITS                 : constant := 16#8F38#;
	GL_MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS  : constant := 16#8F39#;
	GL_IMAGE_BINDING_NAME              : constant := 16#8F3A#;
	GL_IMAGE_BINDING_LEVEL             : constant := 16#8F3B#;
	GL_IMAGE_BINDING_LAYERED           : constant := 16#8F3C#;
	GL_IMAGE_BINDING_LAYER             : constant := 16#8F3D#;
	GL_IMAGE_BINDING_ACCESS            : constant := 16#8F3E#;
	GL_IMAGE_1D                        : constant := 16#904C#;
	GL_IMAGE_2D                        : constant := 16#904D#;
	GL_IMAGE_3D                        : constant := 16#904E#;
	GL_IMAGE_2D_RECT                   : constant := 16#904F#;
	GL_IMAGE_CUBE                      : constant := 16#9050#;
	GL_IMAGE_BUFFER                    : constant := 16#9051#;
	GL_IMAGE_1D_ARRAY                  : constant := 16#9052#;
	GL_IMAGE_2D_ARRAY                  : constant := 16#9053#;
	GL_IMAGE_CUBE_MAP_ARRAY            : constant := 16#9054#;
	GL_IMAGE_2D_MULTISAMPLE            : constant := 16#9055#;
	GL_IMAGE_2D_MULTISAMPLE_ARRAY      : constant := 16#9056#;
	GL_INT_IMAGE_1D                    : constant := 16#9057#;
	GL_INT_IMAGE_2D                    : constant := 16#9058#;
	GL_INT_IMAGE_3D                    : constant := 16#9059#;
	GL_INT_IMAGE_2D_RECT               : constant := 16#905A#;
	GL_INT_IMAGE_CUBE                  : constant := 16#905B#;
	GL_INT_IMAGE_BUFFER                : constant := 16#905C#;
	GL_INT_IMAGE_1D_ARRAY              : constant := 16#905D#;
	GL_INT_IMAGE_2D_ARRAY              : constant := 16#905E#;
	GL_INT_IMAGE_CUBE_MAP_ARRAY        : constant := 16#905F#;
	GL_INT_IMAGE_2D_MULTISAMPLE        : constant := 16#9060#;
	GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY  : constant := 16#9061#;
	GL_UNSIGNED_INT_IMAGE_1D           : constant := 16#9062#;
	GL_UNSIGNED_INT_IMAGE_2D           : constant := 16#9063#;
	GL_UNSIGNED_INT_IMAGE_3D           : constant := 16#9064#;
	GL_UNSIGNED_INT_IMAGE_2D_RECT      : constant := 16#9065#;
	GL_UNSIGNED_INT_IMAGE_CUBE         : constant := 16#9066#;
	GL_UNSIGNED_INT_IMAGE_BUFFER       : constant := 16#9067#;
	GL_UNSIGNED_INT_IMAGE_1D_ARRAY     : constant := 16#9068#;
	GL_UNSIGNED_INT_IMAGE_2D_ARRAY     : constant := 16#9069#;
	GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY  : constant := 16#906A#;
	GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE  : constant := 16#906B#;
	GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY  : constant := 16#906C#;
	GL_MAX_IMAGE_SAMPLES               : constant := 16#906D#;
	GL_IMAGE_BINDING_FORMAT            : constant := 16#906E#;
	GL_IMAGE_FORMAT_COMPATIBILITY_TYPE  : constant := 16#90C7#;
	GL_IMAGE_FORMAT_COMPATIBILITY_BY_SIZE  : constant := 16#90C8#;
	GL_IMAGE_FORMAT_COMPATIBILITY_BY_CLASS  : constant := 16#90C9#;
	GL_MAX_VERTEX_IMAGE_UNIFORMS       : constant := 16#90CA#;
	GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS  : constant := 16#90CB#;
	GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS  : constant := 16#90CC#;
	GL_MAX_GEOMETRY_IMAGE_UNIFORMS     : constant := 16#90CD#;
	GL_MAX_FRAGMENT_IMAGE_UNIFORMS     : constant := 16#90CE#;
	GL_MAX_COMBINED_IMAGE_UNIFORMS     : constant := 16#90CF#;
	GL_TEXTURE_IMMUTABLE_FORMAT        : constant := 16#912F#;

	----------------
	-- OpenGL 4.3 --
	----------------
	GL_NUM_SHADING_LANGUAGE_VERSIONS : constant := 16#82E9#;
	GL_VERTEX_ATTRIB_ARRAY_LONG      : constant := 16#874E#;
	GL_COMPRESSED_RGB8_ETC2           : constant := 16#9274#;
	GL_COMPRESSED_SRGB8_ETC2          : constant := 16#9275#;
	GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 : constant := 16#9276#;
	GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 : constant := 16#9277#;
	GL_COMPRESSED_RGBA8_ETC2_EAC      : constant := 16#9278#;
	GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC : constant := 16#9279#;
	GL_COMPRESSED_R11_EAC             : constant := 16#9270#;
	GL_COMPRESSED_SIGNED_R11_EAC      : constant := 16#9271#;
	GL_COMPRESSED_RG11_EAC            : constant := 16#9272#;
	GL_COMPRESSED_SIGNED_RG11_EAC     : constant := 16#9273#;
	GL_PRIMITIVE_RESTART_FIXED_INDEX  : constant := 16#8D69#;
	GL_ANY_SAMPLES_PASSED_CONSERVATIVE : constant := 16#8D6A#;
	GL_MAX_ELEMENT_INDEX              : constant := 16#8D6B#;
	GL_COMPUTE_SHADER                 : constant := 16#91B9#;
	GL_MAX_COMPUTE_UNIFORM_BLOCKS     : constant := 16#91BB#;
	GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS : constant := 16#91BC#;
	GL_MAX_COMPUTE_IMAGE_UNIFORMS     : constant := 16#91BD#;
	GL_MAX_COMPUTE_SHARED_MEMORY_SIZE : constant := 16#8262#;
	GL_MAX_COMPUTE_UNIFORM_COMPONENTS : constant := 16#8263#;
	GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS : constant := 16#8264#;
	GL_MAX_COMPUTE_ATOMIC_COUNTERS    : constant := 16#8265#;
	GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS : constant := 16#8266#;
	GL_MAX_COMPUTE_LOCAL_INVOCATIONS  : constant := 16#90EB#;
	GL_MAX_COMPUTE_WORK_GROUP_COUNT   : constant := 16#91BE#;
	GL_MAX_COMPUTE_WORK_GROUP_SIZE    : constant := 16#91BF#;
	GL_COMPUTE_LOCAL_WORK_SIZE        : constant := 16#8267#;
	GL_UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER : constant := 16#90EC#;
	GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_COMPUTE_SHADER : constant := 16#90ED#;
	GL_DISPATCH_INDIRECT_BUFFER       : constant := 16#90EE#;
	GL_DISPATCH_INDIRECT_BUFFER_BINDING : constant := 16#90EF#;
	GL_DEBUG_OUTPUT_SYNCHRONOUS       : constant := 16#8242#;
	GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH : constant := 16#8243#;
	GL_DEBUG_CALLBACK_FUNCTION        : constant := 16#8244#;
	GL_DEBUG_CALLBACK_USER_PARAM      : constant := 16#8245#;
	GL_DEBUG_SOURCE_API               : constant := 16#8246#;
	GL_DEBUG_SOURCE_WINDOW_SYSTEM     : constant := 16#8247#;
	GL_DEBUG_SOURCE_SHADER_COMPILER   : constant := 16#8248#;
	GL_DEBUG_SOURCE_THIRD_PARTY       : constant := 16#8249#;
	GL_DEBUG_SOURCE_APPLICATION       : constant := 16#824A#;
	GL_DEBUG_SOURCE_OTHER             : constant := 16#824B#;
	GL_DEBUG_TYPE_ERROR               : constant := 16#824C#;
	GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR : constant := 16#824D#;
	GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR  : constant := 16#824E#;
	GL_DEBUG_TYPE_PORTABILITY         : constant := 16#824F#;
	GL_DEBUG_TYPE_PERFORMANCE         : constant := 16#8250#;
	GL_DEBUG_TYPE_OTHER               : constant := 16#8251#;
	GL_DEBUG_TYPE_MARKER              : constant := 16#8268#;
	GL_DEBUG_TYPE_PUSH_GROUP          : constant := 16#8269#;
	GL_DEBUG_TYPE_POP_GROUP           : constant := 16#826A#;
	GL_DEBUG_SEVERITY_NOTIFICATION    : constant := 16#826B#;
	GL_MAX_DEBUG_GROUP_STACK_DEPTH    : constant := 16#826C#;
	GL_DEBUG_GROUP_STACK_DEPTH        : constant := 16#826D#;
	GL_BUFFER                         : constant := 16#82E0#;
	GL_SHADER                         : constant := 16#82E1#;
	GL_PROGRAM                        : constant := 16#82E2#;
	GL_QUERY                          : constant := 16#82E3#;
	GL_PROGRAM_PIPELINE               : constant := 16#82E4#;
	GL_SAMPLER                        : constant := 16#82E6#;
	GL_DISPLAY_LIST                   : constant := 16#82E7#;
	GL_MAX_LABEL_LENGTH               : constant := 16#82E8#;
	GL_MAX_DEBUG_MESSAGE_LENGTH       : constant := 16#9143#;
	GL_MAX_DEBUG_LOGGED_MESSAGES      : constant := 16#9144#;
	GL_DEBUG_LOGGED_MESSAGES          : constant := 16#9145#;
	GL_DEBUG_SEVERITY_HIGH            : constant := 16#9146#;
	GL_DEBUG_SEVERITY_MEDIUM          : constant := 16#9147#;
	GL_DEBUG_SEVERITY_LOW             : constant := 16#9148#;
	GL_DEBUG_OUTPUT                   : constant := 16#92E0#;
	GL_CONTEXT_FLAG_DEBUG_BIT         : constant := 16#00000002#;
	GL_MAX_UNIFORM_LOCATIONS          : constant := 16#826E#;
	GL_FRAMEBUFFER_DEFAULT_WIDTH      : constant := 16#9310#;
	GL_FRAMEBUFFER_DEFAULT_HEIGHT     : constant := 16#9311#;
	GL_FRAMEBUFFER_DEFAULT_LAYERS     : constant := 16#9312#;
	GL_FRAMEBUFFER_DEFAULT_SAMPLES    : constant := 16#9313#;
	GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS : constant := 16#9314#;
	GL_MAX_FRAMEBUFFER_WIDTH          : constant := 16#9315#;
	GL_MAX_FRAMEBUFFER_HEIGHT         : constant := 16#9316#;
	GL_MAX_FRAMEBUFFER_LAYERS         : constant := 16#9317#;
	GL_MAX_FRAMEBUFFER_SAMPLES        : constant := 16#9318#;
	GL_INTERNALFORMAT_SUPPORTED       : constant := 16#826F#;
	GL_INTERNALFORMAT_PREFERRED       : constant := 16#8270#;
	GL_INTERNALFORMAT_RED_SIZE        : constant := 16#8271#;
	GL_INTERNALFORMAT_GREEN_SIZE      : constant := 16#8272#;
	GL_INTERNALFORMAT_BLUE_SIZE       : constant := 16#8273#;
	GL_INTERNALFORMAT_ALPHA_SIZE      : constant := 16#8274#;
	GL_INTERNALFORMAT_DEPTH_SIZE      : constant := 16#8275#;
	GL_INTERNALFORMAT_STENCIL_SIZE    : constant := 16#8276#;
	GL_INTERNALFORMAT_SHARED_SIZE     : constant := 16#8277#;
	GL_INTERNALFORMAT_RED_TYPE        : constant := 16#8278#;
	GL_INTERNALFORMAT_GREEN_TYPE      : constant := 16#8279#;
	GL_INTERNALFORMAT_BLUE_TYPE       : constant := 16#827A#;
	GL_INTERNALFORMAT_ALPHA_TYPE      : constant := 16#827B#;
	GL_INTERNALFORMAT_DEPTH_TYPE      : constant := 16#827C#;
	GL_INTERNALFORMAT_STENCIL_TYPE    : constant := 16#827D#;
	GL_MAX_WIDTH                      : constant := 16#827E#;
	GL_MAX_HEIGHT                     : constant := 16#827F#;
	GL_MAX_DEPTH                      : constant := 16#8280#;
	GL_MAX_LAYERS                     : constant := 16#8281#;
	GL_MAX_COMBINED_DIMENSIONS        : constant := 16#8282#;
	GL_COLOR_COMPONENTS               : constant := 16#8283#;
	GL_DEPTH_COMPONENTS               : constant := 16#8284#;
	GL_STENCIL_COMPONENTS             : constant := 16#8285#;
	GL_COLOR_RENDERABLE               : constant := 16#8286#;
	GL_DEPTH_RENDERABLE               : constant := 16#8287#;
	GL_STENCIL_RENDERABLE             : constant := 16#8288#;
	GL_FRAMEBUFFER_RENDERABLE         : constant := 16#8289#;
	GL_FRAMEBUFFER_RENDERABLE_LAYERED : constant := 16#828A#;
	GL_FRAMEBUFFER_BLEND              : constant := 16#828B#;
	GL_READ_PIXELS                    : constant := 16#828C#;
	GL_READ_PIXELS_FORMAT             : constant := 16#828D#;
	GL_READ_PIXELS_TYPE               : constant := 16#828E#;
	GL_TEXTURE_IMAGE_FORMAT           : constant := 16#828F#;
	GL_TEXTURE_IMAGE_TYPE             : constant := 16#8290#;
	GL_GET_TEXTURE_IMAGE_FORMAT       : constant := 16#8291#;
	GL_GET_TEXTURE_IMAGE_TYPE         : constant := 16#8292#;
	GL_MIPMAP                         : constant := 16#8293#;
	GL_MANUAL_GENERATE_MIPMAP         : constant := 16#8294#;
	GL_AUTO_GENERATE_MIPMAP           : constant := 16#8295#;
	GL_COLOR_ENCODING                 : constant := 16#8296#;
	GL_SRGB_READ                      : constant := 16#8297#;
	GL_SRGB_WRITE                     : constant := 16#8298#;
	GL_SRGB_DECODE_ARB                : constant := 16#8299#;
	GL_FILTER                         : constant := 16#829A#;
	GL_VERTEX_TEXTURE                 : constant := 16#829B#;
	GL_TESS_CONTROL_TEXTURE           : constant := 16#829C#;
	GL_TESS_EVALUATION_TEXTURE        : constant := 16#829D#;
	GL_GEOMETRY_TEXTURE               : constant := 16#829E#;
	GL_FRAGMENT_TEXTURE               : constant := 16#829F#;
	GL_COMPUTE_TEXTURE                : constant := 16#82A0#;
	GL_TEXTURE_SHADOW                 : constant := 16#82A1#;
	GL_TEXTURE_GATHER                 : constant := 16#82A2#;
	GL_TEXTURE_GATHER_SHADOW          : constant := 16#82A3#;
	GL_SHADER_IMAGE_LOAD              : constant := 16#82A4#;
	GL_SHADER_IMAGE_STORE             : constant := 16#82A5#;
	GL_SHADER_IMAGE_ATOMIC            : constant := 16#82A6#;
	GL_IMAGE_TEXEL_SIZE               : constant := 16#82A7#;
	GL_IMAGE_COMPATIBILITY_CLASS      : constant := 16#82A8#;
	GL_IMAGE_PIXEL_FORMAT             : constant := 16#82A9#;
	GL_IMAGE_PIXEL_TYPE               : constant := 16#82AA#;
	GL_SIMULTANEOUS_TEXTURE_AND_DEPTH_TEST : constant := 16#82AC#;
	GL_SIMULTANEOUS_TEXTURE_AND_STENCIL_TEST : constant := 16#82AD#;
	GL_SIMULTANEOUS_TEXTURE_AND_DEPTH_WRITE : constant := 16#82AE#;
	GL_SIMULTANEOUS_TEXTURE_AND_STENCIL_WRITE : constant := 16#82AF#;
	GL_TEXTURE_COMPRESSED_BLOCK_WIDTH : constant := 16#82B1#;
	GL_TEXTURE_COMPRESSED_BLOCK_HEIGHT : constant := 16#82B2#;
	GL_TEXTURE_COMPRESSED_BLOCK_SIZE  : constant := 16#82B3#;
	GL_CLEAR_BUFFER                   : constant := 16#82B4#;
	GL_TEXTURE_VIEW                   : constant := 16#82B5#;
	GL_VIEW_COMPATIBILITY_CLASS       : constant := 16#82B6#;
	GL_FULL_SUPPORT                   : constant := 16#82B7#;
	GL_CAVEAT_SUPPORT                 : constant := 16#82B8#;
	GL_IMAGE_CLASS_4_X_32             : constant := 16#82B9#;
	GL_IMAGE_CLASS_2_X_32             : constant := 16#82BA#;
	GL_IMAGE_CLASS_1_X_32             : constant := 16#82BB#;
	GL_IMAGE_CLASS_4_X_16             : constant := 16#82BC#;
	GL_IMAGE_CLASS_2_X_16             : constant := 16#82BD#;
	GL_IMAGE_CLASS_1_X_16             : constant := 16#82BE#;
	GL_IMAGE_CLASS_4_X_8              : constant := 16#82BF#;
	GL_IMAGE_CLASS_2_X_8              : constant := 16#82C0#;
	GL_IMAGE_CLASS_1_X_8              : constant := 16#82C1#;
	GL_IMAGE_CLASS_11_11_10           : constant := 16#82C2#;
	GL_IMAGE_CLASS_10_10_10_2         : constant := 16#82C3#;
	GL_VIEW_CLASS_128_BITS            : constant := 16#82C4#;
	GL_VIEW_CLASS_96_BITS             : constant := 16#82C5#;
	GL_VIEW_CLASS_64_BITS             : constant := 16#82C6#;
	GL_VIEW_CLASS_48_BITS             : constant := 16#82C7#;
	GL_VIEW_CLASS_32_BITS             : constant := 16#82C8#;
	GL_VIEW_CLASS_24_BITS             : constant := 16#82C9#;
	GL_VIEW_CLASS_16_BITS             : constant := 16#82CA#;
	GL_VIEW_CLASS_8_BITS              : constant := 16#82CB#;
	GL_VIEW_CLASS_S3TC_DXT1_RGB       : constant := 16#82CC#;
	GL_VIEW_CLASS_S3TC_DXT1_RGBA      : constant := 16#82CD#;
	GL_VIEW_CLASS_S3TC_DXT3_RGBA      : constant := 16#82CE#;
	GL_VIEW_CLASS_S3TC_DXT5_RGBA      : constant := 16#82CF#;
	GL_VIEW_CLASS_RGTC1_RED           : constant := 16#82D0#;
	GL_VIEW_CLASS_RGTC2_RG            : constant := 16#82D1#;
	GL_VIEW_CLASS_BPTC_UNORM          : constant := 16#82D2#;
	GL_VIEW_CLASS_BPTC_FLOAT          : constant := 16#82D3#;
	GL_UNIFORM                        : constant := 16#92E1#;
	GL_UNIFORM_BLOCK                  : constant := 16#92E2#;
	GL_PROGRAM_INPUT                  : constant := 16#92E3#;
	GL_PROGRAM_OUTPUT                 : constant := 16#92E4#;
	GL_BUFFER_VARIABLE                : constant := 16#92E5#;
	GL_SHADER_STORAGE_BLOCK           : constant := 16#92E6#;
	GL_VERTEX_SUBROUTINE              : constant := 16#92E8#;
	GL_TESS_CONTROL_SUBROUTINE        : constant := 16#92E9#;
	GL_TESS_EVALUATION_SUBROUTINE     : constant := 16#92EA#;
	GL_GEOMETRY_SUBROUTINE            : constant := 16#92EB#;
	GL_FRAGMENT_SUBROUTINE            : constant := 16#92EC#;
	GL_COMPUTE_SUBROUTINE             : constant := 16#92ED#;
	GL_VERTEX_SUBROUTINE_UNIFORM      : constant := 16#92EE#;
	GL_TESS_CONTROL_SUBROUTINE_UNIFORM : constant := 16#92EF#;
	GL_TESS_EVALUATION_SUBROUTINE_UNIFORM : constant := 16#92F0#;
	GL_GEOMETRY_SUBROUTINE_UNIFORM    : constant := 16#92F1#;
	GL_FRAGMENT_SUBROUTINE_UNIFORM    : constant := 16#92F2#;
	GL_COMPUTE_SUBROUTINE_UNIFORM     : constant := 16#92F3#;
	GL_TRANSFORM_FEEDBACK_VARYING     : constant := 16#92F4#;
	GL_ACTIVE_RESOURCES               : constant := 16#92F5#;
	GL_MAX_NAME_LENGTH                : constant := 16#92F6#;
	GL_MAX_NUM_ACTIVE_VARIABLES       : constant := 16#92F7#;
	GL_MAX_NUM_COMPATIBLE_SUBROUTINES : constant := 16#92F8#;
	GL_NAME_LENGTH                    : constant := 16#92F9#;
	GL_TYPE                           : constant := 16#92FA#;
	GL_ARRAY_SIZE                     : constant := 16#92FB#;
	GL_OFFSET                         : constant := 16#92FC#;
	GL_BLOCK_INDEX                    : constant := 16#92FD#;
	GL_ARRAY_STRIDE                   : constant := 16#92FE#;
	GL_MATRIX_STRIDE                  : constant := 16#92FF#;
	GL_IS_ROW_MAJOR                   : constant := 16#9300#;
	GL_ATOMIC_COUNTER_BUFFER_INDEX    : constant := 16#9301#;
	GL_BUFFER_BINDING                 : constant := 16#9302#;
	GL_BUFFER_DATA_SIZE               : constant := 16#9303#;
	GL_NUM_ACTIVE_VARIABLES           : constant := 16#9304#;
	GL_ACTIVE_VARIABLES               : constant := 16#9305#;
	GL_REFERENCED_BY_VERTEX_SHADER    : constant := 16#9306#;
	GL_REFERENCED_BY_TESS_CONTROL_SHADER : constant := 16#9307#;
	GL_REFERENCED_BY_TESS_EVALUATION_SHADER : constant := 16#9308#;
	GL_REFERENCED_BY_GEOMETRY_SHADER  : constant := 16#9309#;
	GL_REFERENCED_BY_FRAGMENT_SHADER  : constant := 16#930A#;
	GL_REFERENCED_BY_COMPUTE_SHADER   : constant := 16#930B#;
	GL_TOP_LEVEL_ARRAY_SIZE           : constant := 16#930C#;
	GL_TOP_LEVEL_ARRAY_STRIDE         : constant := 16#930D#;
	GL_LOCATION                       : constant := 16#930E#;
	GL_LOCATION_INDEX                 : constant := 16#930F#;
	GL_IS_PER_PATCH                   : constant := 16#92E7#;
	GL_SHADER_STORAGE_BUFFER          : constant := 16#90D2#;
	GL_SHADER_STORAGE_BUFFER_BINDING  : constant := 16#90D3#;
	GL_SHADER_STORAGE_BUFFER_START    : constant := 16#90D4#;
	GL_SHADER_STORAGE_BUFFER_SIZE     : constant := 16#90D5#;
	GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS : constant := 16#90D6#;
	GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS : constant := 16#90D7#;
	GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS : constant := 16#90D8#;
	GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS : constant := 16#90D9#;
	GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS : constant := 16#90DA#;
	GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS : constant := 16#90DB#;
	GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS : constant := 16#90DC#;
	GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS : constant := 16#90DD#;
	GL_MAX_SHADER_STORAGE_BLOCK_SIZE  : constant := 16#90DE#;
	GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT : constant := 16#90DF#;
	GL_SHADER_STORAGE_BARRIER_BIT     : constant := 16#2000#;
	GL_MAX_COMBINED_SHADER_OUTPUT_RESOURCES : constant := 16#8F39#;
	GL_DEPTH_STENCIL_TEXTURE_MODE     : constant := 16#90EA#;
	GL_TEXTURE_BUFFER_OFFSET          : constant := 16#919D#;
	GL_TEXTURE_BUFFER_SIZE            : constant := 16#919E#;
	GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT : constant := 16#919F#;
	GL_TEXTURE_VIEW_MIN_LEVEL         : constant := 16#82DB#;
	GL_TEXTURE_VIEW_NUM_LEVELS        : constant := 16#82DC#;
	GL_TEXTURE_VIEW_MIN_LAYER         : constant := 16#82DD#;
	GL_TEXTURE_VIEW_NUM_LAYERS        : constant := 16#82DE#;
	GL_TEXTURE_IMMUTABLE_LEVELS       : constant := 16#82DF#;
	GL_VERTEX_ATTRIB_BINDING          : constant := 16#82D4#;
	GL_VERTEX_ATTRIB_RELATIVE_OFFSET  : constant := 16#82D5#;
	GL_VERTEX_BINDING_DIVISOR         : constant := 16#82D6#;
	GL_VERTEX_BINDING_OFFSET          : constant := 16#82D7#;
	GL_VERTEX_BINDING_STRIDE          : constant := 16#82D8#;
	GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET : constant := 16#82D9#;
	GL_MAX_VERTEX_ATTRIB_BINDINGS     : constant := 16#82DA#;

	----------------
	-- OpenGL 4.4 --
	----------------
	GL_MAX_VERTEX_ATTRIB_STRIDE       : constant := 16#82E5#;
	GL_PRIMITIVE_RESTART_FOR_PATCHES_SUPPORTED : constant := 16#8221#;
	GL_TEXTURE_BUFFER_BINDING         : constant := 16#8C2A#;
	GL_MAP_PERSISTENT_BIT             : constant := 16#0040#;
	GL_MAP_COHERENT_BIT               : constant := 16#0080#;
	GL_DYNAMIC_STORAGE_BIT            : constant := 16#0100#;
	GL_CLIENT_STORAGE_BIT             : constant := 16#0200#;
	GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT : constant := 16#00004000#;
	GL_BUFFER_IMMUTABLE_STORAGE       : constant := 16#821F#;
	GL_BUFFER_STORAGE_FLAGS           : constant := 16#8220#;
	GL_CLEAR_TEXTURE                  : constant := 16#9365#;
	GL_LOCATION_COMPONENT             : constant := 16#934A#;
	GL_TRANSFORM_FEEDBACK_BUFFER_INDEX : constant := 16#934B#;
	GL_TRANSFORM_FEEDBACK_BUFFER_STRIDE : constant := 16#934C#;
	GL_QUERY_BUFFER                   : constant := 16#9192#;
	GL_QUERY_BUFFER_BARRIER_BIT       : constant := 16#00008000#;
	GL_QUERY_BUFFER_BINDING           : constant := 16#9193#;
	GL_QUERY_RESULT_NO_WAIT           : constant := 16#9194#;
	GL_MIRROR_CLAMP_TO_EDGE           : constant := 16#8743#;

	------------------------
	-- GL_ARB_copy_buffer --
	------------------------
	GL_COPY_READ_BUFFER_BINDING        : constant := 16#8F36#;
	GL_COPY_WRITE_BUFFER_BINDING       : constant := 16#8F37#;

	----------------------------------------
	-- GL_ARB_compute_variable_group_size --
	----------------------------------------
	GL_MAX_COMPUTE_VARIABLE_GROUP_INVOCATIONS_ARB : constant := 16#9344#;
	GL_MAX_COMPUTE_FIXED_GROUP_INVOCATIONS_ARB : constant := 16#90EB#;
	GL_MAX_COMPUTE_VARIABLE_GROUP_SIZE_ARB : constant := 16#9345#;
	GL_MAX_COMPUTE_FIXED_GROUP_SIZE_ARB : constant := 16#91BF#;

	---------------------------
	-- GL_ARB_sample_shading --
	---------------------------
	GL_SAMPLE_SHADING_ARB              : constant := 16#8C36#;
	GL_MIN_SAMPLE_SHADING_VALUE_ARB    : constant := 16#8C37#;

	-----------------------------------
	-- GL_ARB_texture_cube_map_array --
	-----------------------------------
	GL_TEXTURE_CUBE_MAP_ARRAY_ARB               : constant := 16#9009#;
	GL_TEXTURE_BINDING_CUBE_MAP_ARRAY_ARB       : constant := 16#900A#;
	GL_PROXY_TEXTURE_CUBE_MAP_ARRAY_ARB         : constant := 16#900B#;
	GL_SAMPLER_CUBE_MAP_ARRAY_ARB               : constant := 16#900C#;
	GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW_ARB        : constant := 16#900D#;
	GL_INT_SAMPLER_CUBE_MAP_ARRAY_ARB           : constant := 16#900E#;
	GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY_ARB  : constant := 16#900F#;

	---------------------------
	-- GL_ARB_sparse_texture --
	---------------------------
	GL_TEXTURE_SPARSE_ARB             : constant := 16#91A6#;
	GL_VIRTUAL_PAGE_SIZE_INDEX_ARB    : constant := 16#91A7#;
	GL_MIN_SPARSE_LEVEL_ARB           : constant := 16#919B#;
	GL_NUM_VIRTUAL_PAGE_SIZES_ARB     : constant := 16#91A8#;
	GL_VIRTUAL_PAGE_SIZE_X_ARB        : constant := 16#9195#;
	GL_VIRTUAL_PAGE_SIZE_Y_ARB        : constant := 16#9196#;
	GL_VIRTUAL_PAGE_SIZE_Z_ARB        : constant := 16#9197#;
	GL_MAX_SPARSE_TEXTURE_SIZE_ARB    : constant := 16#9198#;
	GL_MAX_SPARSE_3D_TEXTURE_SIZE_ARB : constant := 16#9199#;
	GL_MAX_SPARSE_ARRAY_TEXTURE_LAYERS_ARB : constant := 16#919A#;
	GL_SPARSE_TEXTURE_FULL_ARRAY_CUBE_MIPMAPS_ARB : constant := 16#91A9#;

	---------------------------
	-- GL_ARB_texture_gather --
	---------------------------
	GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET_ARB  : constant := 16#8E5E#;
	GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET_ARB  : constant := 16#8E5F#;
	GL_MAX_PROGRAM_TEXTURE_GATHER_COMPONENTS_ARB : constant := 16#8F9F#;

	-------------------------------------
	-- GL_ARB_shading_language_include --
	-------------------------------------
	GL_SHADER_INCLUDE_ARB              : constant := 16#8DAE#;
	GL_NAMED_STRING_LENGTH_ARB         : constant := 16#8DE9#;
	GL_NAMED_STRING_TYPE_ARB           : constant := 16#8DEA#;

	-------------------------------------
	-- GL_ARB_texture_compression_bptc --
	-------------------------------------
	GL_COMPRESSED_RGBA_BPTC_UNORM_ARB           : constant := 16#8E8C#;
	GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM_ARB     : constant := 16#8E8D#;
	GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB     : constant := 16#8E8E#;
	GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB   : constant := 16#8E8F#;

	--------------------------------
	-- GL_ARB_transform_feedback2 --
	--------------------------------
	GL_TRANSFORM_FEEDBACK_PAUSED        : constant := 16#8E23#;
	GL_TRANSFORM_FEEDBACK_ACTIVE        : constant := 16#8E24#;
	
	---------------------
	-- GL_ARB_cl_event --
	---------------------
	GL_SYNC_CL_EVENT_ARB               : constant := 16#8240#;
	GL_SYNC_CL_EVENT_COMPLETE_ARB      : constant := 16#8241#;

	--------------------
	-- GL_ARB_imaging --
	--------------------
	GL_BLEND_COLOR              : constant := 16#8005#;
	GL_BLEND_EQUATION           : constant := 16#8009#;

	-------------------------
	-- GL_ARB_debug_output --
	-------------------------
	GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB   : constant := 16#8242#;
	GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH_ARB : constant := 16#8243#;
	GL_DEBUG_CALLBACK_FUNCTION_ARB    : constant := 16#8244#;
	GL_DEBUG_CALLBACK_USER_PARAM_ARB  : constant := 16#8245#;
	GL_DEBUG_SOURCE_API_ARB           : constant := 16#8246#;
	GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB : constant := 16#8247#;
	GL_DEBUG_SOURCE_SHADER_COMPILER_ARB : constant := 16#8248#;
	GL_DEBUG_SOURCE_THIRD_PARTY_ARB   : constant := 16#8249#;
	GL_DEBUG_SOURCE_APPLICATION_ARB   : constant := 16#824A#;
	GL_DEBUG_SOURCE_OTHER_ARB         : constant := 16#824B#;
	GL_DEBUG_TYPE_ERROR_ARB           : constant := 16#824C#;
	GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB : constant := 16#824D#;
	GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB : constant := 16#824E#;
	GL_DEBUG_TYPE_PORTABILITY_ARB     : constant := 16#824F#;
	GL_DEBUG_TYPE_PERFORMANCE_ARB     : constant := 16#8250#;
	GL_DEBUG_TYPE_OTHER_ARB           : constant := 16#8251#;
	GL_MAX_DEBUG_MESSAGE_LENGTH_ARB   : constant := 16#9143#;
	GL_MAX_DEBUG_LOGGED_MESSAGES_ARB  : constant := 16#9144#;
	GL_DEBUG_LOGGED_MESSAGES_ARB      : constant := 16#9145#;
	GL_DEBUG_SEVERITY_HIGH_ARB        : constant := 16#9146#;
	GL_DEBUG_SEVERITY_MEDIUM_ARB      : constant := 16#9147#;
	GL_DEBUG_SEVERITY_LOW_ARB         : constant := 16#9148#;

	-----------------------------
	-- GL_ARB_bindless_texture --
	-----------------------------
	GL_UNSIGNED_INT64_ARB             : constant := 16#140F#;

	-----------------------
	-- GL_ARB_robustness --
	-----------------------
	GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT_ARB  : constant := 16#00000004#;
	GL_LOSE_CONTEXT_ON_RESET_ARB       : constant := 16#8252#;
	GL_GUILTY_CONTEXT_RESET_ARB        : constant := 16#8253#;
	GL_INNOCENT_CONTEXT_RESET_ARB      : constant := 16#8254#;
	GL_UNKNOWN_CONTEXT_RESET_ARB       : constant := 16#8255#;
	GL_RESET_NOTIFICATION_STRATEGY_ARB  : constant := 16#8256#;
	GL_NO_RESET_NOTIFICATION_ARB       : constant := 16#8261#;

	--------------------------------
	-- GL_ARB_indirect_parameters --
	--------------------------------
	GL_PARAMETER_BUFFER_ARB : constant := 16#80EE#;
	GL_PARAMETER_BUFFER_BINDING_ARB : constant := 16#80EF#;

	-----------------------------------------
	-- GL_KHR_texture_compression_astc_ldr --
	-----------------------------------------
	GL_COMPRESSED_RGBA_ASTC_4x4_KHR   : constant := 16#93B0#;
	GL_COMPRESSED_RGBA_ASTC_5x4_KHR   : constant := 16#93B1#;
	GL_COMPRESSED_RGBA_ASTC_5x5_KHR   : constant := 16#93B2#;
	GL_COMPRESSED_RGBA_ASTC_6x5_KHR   : constant := 16#93B3#;
	GL_COMPRESSED_RGBA_ASTC_6x6_KHR   : constant := 16#93B4#;
	GL_COMPRESSED_RGBA_ASTC_8x5_KHR   : constant := 16#93B5#;
	GL_COMPRESSED_RGBA_ASTC_8x6_KHR   : constant := 16#93B6#;
	GL_COMPRESSED_RGBA_ASTC_8x8_KHR   : constant := 16#93B7#;
	GL_COMPRESSED_RGBA_ASTC_10x5_KHR  : constant := 16#93B8#;
	GL_COMPRESSED_RGBA_ASTC_10x6_KHR  : constant := 16#93B9#;
	GL_COMPRESSED_RGBA_ASTC_10x8_KHR  : constant := 16#93BA#;
	GL_COMPRESSED_RGBA_ASTC_10x10_KHR : constant := 16#93BB#;
	GL_COMPRESSED_RGBA_ASTC_12x10_KHR : constant := 16#93BC#;
	GL_COMPRESSED_RGBA_ASTC_12x12_KHR : constant := 16#93BD#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR : constant := 16#93D0#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR : constant := 16#93D1#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR : constant := 16#93D2#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR : constant := 16#93D3#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR : constant := 16#93D4#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR : constant := 16#93D5#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR : constant := 16#93D6#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR : constant := 16#93D7#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR : constant := 16#93D8#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR : constant := 16#93D9#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR : constant := 16#93DA#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR : constant := 16#93DB#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR : constant := 16#93DC#;
	GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR : constant := 16#93DD#;

	---------------------------
	-- GL_ARB_compute_shader --
	---------------------------
	GL_COMPUTE_SHADER_BIT             : constant := 16#00000020#;

	----------------------------------
	-- GL_ARB_uniform_buffer_object --
	----------------------------------
	GL_MAX_GEOMETRY_UNIFORM_BLOCKS     : constant := 16#8A2C#;
	GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS  : constant := 16#8A32#;
	GL_UNIFORM_BLOCK_REFERENCED_BY_GEOMETRY_SHADER  : constant := 16#8A45#;

	----------------
	-- OpenGL 1.0 --
	----------------
	procedure glCullFace (mode : GLenum);
	procedure glFrontFace (mode : GLenum);
	procedure glHint (target : GLenum;
					  mode : GLenum);

	procedure glLineWidth (width : GLfloat);
	procedure glPointSize (size : GLfloat);

	procedure glPolygonMode (face : GLenum;
							 mode : GLenum);

	procedure glScissor (x     : GLint;
						 y      : GLint;
						 width  : GLsizei;
						 height : GLsizei);

	procedure glTexParameterf (target : GLenum;
							   pname  : GLenum;
							   param  : GLfloat);

	procedure glTexParameterfv (target : GLenum;
								pname  : GLenum;
								params : access GLfloat);

	procedure glTexParameteri (target : GLenum;
							   pname : GLenum;
							   param : GLenum);

	procedure glTexParameteriv (target : GLenum;
								pname  : GLenum;
								params : access GLint);

	procedure glTexImage1D (target         : GLenum;
							level          : GLint;
							internalFormat : GLint;
							width          : GLsizei;
							border         : GLint;
							format         : GLenum;
							type_Id        : GLenum;
							pixels         : System.Address);

	procedure glTexImage2D (target         : GLenum;
							level          : GLint;
							internalFormat : GLint;
							width          : GLsizei;
							height         : GLsizei;
							border         : GLint;
							format         : GLenum;
							type_Id        : GLenum;
							pixels         : System.Address);

	procedure glDrawBuffer (mode : GLenum);
	procedure glClear (mask : GLbitfield);

	procedure glClearColor (red   : GLclampf;
							green : GLclampf;
							blue  : GLclampf;
							alpha : GLclampf);

	procedure glClearStencil (s : GLint);
	procedure glClearDepth (depth : GLclampd);
	procedure glStencilMask (mask : GLuint);

	procedure glColorMask (red   : GLboolean;
						   green : GLboolean;
						   blue  : GLboolean;
						   alpha : GLboolean);

	procedure glDepthMask (flag : GLboolean);
	procedure glDisable (cap : GLenum);
	procedure glEnable (cap : GLenum);
	procedure glFinish;
	procedure glFlush;
	procedure glBlendFunc (sfactor : GLenum; dfactor : GLenum);
	procedure glLogicOp (opcode : GLenum);
	procedure glStencilFunc (func : GLenum; ref : GLint; mask : GLuint);
	procedure glStencilOp (fail : GLenum; zfail : GLenum; zpass : GLenum);
	procedure glDepthFunc (func : GLenum);
	procedure glPixelStoref (pname : GLenum; param : GLfloat);
	procedure glPixelStorei (pname : GLenum; param : GLint);
	procedure glReadBuffer (mode : GLenum);

	procedure glReadPixels (x       : GLint;
							y       : GLint;
							width   : GLsizei;
							height  : GLsizei;
							format  : GLenum;
							type_Id : GLenum;
							pixels  : GLvoid);

	procedure glGetBooleanv (pname : GLenum; params : access GLboolean);
	procedure glGetDoublev (pname : GLenum; params : access GLdouble);
	function glGetError return GLenum;
	procedure glGetFloatv (pname : GLenum; params : access GLfloat);
	procedure glGetIntegerv (pname : GLenum; params : access GLint);
	function glGetString (name : GLenum) return a_GLubyte;

	procedure glGetTexImage (target  : GLenum;
							 level   : GLint;
							 format  : GLenum;
							 type_Id : GLenum;
							 pixels  : GLvoid);

	procedure glGetTexParameterfv (target : GLenum;
								   pname  : GLenum;
								   params : access constant GLfloat);

	procedure glGetTexParameteriv (target : GLenum;
								   pname  : GLenum;
								   params : access constant GLint);

	procedure glGetTexLevelParameterfv (target : GLenum;
										level  : GLint;
										pname  : GLenum;
										params : access GLfloat);

	procedure glGetTexLevelParameteriv (target : GLenum;
										level  : GLint;
										pname  : GLenum;
										params : access GLint);

	function glIsEnabled (cap : GLenum) return GLboolean;
	procedure glDepthRange (near_val : GLclampd; far_val : GLclampd);

	procedure glViewport (x      : GLint;
						  y      : GLint;
						  width  : GLsizei;
						  height : GLsizei);

	----------------
	-- OpenGL 1.1 --
	----------------
	procedure glDrawArrays (mode : GLenum; first : GLint; count : GLsizei);

	procedure glDrawElements (mode    : GLenum;
							  count   : GLsizei;
							  type_Id : GLenum;
							  indices : GLvoid);

	procedure glGetPointerv (pname  : GLenum;
							 params : access Interfaces.C.Extensions.void_ptr);

	procedure glPolygonOffset (factor : GLfloat; units : GLfloat);

	procedure glCopyTexImage1D (target         : GLenum;
								level          : GLint;
								internalformat : GLenum;
								x              : GLint;
								y              : GLint;
								width          : GLsizei;
								border         : GLint);
	-- Copy pixels into a 1D texture image.

	procedure glCopyTexImage2D (target         : GLenum;
								level          : GLint;
								internalformat : GLenum;
								x              : GLint;
								y              : GLint;
								width          : GLsizei;
								height         : GLsizei;
								border         : GLint);
	-- Copy pixels into a 2D texture image.

	procedure glCopyTexSubImage1D (target  : GLenum;
								   level   : GLint;
								   xoffset : GLint;
								   x       : GLint;
								   y       : GLint;
								   width   : GLsizei);
	-- Copy a one-dimensional texture subimage.

	procedure glCopyTexSubImage2D (target  : GLenum;
								   level   : GLint;
								   xoffset : GLint;
								   yoffset : GLint;
								   x       : GLint;
								   y       : GLint;
								   width   : GLsizei;
								   height  : GLsizei);
	-- Copy a two-dimensional texture subimage.

	procedure glTexSubImage1D (target  : GLenum;
							   level   : GLint;
							   xoffset : GLint;
							   width   : GLsizei;
							   format  : GLenum;
							   type_Id : GLenum;
							   pixels  : GLvoid);

	procedure glTexSubImage2D (target  : GLenum;
							   level   : GLint;
							   xoffset : GLint;
							   yoffset : GLint;
							   width   : GLsizei;
							   height  : GLsizei;
							   format  : GLenum;
							   type_Id : GLenum;
							   pixels  : GLvoid);

	procedure glBindTexture (target : GLenum; texture : GLuint);
	procedure glDeleteTextures (n : GLsizei; textures : access constant GLuint);
	procedure glGenTextures (n : GLsizei; textures : access GLUint);
	function glIsTexture (texture : GLuint) return GLboolean;

	----------------
	-- OpenGL 1.2 --
	----------------
	type PFNGLDRAWRANGEELEMENTSPROC is access procedure (mode       : GLenum;
														 start      : GLuint;
														 endd       : GLuint;
														 count      : GLsizei;
														 typee      : GLenum;
														 indices    : GLvoid);

	type PFNGLTEXIMAGE3DPROC is access procedure (target    : GLenum;
												  level     : GLint;
												  internalformat : GLint;
												  width     : GLsizei;
												  height    : GLsizei;
												  depth     : GLsizei;
												  border    : GLint;
												  format    : GLenum;
												  typee     : GLenum;
												  pixels    : GLvoid);

	type PFNGLTEXSUBIMAGE3DPROC is access procedure (target     : GLenum;
													 level      : GLint;
													 xoffset    : GLint;
													 yoffset    : GLint;
													 zoffset    : GLint;
													 width      : GLsizei;
													 height     : GLsizei;
													 depth      : GLsizei;
													 format     : GLenum;
													 typee      : GLenum;
													 pixels     : GLvoid);

	type PFNGLCOPYTEXSUBIMAGE3DPROC is access procedure (target     : GLenum;
														 level      : GLint;
														 xoffset    : GLint;
														 yoffset    : GLint;
														 zoffset    : GLint;
														 x          : GLint;
														 y          : GLint;
														 width      : GLsizei;
														 height     : GLsizei);

	pragma Convention (Stdcall, PFNGLDRAWRANGEELEMENTSPROC);
	pragma Convention (Stdcall, PFNGLTEXIMAGE3DPROC);
	pragma Convention (Stdcall, PFNGLTEXSUBIMAGE3DPROC);
	pragma Convention (Stdcall, PFNGLCOPYTEXSUBIMAGE3DPROC);

	glDrawRangeElements : PFNGLDRAWRANGEELEMENTSPROC;
	glTexImage3D        : PFNGLTEXIMAGE3DPROC;
	glTexSubImage3D     : PFNGLTEXSUBIMAGE3DPROC;
	glCopyTexSubImage3D : PFNGLCOPYTEXSUBIMAGE3DPROC;

	----------------
	-- OpenGL 1.3 --
	----------------
	type PFNGLACTIVETEXTUREPROC is access procedure (texture: GLenum);

	type PFNGLSAMPLECOVERAGEPROC is access procedure (value: GLclampf; invert: GLboolean);


	type PFNGLCOMPRESSEDTEXIMAGE3DPROC is access procedure (target         :        GLenum;
															level          :        GLint;
															internalformat :        GLenum;
															width          :        GLsizei;
															height         :        GLsizei;
															depth          :        GLsizei;
															border         :        GLint;
															imageSize      :        GLsizei;
															data           : GLvoid);

	type PFNGLCOMPRESSEDTEXIMAGE2DPROC is access procedure (target         :        GLenum;
															level          :        GLint;
															internalformat :        GLenum;
															width          :        GLsizei;
															height         :        GLsizei;
															border         :        GLint;
															imageSize      :        GLsizei;
															data           : GLvoid);

	type PFNGLCOMPRESSEDTEXIMAGE1DPROC is access procedure (target         :        GLenum;
															level          :        GLint;
															internalformat :        GLenum;
															width          :        GLsizei;
															border         :        GLint;
															imageSize      :        GLsizei;
															data           : GLvoid);

	type PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC is access procedure (target      :        GLenum;
															   level       :        GLint;
															   xoffset     :        GLint;
															   yoffset     :        GLint;
															   zoffset     :        GLint;
															   width       :        GLsizei;
															   height      :        GLsizei;
															   depth       :        GLsizei;
															   format      :        GLenum;
															   imageSize   :        GLsizei;
															   data        : GLvoid);

	type PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC is access procedure (target    :        GLenum;
															   level     :        GLint;
															   xoffset   :        GLint;
															   yoffset   :        GLint;
															   width     :        GLsizei;
															   height    :        GLsizei;
															   format    :        GLenum;
															   imageSize :        GLsizei;
															   data      : GLvoid);

	type PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC is access procedure (target    :        GLenum;
															   level     :        GLint;
															   xoffset   :        GLint;
															   width     :        GLsizei;
															   format    :        GLenum;
															   imageSize :        GLsizei;
															   data      : GLvoid);

	type PFNGLGETCOMPRESSEDTEXIMAGEPROC is access procedure (target      :        GLenum;
															 lod         :        GLint;
															 img         : GLvoid);

	pragma Convention (Stdcall, PFNGLACTIVETEXTUREPROC);
	pragma Convention (Stdcall, PFNGLSAMPLECOVERAGEPROC);
	pragma Convention (Stdcall, PFNGLCOMPRESSEDTEXIMAGE3DPROC);
	pragma Convention (Stdcall, PFNGLCOMPRESSEDTEXIMAGE2DPROC);
	pragma Convention (Stdcall, PFNGLCOMPRESSEDTEXIMAGE1DPROC);
	pragma Convention (Stdcall, PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC);
	pragma Convention (Stdcall, PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC);
	pragma Convention (Stdcall, PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC);
	pragma Convention (Stdcall, PFNGLGETCOMPRESSEDTEXIMAGEPROC);

	glActiveTexture             : PFNGLACTIVETEXTUREPROC;
	glSampleCoverage            : PFNGLSAMPLECOVERAGEPROC;
	glCompressedTexImage3D      : PFNGLCOMPRESSEDTEXIMAGE3DPROC;
	glCompressedTexImage2D      : PFNGLCOMPRESSEDTEXIMAGE2DPROC;
	glCompressedTexImage1D      : PFNGLCOMPRESSEDTEXIMAGE1DPROC;
	glCompressedTexSubImage3D   : PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC;
	glCompressedTexSubImage2D   : PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC;
	glCompressedTexSubImage1D   : PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC;
	glGetCompressedTexImage     : PFNGLGETCOMPRESSEDTEXIMAGEPROC;

	----------------
	-- OpenGL 1.4 --
	----------------
	type PFNGLBLENDFUNCSEPARATEPROC is access procedure (sfactorRGB   : GLenum;
														 dfactorRGB   : GLenum;
														 sfactorAlpha : GLenum;
														 dfactorAlpha : GLenum);

	type PFNGLMULTIDRAWARRAYSPROC is access procedure (mode      :        GLenum;
													   first     : access constant GLint;
													   count     : access constant GLsizei;
													   primcount :        GLsizei);

	type PFNGLMULTIDRAWELEMENTSPROC is access procedure (mode      :        GLenum;
														 count     : access constant GLsizei;
														 typ       :        GLenum;
														 indices   :        System.Address;
														 primcount :        GLsizei);

	type PFNGLPOINTPARAMETERFPROC is access procedure (pname : GLenum;
													   param : GLfloat);

	type PFNGLPOINTPARAMETERFVPROC is access procedure (pname  :        GLenum;
														params : access constant GLfloat);

	type PFNGLPOINTPARAMETERIPROC is access procedure (pname : GLenum;
													   param : GLint);

	type PFNGLPOINTPARAMETERIVPROC is access procedure (pname  :        GLenum;
														params : access constant GLint);

	type PFNGLBLENDCOLORPROC is access procedure (red   : GLfloat;
												  green : GLfloat;
												  blue  : GLfloat;
												  alpha : GLfloat);

	type PFNGLBLENDEQUATIONPROC is access procedure (mode : GLenum);

	pragma Convention (Stdcall, PFNGLBLENDFUNCSEPARATEPROC);
	pragma Convention (Stdcall, PFNGLMULTIDRAWARRAYSPROC);
	pragma Convention (Stdcall, PFNGLMULTIDRAWELEMENTSPROC);
	pragma Convention (Stdcall, PFNGLPOINTPARAMETERFPROC);
	pragma Convention (Stdcall, PFNGLPOINTPARAMETERFVPROC);
	pragma Convention (Stdcall, PFNGLPOINTPARAMETERIPROC);
	pragma Convention (Stdcall, PFNGLPOINTPARAMETERIVPROC);
	pragma Convention (Stdcall, PFNGLBLENDCOLORPROC);
	pragma Convention (Stdcall, PFNGLBLENDEQUATIONPROC);

	glBlendFuncSeparate : PFNGLBLENDFUNCSEPARATEPROC;
	glMultiDrawArrays   : PFNGLMULTIDRAWARRAYSPROC;
	glMultiDrawElements : PFNGLMULTIDRAWELEMENTSPROC;
	glPointParameterf   : PFNGLPOINTPARAMETERFPROC;
	glPointParameterfv  : PFNGLPOINTPARAMETERFVPROC;
	glPointParameteri   : PFNGLPOINTPARAMETERIPROC;
	glPointParameteriv  : PFNGLPOINTPARAMETERIVPROC;
	glBlendColor        : PFNGLBLENDCOLORPROC;
	glBlendEquation     : PFNGLBLENDEQUATIONPROC;

	----------------
	-- OpenGL 1.5 --
	----------------
	type PFNGLGENQUERIESPROC is access procedure (n   :        GLsizei;
												  ids : access GLuint);

	type PFNGLDELETEQUERIESPROC is access procedure (n   :        GLsizei;
													 ids : access constant GLuint);

	type PFNGLISQUERYPROC is access function (id: GLuint) return GLboolean;

	type PFNGLBEGINQUERYPROC is access procedure (target : GLenum;
												  id     : GLuint);

	type PFNGLENDQUERYPROC is access procedure (target: GLenum);

	type PFNGLGETQUERYIVPROC is access procedure (target : GLenum;
												  pname  : GLenum;
												  params : access GLint);

	type PFNGLGETQUERYOBJECTIVPROC is access procedure (id     :        GLuint;
														pname  :        GLenum;
														params : access GLint);

	type PFNGLGETQUERYOBJECTUIVPROC is access procedure (id     :        GLuint;
														 pname  :        GLenum;
														 params : access GLuint);

	type PFNGLBINDBUFFERPROC is access procedure (target: GLenum;
												  buffer: GLuint);

	type PFNGLDELETEBUFFERSPROC is access procedure (n: GLsizei;
													 buffers: access constant GLuint);

	type PFNGLGENBUFFERSPROC is access procedure (n: GLsizei;
												  buffers: access GLuint);

	type PFNGLISBUFFERPROC is access function (buffer: GLuint) return GLboolean;


	type PFNGLBUFFERDATAPROC is access procedure (target :        GLenum;
												  size   : GLsizeiptr;
												  data   : System.Address;
												  usage  :        GLenum);

	type PFNGLBUFFERSUBDATAPROC is access procedure (target :        GLenum;
													 offset : access GLint;
													 size   : access GLsizei;
													 data   : GLvoid);

	type PFNGLGETBUFFERSUBDATAPROC is access procedure (target :        GLenum;
														offset : access GLint;
														size   : access GLsizei;
														data   : GLvoid);

	type PFNGLMAPBUFFERPROC is access function (target: GLenum;
												accesss: GLenum) return GLvoid;

	type PFNGLUNMAPBUFFERPROC is access function (target: GLenum) return GLboolean;

	type PFNGLGETBUFFERPARAMETERIVPROC is access procedure (target :        GLenum;
															pname  :        GLenum;
															params : access GLint);

	type PFNGLGETBUFFERPOINTERVPROC is access procedure (target : GLenum;
														 pname  : GLenum;
														 params : System.Address);


	pragma Convention (Stdcall, PFNGLGENQUERIESPROC);
	pragma Convention (Stdcall, PFNGLDELETEQUERIESPROC);
	pragma Convention (Stdcall, PFNGLISQUERYPROC);
	pragma Convention (Stdcall, PFNGLBEGINQUERYPROC);
	pragma Convention (Stdcall, PFNGLENDQUERYPROC);
	pragma Convention (Stdcall, PFNGLGETQUERYIVPROC);
	pragma Convention (Stdcall, PFNGLGETQUERYOBJECTIVPROC);
	pragma Convention (Stdcall, PFNGLGETQUERYOBJECTUIVPROC);
	pragma Convention (Stdcall, PFNGLBINDBUFFERPROC);
	pragma Convention (Stdcall, PFNGLDELETEBUFFERSPROC);
	pragma Convention (Stdcall, PFNGLGENBUFFERSPROC);
	pragma Convention (Stdcall, PFNGLISBUFFERPROC);
	pragma Convention (Stdcall, PFNGLBUFFERDATAPROC);
	pragma Convention (Stdcall, PFNGLBUFFERSUBDATAPROC);
	pragma Convention (Stdcall, PFNGLGETBUFFERSUBDATAPROC);
	pragma Convention (Stdcall, PFNGLMAPBUFFERPROC);
	pragma Convention (Stdcall, PFNGLUNMAPBUFFERPROC);
	pragma Convention (Stdcall, PFNGLGETBUFFERPARAMETERIVPROC);
	pragma Convention (Stdcall, PFNGLGETBUFFERPOINTERVPROC);

	glGenQueries           : PFNGLGENQUERIESPROC;
	glDeleteQueries        : PFNGLDELETEQUERIESPROC;
	glIsQuery              : PFNGLISQUERYPROC;
	glBeginQuery           : PFNGLBEGINQUERYPROC;
	glEndQuery             : PFNGLENDQUERYPROC;
	glGetQueryiv           : PFNGLGETQUERYIVPROC;
	glGetQueryObjectiv     : PFNGLGETQUERYOBJECTIVPROC;
	glGetQueryObjectuiv    : PFNGLGETQUERYOBJECTUIVPROC;
	glBindBuffer           : PFNGLBINDBUFFERPROC;
	glDeleteBuffers        : PFNGLDELETEBUFFERSPROC;
	glGenBuffers           : PFNGLGENBUFFERSPROC;
	glIsBuffer             : PFNGLISBUFFERPROC;
	glBufferData           : PFNGLBUFFERDATAPROC;
	glBufferSubData        : PFNGLBUFFERSUBDATAPROC;
	glGetBufferSubData     : PFNGLGETBUFFERSUBDATAPROC;
	glMapBuffer            : PFNGLMAPBUFFERPROC;
	glUnmapBuffer          : PFNGLUNMAPBUFFERPROC;
	glGetBufferParameteriv : PFNGLGETBUFFERPARAMETERIVPROC;
	glGetBufferPointerv    : PFNGLGETBUFFERPOINTERVPROC;

	----------------
	-- OpenGL 2.0 --
	----------------
	type PFNGLBLENDEQUATIONSEPARATEPROC is access procedure (modeRGB   : GLenum;
															 modeAlpha : GLenum);

	type PFNGLDRAWBUFFERSPROC is access procedure (n    :        GLsizei;
												   bufs : access constant GLenum);

	type PFNGLSTENCILOPSEPARATEPROC is access procedure (face   : GLenum;
														 sfail  : GLenum;
														 dpfail : GLenum;
														 dppass : GLenum);

	type PFNGLSTENCILFUNCSEPARATEPROC is access procedure (frontfunc : GLenum;
														   backfunc  : GLenum;
														   ref       : GLint;
														   mask      : GLuint);

	type PFNGLSTENCILMASKSEPARATEPROC is access procedure (face : GLenum;
														   mask : GLuint);


	type PFNGLATTACHSHADERPROC is access procedure (program : GLuint; shader: GLuint);

	type PFNGLBINDATTRIBLOCATIONPROC is access procedure (program :        GLuint;
														  index   :        GLuint;
														  name    : access constant GLchar);

	type PFNGLCOMPILESHADERPROC is access procedure (shader : GLuint);

	type PFNGLCREATEPROGRAMPROC is access function return GLuint;

	type PFNGLCREATESHADERPROC is access function (typ : GLenum) return GLuint;

	type PFNGLDELETEPROGRAMPROC is access procedure (program : GLuint);

	type PFNGLDELETESHADERPROC is access procedure (shader : GLuint);

	type PFNGLDETACHSHADERPROC is access procedure (program : GLuint; shader: GLuint);

	type PFNGLDISABLEVERTEXATTRIBARRAYPROC is access procedure (index : GLuint);

	type PFNGLENABLEVERTEXATTRIBARRAYPROC is access procedure (index : GLuint);

	type PFNGLGETACTIVEATTRIBPROC is access procedure (program   :        GLuint;
													   index     :        GLuint;
													   maxLength :        GLsizei;
													   length    : access GLsizei;
													   size      : access GLint;
													   typ       : access GLenum;
													   name      : access GLchar);

	type PFNGLGETACTIVEUNIFORMPROC is access procedure (program   :        GLuint;
														index     :        GLuint;
														maxLength :        GLsizei;
														length    : access GLsizei;
														size      : access GLint;
														typ       : access GLenum;
														name      : access GLchar);

	type PFNGLGETATTACHEDSHADERSPROC is access procedure (program  :        GLuint;
														  maxCount :        GLsizei;
														  count    : access GLsizei;
														  shaders  : access GLuint);

	type PFNGLGETATTRIBLOCATIONPROC is access function (program :        GLuint;
														name    : access constant GLchar)
														return GLint;

	type PFNGLGETPROGRAMIVPROC is access procedure (program :        GLuint;
													pname   :        GLenum;
													param   : access GLint);

	type PFNGLGETPROGRAMINFOLOGPROC is access procedure (program :        GLuint;
														 bufSize :        GLsizei;
														 length  : access GLsizei;
														 infoLog : access GLchar);

	type PFNGLGETSHADERIVPROC is access procedure (shader :        GLuint;
												   pname  :        GLenum;
												   param  : access GLint);

	type PFNGLGETSHADERINFOLOGPROC is access procedure (shader  :        GLuint;
														bufSize :        GLsizei;
														length  : access GLsizei;
														infoLog : access GLchar);

	type PFNGLGETSHADERSOURCEPROC is access procedure (shader    :        GLuint;
													   bufSize   :        GLsizei;
													   length    : access GLsizei;
													   source    : access GLchar);

	type PFNGLGETUNIFORMLOCATIONPROC is access function (program :        GLuint;
														 name    : access constant GLchar)
														 return GLint;

	type PFNGLGETUNIFORMFVPROC is access procedure (program  :        GLuint;
													location :        GLint;
													params   : access GLfloat);

	type PFNGLGETUNIFORMIVPROC is access procedure (program  :        GLuint;
													location :        GLint;
													params   : access GLint);

	type PFNGLGETVERTEXATTRIBDVPROC is access procedure (index  :        GLuint;
														 pname  :        GLenum;
														 params : access GLdouble);

	type PFNGLGETVERTEXATTRIBFVPROC is access procedure (index  :        GLuint;
														 pname  :        GLenum;
														 params : access GLfloat);

	type PFNGLGETVERTEXATTRIBIVPROC is access procedure (index  :        GLuint;
														 pname  :        GLenum;
														 params : access GLint);

	type PFNGLGETVERTEXATTRIBPOINTERVPROC is access procedure (index   : GLuint;
															   pname   : GLenum;
															   pointer : System.Address);

	type PFNGLISPROGRAMPROC is access function (program: GLuint) return GLboolean;

	type PFNGLISSHADERPROC is access function (shader: GLuint) return GLboolean;

	type PFNGLLINKPROGRAMPROC is access procedure (program: GLuint);

	type PFNGLSHADERSOURCEPROC is access procedure (shader  :        GLuint;
													count   :        GLsizei;
													strings :        System.Address;
													lengths : access constant GLint);

	type PFNGLUSEPROGRAMPROC is access procedure (program : GLuint);

	type PFNGLUNIFORM1FPROC is access procedure (location : GLint;
												 v0       : GLfloat);

	type PFNGLUNIFORM2FPROC is access procedure (location : GLint;
												 v0       : GLfloat;
												 v1       : GLfloat);

	type PFNGLUNIFORM3FPROC is access procedure (location : GLint;
												 v0       : GLfloat;
												 v1       : GLfloat;
												 v2       : GLfloat);

	type PFNGLUNIFORM4FPROC is access procedure (location : GLint;
												 v0       : GLfloat;
												 v1       : GLfloat;
												 v2       : GLfloat;
												 v3       : GLfloat);


	type PFNGLUNIFORM1IPROC is access procedure (location : GLint;
												 v0       : GLint);

	type PFNGLUNIFORM2IPROC is access procedure (location : GLint;
												 v0       : GLint;
												 v1       : GLint);

	type PFNGLUNIFORM3IPROC is access procedure (location : GLint;
												 v0       : GLint;
												 v1       : GLint;
												 v2       : GLint);

	type PFNGLUNIFORM4IPROC is access procedure (location : GLint;
												 v0       : GLint;
												 v1       : GLint;
												 v2       : GLint;
												 v3       : GLint);

	type PFNGLUNIFORM1FVPROC is access procedure (location :        GLint;
												  count    :        GLsizei;
												  value    : access constant GLfloat);

	type PFNGLUNIFORM2FVPROC is access procedure (location :        GLint;
												  count    :        GLsizei;
												  value    : access constant GLfloat);

	type PFNGLUNIFORM3FVPROC is access procedure (location :        GLint;
												  count    :        GLsizei;
												  value    : access constant GLfloat);

	type PFNGLUNIFORM4FVPROC is access procedure (location :        GLint;
												  count    :        GLsizei;
												  value    : access constant GLfloat);

	type PFNGLUNIFORM1IVPROC is access procedure (location :        GLint;
												  count    :        GLsizei;
												  value    : access constant GLint);

	type PFNGLUNIFORM2IVPROC is access procedure (location :        GLint;
												  count    :        GLsizei;
												  value    : access constant GLint);

	type PFNGLUNIFORM3IVPROC is access procedure (location :        GLint;
												  count    :        GLsizei;
												  value    : access constant GLint);

	type PFNGLUNIFORM4IVPROC is access procedure (location :        GLint;
												  count    :        GLsizei;
												  value    : access constant GLint);

	type PFNGLUNIFORMMATRIX2FVPROC is access procedure (location  :        GLint;
														count     :        GLsizei;
														transpose :        GLboolean;
														value     : access constant GLfloat);

	type PFNGLUNIFORMMATRIX3FVPROC is access procedure (location  :        GLint;
														count     :        GLsizei;
														transpose :        GLboolean;
														value     : access constant GLfloat);

	type PFNGLUNIFORMMATRIX4FVPROC is access procedure (location  :        GLint;
														count     :        GLsizei;
														transpose :        GLboolean;
														value     : access constant GLfloat);

	type PFNGLVALIDATEPROGRAMPROC is access procedure (program : GLuint);

	type PFNGLVERTEXATTRIB1DPROC is access procedure (index : GLuint;
													  x : GLdouble);

	type PFNGLVERTEXATTRIB1DVPROC is access procedure (index : GLuint;
													   v : access constant GLdouble);

	type PFNGLVERTEXATTRIB1FPROC is access procedure (index : GLuint;
													  x : GLfloat);

	type PFNGLVERTEXATTRIB1FVPROC is access procedure (index : GLuint;
													   v : access constant GLfloat);

	type PFNGLVERTEXATTRIB1SPROC is access procedure (index : GLuint;
													  x : GLshort);

	type PFNGLVERTEXATTRIB1SVPROC is access procedure (index : GLuint;
													   v : access constant GLshort);

	type PFNGLVERTEXATTRIB2DPROC is access procedure (index : GLuint;
													  x : GLdouble);

	type PFNGLVERTEXATTRIB2DVPROC is access procedure (index : GLuint;
													   v : access constant GLdouble);

	type PFNGLVERTEXATTRIB2FPROC is access procedure (index  :        GLuint;
													  x      :        GLfloat);

	type PFNGLVERTEXATTRIB2FVPROC is access procedure (index :        GLuint;
													   v     : access constant GLfloat);

	type PFNGLVERTEXATTRIB2SPROC is access procedure (index  :        GLuint;
													  x      :        GLshort);

	type PFNGLVERTEXATTRIB2SVPROC is access procedure (index :        GLuint;
													   v     : access constant GLshort);

	type PFNGLVERTEXATTRIB3DPROC is access procedure (index  :        GLuint;
													  x      :        GLdouble);

	type PFNGLVERTEXATTRIB3DVPROC is access procedure (index :        GLuint;
													   v     : access constant GLdouble);

	type PFNGLVERTEXATTRIB3FPROC is access procedure (index  :        GLuint;
													  x      :        GLfloat);

	type PFNGLVERTEXATTRIB3FVPROC is access procedure (index :        GLuint;
													   v     : access constant GLfloat);

	type PFNGLVERTEXATTRIB3SPROC is access procedure (index  :        GLuint;
													  x      :        GLshort);

	type PFNGLVERTEXATTRIB3SVPROC is access procedure (index  :        GLuint;
													   v      : access constant GLshort);

	type PFNGLVERTEXATTRIB4NBVPROC is access procedure (index :        GLuint
														; v   : access constant GLbyte);

	type PFNGLVERTEXATTRIB4NIVPROC is access procedure (index :        GLuint;
														v     : access constant GLint);

	type PFNGLVERTEXATTRIB4NSVPROC is access procedure (index :        GLuint;
														v     : access constant GLshort);

	type PFNGLVERTEXATTRIB4NUBPROC is access procedure (index : GLuint;
														x     : GLubyte;
														y     : GLubyte;
														z     : GLubyte;
														w     : GLubyte);

	type PFNGLVERTEXATTRIB4NUBVPROC is access procedure (index :        GLuint;
														 v     : access constant GLubyte);

	type PFNGLVERTEXATTRIB4NUIVPROC is access procedure (index :        GLuint;
														 v     : access constant GLuint);

	type PFNGLVERTEXATTRIB4NUSVPROC is access procedure (index :        GLuint;
														 v     : access constant GLushort);

	type PFNGLVERTEXATTRIB4BVPROC is access procedure (index   :        GLuint;
													   v       : access constant GLbyte);

	type PFNGLVERTEXATTRIB4DPROC is access procedure (index  :        GLuint;
													  x      :        GLdouble;
													  y      :        GLdouble;
													  z      :        GLdouble;
													  w      :        GLdouble);

	type PFNGLVERTEXATTRIB4DVPROC is access procedure (index :        GLuint;
													   v     : access constant GLdouble);

	type PFNGLVERTEXATTRIB4FPROC is access procedure (index  :        GLuint;
													  x      :        GLfloat;
													  y      :        GLfloat;
													  z      :        GLfloat;
													  w      :        GLfloat);

	type PFNGLVERTEXATTRIB4FVPROC is access procedure (index :        GLuint;
													   v     : access constant GLfloat);

	type PFNGLVERTEXATTRIB4IVPROC is access procedure (index :        GLuint;
													   v     : access constant GLint);

	type PFNGLVERTEXATTRIB4SPROC is access procedure (index  :        GLuint;
													  x      :        GLshort;
													  y      :        GLshort;
													  z      :        GLshort;
													  w      :        GLshort);

	type PFNGLVERTEXATTRIB4SVPROC is access procedure (index  :        GLuint;
													   v      : access constant GLshort);

	type PFNGLVERTEXATTRIB4UBVPROC is access procedure (index :        GLuint;
														v     : access constant GLubyte);

	type PFNGLVERTEXATTRIB4UIVPROC is access procedure (index :        GLuint;
														v     : access constant GLuint);

	type PFNGLVERTEXATTRIB4USVPROC is access procedure (index :        GLuint;
														v     : access constant GLushort);

	type PFNGLVERTEXATTRIBPOINTERPROC is access procedure (index      :        GLuint;
														   size       :        GLint;
														   typ        :        GLenum;
														   normalized :        GLboolean;
														   stride     :        GLsizei;
														   pointer    : GLvoid);

	pragma Convention (Stdcall, PFNGLBLENDEQUATIONSEPARATEPROC);
	pragma Convention (Stdcall, PFNGLDRAWBUFFERSPROC);
	pragma Convention (Stdcall, PFNGLSTENCILOPSEPARATEPROC);
	pragma Convention (Stdcall, PFNGLSTENCILFUNCSEPARATEPROC);
	pragma Convention (Stdcall, PFNGLSTENCILMASKSEPARATEPROC);
	pragma Convention (Stdcall, PFNGLATTACHSHADERPROC);
	pragma Convention (Stdcall, PFNGLBINDATTRIBLOCATIONPROC);
	pragma Convention (Stdcall, PFNGLCOMPILESHADERPROC);
	pragma Convention (Stdcall, PFNGLCREATEPROGRAMPROC);
	pragma Convention (Stdcall, PFNGLCREATESHADERPROC);
	pragma Convention (Stdcall, PFNGLDELETEPROGRAMPROC);
	pragma Convention (Stdcall, PFNGLDELETESHADERPROC);
	pragma Convention (Stdcall, PFNGLDETACHSHADERPROC);
	pragma Convention (Stdcall, PFNGLDISABLEVERTEXATTRIBARRAYPROC);
	pragma Convention (Stdcall, PFNGLENABLEVERTEXATTRIBARRAYPROC);
	pragma Convention (Stdcall, PFNGLGETACTIVEATTRIBPROC);
	pragma Convention (Stdcall, PFNGLGETACTIVEUNIFORMPROC);
	pragma Convention (Stdcall, PFNGLGETATTACHEDSHADERSPROC);
	pragma Convention (Stdcall, PFNGLGETATTRIBLOCATIONPROC);
	pragma Convention (Stdcall, PFNGLGETPROGRAMIVPROC);
	pragma Convention (Stdcall, PFNGLGETPROGRAMINFOLOGPROC);
	pragma Convention (Stdcall, PFNGLGETSHADERIVPROC);
	pragma Convention (Stdcall, PFNGLGETSHADERINFOLOGPROC);
	pragma Convention (Stdcall, PFNGLGETSHADERSOURCEPROC);
	pragma Convention (Stdcall, PFNGLGETUNIFORMLOCATIONPROC);
	pragma Convention (Stdcall, PFNGLGETUNIFORMFVPROC);
	pragma Convention (Stdcall, PFNGLGETUNIFORMIVPROC);
	pragma Convention (Stdcall, PFNGLGETVERTEXATTRIBDVPROC);
	pragma Convention (Stdcall, PFNGLGETVERTEXATTRIBFVPROC);
	pragma Convention (Stdcall, PFNGLGETVERTEXATTRIBIVPROC);
	pragma Convention (Stdcall, PFNGLGETVERTEXATTRIBPOINTERVPROC);
	pragma Convention (Stdcall, PFNGLISPROGRAMPROC);
	pragma Convention (Stdcall, PFNGLISSHADERPROC);
	pragma Convention (Stdcall, PFNGLLINKPROGRAMPROC);
	pragma Convention (Stdcall, PFNGLSHADERSOURCEPROC);
	pragma Convention (Stdcall, PFNGLUSEPROGRAMPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM1FPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM2FPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM3FPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM4FPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM1IPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM2IPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM3IPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM4IPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM1FVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM2FVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM3FVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM4FVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM1IVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM2IVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM3IVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORM4IVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORMMATRIX2FVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORMMATRIX3FVPROC);
	pragma Convention (Stdcall, PFNGLUNIFORMMATRIX4FVPROC);
	pragma Convention (Stdcall, PFNGLVALIDATEPROGRAMPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB1DPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB1DVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB1FPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB1FVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB1SPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB1SVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB2DPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB2DVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB2FPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB2FVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB2SPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB2SVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB3DPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB3DVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB3FPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB3FVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB3SPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB3SVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB4NBVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB4NIVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB4NSVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB4NUBPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB4NUBVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB4NUIVPROC);
	pragma Convention (Stdcall, PFNGLVERTEXATTRIB4NUSVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4BVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4DPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4DVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4FPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4FVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4IVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4SPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4SVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4UBVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIB4USVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBPOINTERPROC);

	glBlendEquationSeparate     : PFNGLBLENDEQUATIONSEPARATEPROC;
	glDrawBuffers               : PFNGLDRAWBUFFERSPROC;
	glStencilOpSeparate         : PFNGLSTENCILOPSEPARATEPROC;
	glStencilFuncSeparate       : PFNGLSTENCILFUNCSEPARATEPROC;
	glStencilMaskSeparate       : PFNGLSTENCILMASKSEPARATEPROC;
	glAttachShader              : PFNGLATTACHSHADERPROC;
	glBindAttribLocation        : PFNGLBINDATTRIBLOCATIONPROC;
	glCompileShader             : PFNGLCOMPILESHADERPROC;
	glCreateProgram             : PFNGLCREATEPROGRAMPROC;
	glCreateShader              : PFNGLCREATESHADERPROC;
	glDeleteProgram             : PFNGLDELETEPROGRAMPROC;
	glDeleteShader              : PFNGLDELETESHADERPROC;
	glDetachShader              : PFNGLDETACHSHADERPROC;
	glDisableVertexAttribArray  : PFNGLDISABLEVERTEXATTRIBARRAYPROC;
	glEnableVertexAttribArray   : PFNGLENABLEVERTEXATTRIBARRAYPROC;
	glGetActiveAttrib           : PFNGLGETACTIVEATTRIBPROC;
	glGetActiveUniform          : PFNGLGETACTIVEUNIFORMPROC;
	glGetAttachedShaders        : PFNGLGETATTACHEDSHADERSPROC;
	glGetAttribLocation         : PFNGLGETATTRIBLOCATIONPROC;
	glGetProgramiv              : PFNGLGETPROGRAMIVPROC;
	glGetProgramInfoLog         : PFNGLGETPROGRAMINFOLOGPROC;
	glGetShaderiv               : PFNGLGETSHADERIVPROC;
	glGetShaderInfoLog          : PFNGLGETSHADERINFOLOGPROC;
	glGetShaderSource           : PFNGLGETSHADERSOURCEPROC;
	glGetUniformLocation        : PFNGLGETUNIFORMLOCATIONPROC;
	glGetUniformfv              : PFNGLGETUNIFORMFVPROC;
	glGetUniformiv              : PFNGLGETUNIFORMIVPROC;
	glGetVertexAttribdv         : PFNGLGETVERTEXATTRIBDVPROC;
	glGetVertexAttribfv         : PFNGLGETVERTEXATTRIBFVPROC;
	glGetVertexAttribiv         : PFNGLGETVERTEXATTRIBIVPROC;
	glGetVertexAttribPointerv   : PFNGLGETVERTEXATTRIBPOINTERVPROC;
	glIsProgram                 : PFNGLISPROGRAMPROC;
	glIsShader                  : PFNGLISSHADERPROC;
	glLinkProgram               : PFNGLLINKPROGRAMPROC;
	glShaderSource              : PFNGLSHADERSOURCEPROC;
	glUseProgram                : PFNGLUSEPROGRAMPROC;
	glUniform1f                 : PFNGLUNIFORM1FPROC;
	glUniform2f                 : PFNGLUNIFORM2FPROC;
	glUniform3f                 : PFNGLUNIFORM3FPROC;
	glUniform4f                 : PFNGLUNIFORM4FPROC;
	glUniform1i                 : PFNGLUNIFORM1IPROC;
	glUniform2i                 : PFNGLUNIFORM2IPROC;
	glUniform3i                 : PFNGLUNIFORM3IPROC;
	glUniform4i                 : PFNGLUNIFORM4IPROC;
	glUniform1fv                : PFNGLUNIFORM1FVPROC;
	glUniform2fv                : PFNGLUNIFORM2FVPROC;
	glUniform3fv                : PFNGLUNIFORM3FVPROC;
	glUniform4fv                : PFNGLUNIFORM4FVPROC;
	glUniform1iv                : PFNGLUNIFORM1IVPROC;
	glUniform2iv                : PFNGLUNIFORM2IVPROC;
	glUniform3iv                : PFNGLUNIFORM3IVPROC;
	glUniform4iv                : PFNGLUNIFORM4IVPROC;
	glUniformMatrix2fv          : PFNGLUNIFORMMATRIX2FVPROC;
	glUniformMatrix3fv          : PFNGLUNIFORMMATRIX3FVPROC;
	glUniformMatrix4fv          : PFNGLUNIFORMMATRIX4FVPROC;
	glValidateProgram           : PFNGLVALIDATEPROGRAMPROC;
	glVertexAttrib1d            : PFNGLVERTEXATTRIB1DPROC;
	glVertexAttrib1dv           : PFNGLVERTEXATTRIB1DVPROC;
	glVertexAttrib1f            : PFNGLVERTEXATTRIB1FPROC;
	glVertexAttrib1fv           : PFNGLVERTEXATTRIB1FVPROC;
	glVertexAttrib1s            : PFNGLVERTEXATTRIB1SPROC;
	glVertexAttrib1sv           : PFNGLVERTEXATTRIB1SVPROC;
	glVertexAttrib2d            : PFNGLVERTEXATTRIB2DPROC;
	glVertexAttrib2dv           : PFNGLVERTEXATTRIB2DVPROC;
	glVertexAttrib2f            : PFNGLVERTEXATTRIB2FPROC;
	glVertexAttrib2fv           : PFNGLVERTEXATTRIB2FVPROC;
	glVertexAttrib2s            : PFNGLVERTEXATTRIB2SPROC;
	glVertexAttrib2sv           : PFNGLVERTEXATTRIB2SVPROC;
	glVertexAttrib3d            : PFNGLVERTEXATTRIB3DPROC;
	glVertexAttrib3dv           : PFNGLVERTEXATTRIB3DVPROC;
	glVertexAttrib3f            : PFNGLVERTEXATTRIB3FPROC;
	glVertexAttrib3fv           : PFNGLVERTEXATTRIB3FVPROC;
	glVertexAttrib3s            : PFNGLVERTEXATTRIB3SPROC;
	glVertexAttrib3sv           : PFNGLVERTEXATTRIB3SVPROC;
	glVertexAttrib4Nbv          : PFNGLVERTEXATTRIB4NBVPROC;
	glVertexAttrib4Niv          : PFNGLVERTEXATTRIB4NIVPROC;
	glVertexAttrib4Nsv          : PFNGLVERTEXATTRIB4NSVPROC;
	glVertexAttrib4Nub          : PFNGLVERTEXATTRIB4NUBPROC;
	glVertexAttrib4Nubv         : PFNGLVERTEXATTRIB4NUBVPROC;
	glVertexAttrib4Nuiv         : PFNGLVERTEXATTRIB4NUIVPROC;
	glVertexAttrib4Nusv         : PFNGLVERTEXATTRIB4NUSVPROC;
	glVertexAttrib4bv           : PFNGLVERTEXATTRIB4BVPROC;
	glVertexAttrib4d            : PFNGLVERTEXATTRIB4DPROC;
	glVertexAttrib4dv           : PFNGLVERTEXATTRIB4DVPROC;
	glVertexAttrib4f            : PFNGLVERTEXATTRIB4FPROC;
	glVertexAttrib4fv           : PFNGLVERTEXATTRIB4FVPROC;
	glVertexAttrib4iv           : PFNGLVERTEXATTRIB4IVPROC;
	glVertexAttrib4s            : PFNGLVERTEXATTRIB4SPROC;
	glVertexAttrib4sv           : PFNGLVERTEXATTRIB4SVPROC;
	glVertexAttrib4ubv          : PFNGLVERTEXATTRIB4UBVPROC;
	glVertexAttrib4uiv          : PFNGLVERTEXATTRIB4UIVPROC;
	glVertexAttrib4usv          : PFNGLVERTEXATTRIB4USVPROC;
	glVertexAttribPointer       : PFNGLVERTEXATTRIBPOINTERPROC;

	----------------
	-- OpenGL 2.1 --
	----------------
	type PFNGLUNIFORMMATRIX2X3FVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLfloat);

	type PFNGLUNIFORMMATRIX3X2FVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLfloat);

	type PFNGLUNIFORMMATRIX2X4FVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLfloat);

	type PFNGLUNIFORMMATRIX4X2FVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLfloat);

	type PFNGLUNIFORMMATRIX3X4FVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLfloat);

	type PFNGLUNIFORMMATRIX4X3FVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLfloat);

	pragma convention (Stdcall, PFNGLUNIFORMMATRIX2X3FVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX3X2FVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX2X4FVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX4X2FVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX3X4FVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX4X3FVPROC);

	glUniformMatrix2x3fv       : PFNGLUNIFORMMATRIX2X3FVPROC;
	glUniformMatrix3x2fv       : PFNGLUNIFORMMATRIX3X2FVPROC;
	glUniformMatrix2x4fv       : PFNGLUNIFORMMATRIX2X4FVPROC;
	glUniformMatrix4x2fv       : PFNGLUNIFORMMATRIX4X2FVPROC;
	glUniformMatrix3x4fv       : PFNGLUNIFORMMATRIX3X4FVPROC;
	glUniformMatrix4x3fv       : PFNGLUNIFORMMATRIX4X3FVPROC;

	----------------
	-- OpenGL 3.0 --
	----------------
	type PFNGLCOLORMASKIPROC is access procedure (index : GLuint;
												  r     : GLboolean;
												  g     : GLboolean;
												  b     : GLboolean;
												  a     : GLboolean);

	type PFNGLGETBOOLEANI_VPROC is access procedure (target :        GLenum;
													 index  :        GLuint;
													 data   : access GLboolean);

	type PFNGLGETINTEGERI_VPROC is access procedure (target :        GLenum;
													 index  :        GLuint;
													 data   : access GLint);

	type PFNGLENABLEIPROC is access procedure (target  : GLenum; index: GLuint);
	type PFNGLDISABLEIPROC is access procedure (target : GLenum; index: GLuint);

	type PFNGLISENABLEDIPROC is access function (target : GLenum;
												 index  : GLuint)
												 return GLboolean;

	type PFNGLBEGINTRANSFORMFEEDBACKPROC is access procedure (primitiveMode : GLenum);

	type PFNGLENDTRANSFORMFEEDBACKPROC is access procedure;

	type PFNGLBINDBUFFERRANGEPROC is access procedure (target : GLenum;
													   index  : GLuint;
													   buffer : GLuint;
													   offset : GLintptr;
													   size   : GLsizeiptr);

	type PFNGLBINDBUFFERBASEPROC is access procedure (target  : GLenum;
													  index   : GLuint;
													  buffer  : GLuint);

	type PFNGLTRANSFORMFEEDBACKVARYINGSPROC is access procedure (program    : GLuint;
																 count      : GLsizei;
																 varyings   : System.Address;
																 bufferMode : GLenum);

	type PFNGLGETTRANSFORMFEEDBACKVARYINGPROC is access procedure (program :        GLuint;
																   index   :        GLuint;
																   bufSize :        GLsizei;
																   length  : access GLsizei;
																   size    : access GLsizei;
																   typee   : access GLenum;
																   name    : access GLchar);

	type PFNGLCLAMPCOLORPROC is access procedure (target : GLenum;
												  clamp  : GLenum);

	type PFNGLBEGINCONDITIONALRENDERPROC is access procedure (id   : GLuint;
															  mode : GLenum);

	type PFNGLENDCONDITIONALRENDERPROC is access procedure;

	type PFNGLVERTEXATTRIBIPOINTERPROC is access procedure (index   : GLuint;
															size    : GLint;
															typee   : GLenum;
															stride  : GLsizei;
															pointer : GLvoid);

	type PFNGLGETVERTEXATTRIBIIVPROC is access procedure (index  :        GLuint;
														  pname  :        GLenum;
														  params : access GLint);

	type PFNGLGETVERTEXATTRIBIUIVPROC is access procedure (index  :        GLuint;
														   pname  :        GLenum;
														   params : access GLuint);

	type PFNGLVERTEXATTRIBI1IPROC is access procedure (index : GLuint;
													   x     : GLint);

	type PFNGLVERTEXATTRIBI2IPROC is access procedure (index : GLuint;
													   x     : GLint;
													   y     : GLint);

	type PFNGLVERTEXATTRIBI3IPROC is access procedure (index  : GLuint;
													   x      : GLint;
													   y      : GLint;
													   z      : GLint);

	type PFNGLVERTEXATTRIBI4IPROC is access procedure (index  : GLuint;
													   x      : GLint;
													   y      : GLint;
													   z      : GLint;
													   W      : GLint);

	type PFNGLVERTEXATTRIBI1UIPROC is access procedure (index : GLuint;
														x     : GLuint);

	type PFNGLVERTEXATTRIBI2UIPROC is access procedure (index : GLuint;
														x     : GLuint;
														y     : GLuint);

	type PFNGLVERTEXATTRIBI3UIPROC is access procedure (index : GLuint;
														x     : GLuint;
														y     : GLuint;
														z     : GLuint);

	type PFNGLVERTEXATTRIBI4UIPROC is access procedure (index : GLuint;
														x     : GLuint;
														y     : GLuint;
														z     : GLuint;
														w     : GLuint);

	type PFNGLVERTEXATTRIBI1IVPROC is access procedure (index  :        GLuint;
														v      : access constant GLint);

	type PFNGLVERTEXATTRIBI2IVPROC is access procedure (index  :        GLuint;
														v      : access constant GLint);

	type PFNGLVERTEXATTRIBI3IVPROC is access procedure (index  :        GLuint;
														v      : access constant GLint);

	type PFNGLVERTEXATTRIBI4IVPROC is access procedure (index  :        GLuint;
														v      : access constant GLint);

	type PFNGLVERTEXATTRIBI1UIVPROC is access procedure (index :        GLuint;
														 v     : access constant GLuint);

	type PFNGLVERTEXATTRIBI2UIVPROC is access procedure (index :        GLuint;
														 v     : access constant GLuint);

	type PFNGLVERTEXATTRIBI3UIVPROC is access procedure (index :        GLuint;
														 v     : access constant GLuint);

	type PFNGLVERTEXATTRIBI4UIVPROC is access procedure (index :        GLuint;
														 v     : access constant GLuint);

	type PFNGLVERTEXATTRIBI4BVPROC is access procedure (index  :        GLuint;
														v      : access constant GLbyte);

	type PFNGLVERTEXATTRIBI4SVPROC is access procedure (index  :        GLuint;
														v      : access constant GLshort);

	type PFNGLVERTEXATTRIBI4UBVPROC is access procedure (index :        GLuint;
														 v     : access constant GLubyte);

	type PFNGLVERTEXATTRIBI4USVPROC is access procedure (index :        GLuint;
														 v     : access constant GLushort);

	type PFNGLGETUNIFORMUIVPROC is access procedure (program  :        GLuint;
													 location :        GLint;
													 params   : access GLuint);

	type PFNGLBINDFRAGDATALOCATIONPROC is access procedure (program :        GLuint;
															color   :        GLuint;
															name    : access constant GLchar);

	type PFNGLGETFRAGDATALOCATIONPROC is access function (program :        GLuint;
														  name    : access constant GLchar)
														  return GLint;

	type PFNGLUNIFORM1UIPROC is access procedure (location : GLint;
												  v0       : GLuint);

	type PFNGLUNIFORM2UIPROC is access procedure (location : GLint;
												  v0       : GLuint;
												  v1       : GLuint);

	type PFNGLUNIFORM3UIPROC is access procedure (location : GLint;
												  v0       : GLuint;
												  v1       : GLuint;
												  v2       : GLuint);

	type PFNGLUNIFORM4UIPROC is access procedure (location : GLint;
												  v0       : GLuint;
												  v1       : GLuint;
												  v2       : GLuint;
												  v3       : GLuint);

	type PFNGLUNIFORM1UIVPROC is access procedure (location :        GLint;
												   count    :        GLsizei;
												   value    : access constant GLuint);

	type PFNGLUNIFORM2UIVPROC is access procedure (location :        GLint;
												   count    :        GLsizei;
												   value    : access constant GLuint);

	type PFNGLUNIFORM3UIVPROC is access procedure (location :        GLint;
												   count    :        GLsizei;
												   value    : access constant GLuint);

	type PFNGLUNIFORM4UIVPROC is access procedure (location :        GLint;
												   count    :        GLsizei;
												   value    : access constant GLuint);

	type PFNGLTEXPARAMETERIIVPROC is access procedure (target :        GLenum;
													   pname  :        GLenum;
													   params : access constant GLint);

	type PFNGLTEXPARAMETERIUIVPROC is access procedure (target :        GLenum;
														pname  :        GLenum;
														params : access constant GLuint);

	type PFNGLGETTEXPARAMETERIIVPROC is access procedure (target :        GLenum;
														  pname  :        GLenum;
														  params : access GLint);

	type PFNGLGETTEXPARAMETERIUIVPROC is access procedure (target :        GLenum;
														   pname  :        GLenum;
														   params : access GLuint);

	type PFNGLCLEARBUFFERIVPROC is access procedure (buffer     :        GLenum;
													 drawbuffer :        GLint;
													 value      : access constant GLint);

	type PFNGLCLEARBUFFERUIVPROC is access procedure (buffer     :        GLenum;
													  drawbuffer :        GLint;
													  value      : access constant GLuint);

	type PFNGLCLEARBUFFERFVPROC is access procedure (buffer     :        GLenum;
													 drawbuffer :        GLint;
													 value      : access constant GLfloat);

	type PFNGLCLEARBUFFERFIPROC is access procedure (buffer     : GLenum;
													 drawbuffer : GLint;
													 depth      : GLfloat;
													 stencil    : GLint);

	type PFNGLGETSTRINGIPROC is access function (name: GLenum;
												 index: GLuint)
												 return Interfaces.C.Strings.chars_ptr;

	type PFNGLISRENDERBUFFERPROC is access function (renderbuffer : GLuint)
													 return GLboolean;

	type PFNGLBINDRENDERBUFFERPROC is access procedure (target : GLenum;
														renderbuffer : GLuint);

	type PFNGLDELETERENDERBUFFERSPROC is access procedure (n : GLsizei;
														   renderbuffers : access constant GLuint);

	type PFNGLGENRENDERBUFFERSPROC is access procedure (n             :        GLsizei;
														renderbuffers : access GLuint);

	type PFNGLRENDERBUFFERSTORAGEPROC is access procedure (target         : GLenum;
														   internalformat : GLenum;
														   width          : GLsizei;
														   height         : GLsizei);

	type PFNGLGETRENDERBUFFERPARAMETERIVPROC is access procedure (target :        GLenum;
																  pname  :        GLenum;
																  params : access GLint);

	type PFNGLISFRAMEBUFFERPROC is access function (framebuffer : GLuint)
													return GLboolean;

	type PFNGLBINDFRAMEBUFFERPROC is access procedure (target      : GLenum;
													   framebuffer : GLuint);

	type PFNGLDELETEFRAMEBUFFERSPROC is access procedure (n : GLsizei;
														  framebuffers : access constant GLuint);

	type PFNGLGENFRAMEBUFFERSPROC is access procedure (n : GLsizei;
													   framebuffers : access GLuint);

	type PFNGLCHECKFRAMEBUFFERSTATUSPROC is access function (target : GLenum)
															 return GLenum;


	type PFNGLFRAMEBUFFERTEXTURE1DPROC is access procedure (target     : GLenum;
															attachment : GLenum;
															textarget  : GLenum;
															texture    : GLuint;
															level      : GLint);

	type PFNGLFRAMEBUFFERTEXTURE2DPROC is access procedure (target     : GLenum;
															attachment : GLenum;
															textarget  : GLenum;
															texture    : GLuint;
															level      : GLint);

	type PFNGLFRAMEBUFFERTEXTURE3DPROC is access procedure (target     : GLenum;
															attachment : GLenum;
															textarget  : GLenum;
															texture    : GLuint;
															level      : GLint;
															layer      : GLint);

	type PFNGLFRAMEBUFFERRENDERBUFFERPROC is access procedure (target             : GLenum;
															   attachment         : GLenum;
															   renderbuffertarget : GLenum;
															   renderbuffer       : GLuint);

	type PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC is access procedure (target : GLenum;
																		   attachment : GLenum;
																		   pname : GLenum;
																		   params : access GLint);

	type PFNGLGENERATEMIPMAPPROC is access procedure (target : GLenum);

	type PFNGLBLITFRAMEBUFFERPROC is access procedure (srcX0  : GLint;
													   srcY0  : GLint;
													   srcX1  : GLint;
													   srcY1  : GLint;
													   dstX0  : GLint;
													   dstY0  : GLint;
													   dstX1  : GLint;
													   dstY1  : GLint;
													   mask   : GLbitfield;
													   filter : GLenum);

	type PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC is access procedure (target         : GLenum;
																	  samples        : GLsizei;
																	  internalformat : GLenum;
																	  width          : GLsizei;
																	  height         : GLsizei);

	type PFNGLFRAMEBUFFERTEXTURELAYERPROC is access procedure (target     : GLenum;
															   attachment : GLenum;
															   texture    : GLuint;
															   level      : GLint;
															   layer      : GLint);

	type PFNGLMAPBUFFERRANGEPROC is access function (target    : GLenum;
													 offset    : GLintptr;
													 length    : GLsizeiptr;
													 accessBit : GLbitfield)
													 return GLvoid;

	type PFNGLFLUSHMAPPEDBUFFERRANGEPROC is access procedure (target : GLenum;
															  offset : GLintptr;
															  length : GLsizeiptr);

	type PFNGLBINDVERTEXARRAYPROC is access procedure (arrayy : GLuint);

	type PFNGLDELETEVERTEXARRAYSPROC is access procedure (n : GLsizei;
														  arrays: access constant GLuint);

	type PFNGLGENVERTEXARRAYSPROC is access procedure (n : GLsizei;
													   arrays: access GLuint);

	type PFNGLISVERTEXARRAYPROC is access function (arrayy : GLuint)
													return GLboolean;


	pragma convention (Stdcall, PFNGLCOLORMASKIPROC);
	pragma convention (Stdcall, PFNGLGETBOOLEANI_VPROC);
	pragma convention (Stdcall, PFNGLGETINTEGERI_VPROC);
	pragma convention (Stdcall, PFNGLENABLEIPROC);
	pragma convention (Stdcall, PFNGLDISABLEIPROC);
	pragma convention (Stdcall, PFNGLISENABLEDIPROC);
	pragma convention (Stdcall, PFNGLBEGINTRANSFORMFEEDBACKPROC);
	pragma convention (Stdcall, PFNGLENDTRANSFORMFEEDBACKPROC);
	pragma convention (Stdcall, PFNGLBINDBUFFERRANGEPROC);
	pragma convention (Stdcall, PFNGLBINDBUFFERBASEPROC);
	pragma convention (Stdcall, PFNGLTRANSFORMFEEDBACKVARYINGSPROC);
	pragma convention (Stdcall, PFNGLGETTRANSFORMFEEDBACKVARYINGPROC);
	pragma convention (Stdcall, PFNGLCLAMPCOLORPROC);
	pragma convention (Stdcall, PFNGLBEGINCONDITIONALRENDERPROC);
	pragma convention (Stdcall, PFNGLENDCONDITIONALRENDERPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBIPOINTERPROC);
	pragma convention (Stdcall, PFNGLGETVERTEXATTRIBIIVPROC);
	pragma convention (Stdcall, PFNGLGETVERTEXATTRIBIUIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI1IPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI2IPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI3IPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI4IPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI1UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI2UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI3UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI4UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI1IVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI2IVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI3IVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI4IVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI1UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI2UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI3UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI4UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI4BVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI4SVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI4UBVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBI4USVPROC);
	pragma convention (Stdcall, PFNGLGETUNIFORMUIVPROC);
	pragma convention (Stdcall, PFNGLBINDFRAGDATALOCATIONPROC);
	pragma convention (Stdcall, PFNGLGETFRAGDATALOCATIONPROC);
	pragma convention (Stdcall, PFNGLUNIFORM1UIPROC);
	pragma convention (Stdcall, PFNGLUNIFORM2UIPROC);
	pragma convention (Stdcall, PFNGLUNIFORM3UIPROC);
	pragma convention (Stdcall, PFNGLUNIFORM4UIPROC);
	pragma convention (Stdcall, PFNGLUNIFORM1UIVPROC);
	pragma convention (Stdcall, PFNGLUNIFORM2UIVPROC);
	pragma convention (Stdcall, PFNGLUNIFORM3UIVPROC);
	pragma convention (Stdcall, PFNGLUNIFORM4UIVPROC);
	pragma convention (Stdcall, PFNGLTEXPARAMETERIIVPROC);
	pragma convention (Stdcall, PFNGLTEXPARAMETERIUIVPROC);
	pragma convention (Stdcall, PFNGLGETTEXPARAMETERIIVPROC);
	pragma convention (Stdcall, PFNGLGETTEXPARAMETERIUIVPROC);
	pragma convention (Stdcall, PFNGLCLEARBUFFERIVPROC);
	pragma convention (Stdcall, PFNGLCLEARBUFFERUIVPROC);
	pragma convention (Stdcall, PFNGLCLEARBUFFERFVPROC);
	pragma convention (Stdcall, PFNGLCLEARBUFFERFIPROC);
	pragma convention (Stdcall, PFNGLGETSTRINGIPROC);
	pragma convention (Stdcall, PFNGLISRENDERBUFFERPROC);
	pragma convention (Stdcall, PFNGLBINDRENDERBUFFERPROC);
	pragma convention (Stdcall, PFNGLDELETERENDERBUFFERSPROC);
	pragma convention (Stdcall, PFNGLGENRENDERBUFFERSPROC);
	pragma convention (Stdcall, PFNGLRENDERBUFFERSTORAGEPROC);
	pragma convention (Stdcall, PFNGLGETRENDERBUFFERPARAMETERIVPROC);
	pragma convention (Stdcall, PFNGLISFRAMEBUFFERPROC);
	pragma convention (Stdcall, PFNGLBINDFRAMEBUFFERPROC);
	pragma convention (Stdcall, PFNGLDELETEFRAMEBUFFERSPROC);
	pragma convention (Stdcall, PFNGLGENFRAMEBUFFERSPROC);
	pragma convention (Stdcall, PFNGLCHECKFRAMEBUFFERSTATUSPROC);
	pragma convention (Stdcall, PFNGLFRAMEBUFFERTEXTURE1DPROC);
	pragma convention (Stdcall, PFNGLFRAMEBUFFERTEXTURE2DPROC);
	pragma convention (Stdcall, PFNGLFRAMEBUFFERTEXTURE3DPROC);
	pragma convention (Stdcall, PFNGLFRAMEBUFFERRENDERBUFFERPROC);
	pragma convention (Stdcall, PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC);
	pragma convention (Stdcall, PFNGLGENERATEMIPMAPPROC);
	pragma convention (Stdcall, PFNGLBLITFRAMEBUFFERPROC);
	pragma convention (Stdcall, PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC);
	pragma convention (Stdcall, PFNGLFRAMEBUFFERTEXTURELAYERPROC);
	pragma convention (Stdcall, PFNGLMAPBUFFERRANGEPROC);
	pragma convention (Stdcall, PFNGLFLUSHMAPPEDBUFFERRANGEPROC);
	pragma convention (Stdcall, PFNGLBINDVERTEXARRAYPROC);
	pragma convention (Stdcall, PFNGLDELETEVERTEXARRAYSPROC);
	pragma convention (Stdcall, PFNGLGENVERTEXARRAYSPROC);
	pragma convention (Stdcall, PFNGLISVERTEXARRAYPROC);

	glColorMaski                  : PFNGLCOLORMASKIPROC;
	glGetBooleani_v               : PFNGLGETBOOLEANI_VPROC;
	glGetIntegeri_v               : PFNGLGETINTEGERI_VPROC;
	glEnablei                     : PFNGLENABLEIPROC;
	glDisablei                    : PFNGLDISABLEIPROC;
	glIsEnabledi                  : PFNGLISENABLEDIPROC;
	glBeginTransformFeedback      : PFNGLBEGINTRANSFORMFEEDBACKPROC;
	glEndTransformFeedback        : PFNGLENDTRANSFORMFEEDBACKPROC;
	glBindBufferRange             : PFNGLBINDBUFFERRANGEPROC;
	glBindBufferBase              : PFNGLBINDBUFFERBASEPROC;
	glTransformFeedbackVaryings   : PFNGLTRANSFORMFEEDBACKVARYINGSPROC;
	glGetTransformFeedbackVarying : PFNGLGETTRANSFORMFEEDBACKVARYINGPROC;
	glClampColor                  : PFNGLCLAMPCOLORPROC;
	glBeginConditionalRender      : PFNGLBEGINCONDITIONALRENDERPROC;
	glEndConditionalRender        : PFNGLENDCONDITIONALRENDERPROC;
	glVertexAttribIPointer        : PFNGLVERTEXATTRIBIPOINTERPROC;
	glGetVertexAttribIiv          : PFNGLGETVERTEXATTRIBIIVPROC;
	glGetVertexAttribIuiv         : PFNGLGETVERTEXATTRIBIUIVPROC;
	glVertexAttribI1i             : PFNGLVERTEXATTRIBI1IPROC;
	glVertexAttribI2i             : PFNGLVERTEXATTRIBI2IPROC;
	glVertexAttribI3i             : PFNGLVERTEXATTRIBI3IPROC;
	glVertexAttribI4i             : PFNGLVERTEXATTRIBI4IPROC;
	glVertexAttribI1ui            : PFNGLVERTEXATTRIBI1UIPROC;
	glVertexAttribI2ui            : PFNGLVERTEXATTRIBI2UIPROC;
	glVertexAttribI3ui            : PFNGLVERTEXATTRIBI3UIPROC;
	glVertexAttribI4ui            : PFNGLVERTEXATTRIBI4UIPROC;
	glVertexAttribI1iv            : PFNGLVERTEXATTRIBI1IVPROC;
	glVertexAttribI2iv            : PFNGLVERTEXATTRIBI2IVPROC;
	glVertexAttribI3iv            : PFNGLVERTEXATTRIBI3IVPROC;
	glVertexAttribI4iv            : PFNGLVERTEXATTRIBI4IVPROC;
	glVertexAttribI1uiv           : PFNGLVERTEXATTRIBI1UIVPROC;
	glVertexAttribI2uiv           : PFNGLVERTEXATTRIBI2UIVPROC;
	glVertexAttribI3uiv           : PFNGLVERTEXATTRIBI3UIVPROC;
	glVertexAttribI4uiv           : PFNGLVERTEXATTRIBI4UIVPROC;
	glVertexAttribI4bv            : PFNGLVERTEXATTRIBI4BVPROC;
	glVertexAttribI4sv            : PFNGLVERTEXATTRIBI4SVPROC;
	glVertexAttribI4ubv           : PFNGLVERTEXATTRIBI4UBVPROC;
	glVertexAttribI4usv           : PFNGLVERTEXATTRIBI4USVPROC;
	glGetUniformuiv               : PFNGLGETUNIFORMUIVPROC;
	glBindFragDataLocation        : PFNGLBINDFRAGDATALOCATIONPROC;
	glGetFragDataLocation         : PFNGLGETFRAGDATALOCATIONPROC;
	glUniform1ui                  : PFNGLUNIFORM1UIPROC;
	glUniform2ui                  : PFNGLUNIFORM2UIPROC;
	glUniform3ui                  : PFNGLUNIFORM3UIPROC;
	glUniform4ui                  : PFNGLUNIFORM4UIPROC;
	glUniform1uiv                 : PFNGLUNIFORM1UIVPROC;
	glUniform2uiv                 : PFNGLUNIFORM2UIVPROC;
	glUniform3uiv                 : PFNGLUNIFORM3UIVPROC;
	glUniform4uiv                 : PFNGLUNIFORM4UIVPROC;
	glTexParameterIiv             : PFNGLTEXPARAMETERIIVPROC;
	glTexParameterIuiv            : PFNGLTEXPARAMETERIUIVPROC;
	glGetTexParameterIiv          : PFNGLGETTEXPARAMETERIIVPROC;
	glGetTexParameterIuiv         : PFNGLGETTEXPARAMETERIUIVPROC;
	glClearBufferiv               : PFNGLCLEARBUFFERIVPROC;
	glClearBufferuiv              : PFNGLCLEARBUFFERUIVPROC;
	glClearBufferfv               : PFNGLCLEARBUFFERFVPROC;
	glClearBufferfi               : PFNGLCLEARBUFFERFIPROC;
	glGetStringi                  : PFNGLGETSTRINGIPROC;
	glIsRenderbuffer                      : PFNGLISRENDERBUFFERPROC;
	glBindRenderbuffer                    : PFNGLBINDRENDERBUFFERPROC;
	glDeleteRenderbuffers                 : PFNGLDELETERENDERBUFFERSPROC;
	glGenRenderbuffers                    : PFNGLGENRENDERBUFFERSPROC;
	glRenderbufferStorage                 : PFNGLRENDERBUFFERSTORAGEPROC;
	glGetRenderbufferParameteriv          : PFNGLGETRENDERBUFFERPARAMETERIVPROC;
	glIsFramebuffer                       : PFNGLISFRAMEBUFFERPROC;
	glBindFramebuffer                     : PFNGLBINDFRAMEBUFFERPROC;
	glDeleteFramebuffers                  : PFNGLDELETEFRAMEBUFFERSPROC;
	glGenFramebuffers                     : PFNGLGENFRAMEBUFFERSPROC;
	glCheckFramebufferStatus              : PFNGLCHECKFRAMEBUFFERSTATUSPROC;
	glFramebufferTexture1D                : PFNGLFRAMEBUFFERTEXTURE1DPROC;
	glFramebufferTexture2D                : PFNGLFRAMEBUFFERTEXTURE2DPROC;
	glFramebufferTexture3D                : PFNGLFRAMEBUFFERTEXTURE3DPROC;
	glFramebufferRenderbuffer             : PFNGLFRAMEBUFFERRENDERBUFFERPROC;
	glGetFramebufferAttachmentParameteriv : PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC;
	glGenerateMipmap                      : PFNGLGENERATEMIPMAPPROC;
	glBlitFramebuffer                     : PFNGLBLITFRAMEBUFFERPROC;
	glRenderbufferStorageMultisample      : PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC;
	glFramebufferTextureLayer             : PFNGLFRAMEBUFFERTEXTURELAYERPROC;
	glMapBufferRange         : PFNGLMAPBUFFERRANGEPROC;
	glFlushMappedBufferRange : PFNGLFLUSHMAPPEDBUFFERRANGEPROC;
	glBindVertexArray    : PFNGLBINDVERTEXARRAYPROC;
	glDeleteVertexArrays : PFNGLDELETEVERTEXARRAYSPROC;
	glGenVertexArrays    : PFNGLGENVERTEXARRAYSPROC;
	glIsVertexArray      : PFNGLISVERTEXARRAYPROC;

	----------------
	-- OpenGL 3.1 --
	----------------
	type PFNGLDRAWARRAYSINSTANCEDPROC is access procedure (mode      : GLenum;
														   first     : GLint;
														   count     : GLsizei;
														   primcount : GLsizei);

	type PFNGLDRAWELEMENTSINSTANCEDPROC is access procedure (mode      :        GLenum;
															 count     :        GLsizei;
															 typee     :        GLenum;
															 indices   : GLvoid;
															 primcount :        GLsizei);

	type PFNGLTEXBUFFERPROC is access procedure (target         : GLenum;
												 internalformat : GLenum;
												 buffer         : GLuint);

	type PFNGLPRIMITIVERESTARTINDEXPROC is access procedure (index : GLuint);

	type PFNGLCOPYBUFFERSUBDATAPROC is access procedure (readtarget  : GLenum;
														 writetarget : GLenum;
														 readoffset  : GLintptr;
														 writeoffset : GLintptr;
														 size        : GLsizeiptr);

	type PFNGLGETUNIFORMINDICESPROC is access procedure (program        :        GLuint;
														 uniformCount   :        GLsizei;
														 uniformNames   :        System.Address;
														 uniformIndices : access GLuint);

	type PFNGLGETACTIVEUNIFORMSIVPROC is access procedure (program        :        GLuint;
														   uniformCount   :        GLsizei;
														   uniformIndices : access constant GLuint;
														   pname          :        GLenum;
														   params         : access GLint);

	type PFNGLGETACTIVEUNIFORMNAMEPROC is access procedure (program      :        GLuint;
															uniformIndex :        GLuint;
															bufSize      :        GLsizei;
															length       : access GLsizei;
															uniformName  : access GLchar);

	type PFNGLGETUNIFORMBLOCKINDEXPROC is access function (program          :        GLuint;
														   uniformBlockName : access constant GLchar)
														   return GLuint;

	type PFNGLGETACTIVEUNIFORMBLOCKIVPROC is access procedure (program           :        GLuint;
															   uniformBlockIndex :        GLuint;
															   pname             :        GLenum;
															   params            : access GLint);

	type PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC is access procedure (program           :        GLuint;
																 uniformBlockIndex :        GLuint;
																 bufSize           :        GLsizei;
																 length            : access GLsizei;
																 uniformBlockName  : access GLchar);

	type PFNGLUNIFORMBLOCKBINDINGPROC is access procedure (program             : GLuint;
														   uniformBlockIndex   : GLuint;
														   uniformBlockBinding : GLuint);

	pragma convention (Stdcall, PFNGLDRAWARRAYSINSTANCEDPROC);
	pragma convention (Stdcall, PFNGLDRAWELEMENTSINSTANCEDPROC);
	pragma convention (Stdcall, PFNGLTEXBUFFERPROC);
	pragma convention (Stdcall, PFNGLPRIMITIVERESTARTINDEXPROC);
	pragma convention (Stdcall, PFNGLCOPYBUFFERSUBDATAPROC);
	pragma convention (Stdcall, PFNGLGETUNIFORMINDICESPROC);
	pragma convention (Stdcall, PFNGLGETACTIVEUNIFORMSIVPROC);
	pragma convention (Stdcall, PFNGLGETACTIVEUNIFORMNAMEPROC);
	pragma convention (Stdcall, PFNGLGETUNIFORMBLOCKINDEXPROC);
	pragma convention (Stdcall, PFNGLGETACTIVEUNIFORMBLOCKIVPROC);
	pragma convention (Stdcall, PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC);
	pragma convention (Stdcall, PFNGLUNIFORMBLOCKBINDINGPROC);

	glDrawArraysInstanced   : PFNGLDRAWARRAYSINSTANCEDPROC;
	glDrawElementsInstanced : PFNGLDRAWELEMENTSINSTANCEDPROC;
	glTexBuffer             : PFNGLTEXBUFFERPROC;
	glPrimitiveRestartIndex : PFNGLPRIMITIVERESTARTINDEXPROC;
	glCopyBufferSubData     : PFNGLCOPYBUFFERSUBDATAPROC;
	glGetUniformIndices         : PFNGLGETUNIFORMINDICESPROC;
	glGetActiveUniformsiv       : PFNGLGETACTIVEUNIFORMSIVPROC;
	glGetActiveUniformName      : PFNGLGETACTIVEUNIFORMNAMEPROC;
	glGetUniformBlockIndex      : PFNGLGETUNIFORMBLOCKINDEXPROC;
	glGetActiveUniformBlockiv   : PFNGLGETACTIVEUNIFORMBLOCKIVPROC;
	glGetActiveUniformBlockName : PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC;
	glUniformBlockBinding       : PFNGLUNIFORMBLOCKBINDINGPROC;

	----------------
	-- OpenGL 3.2 --
	----------------
	type PFNGLDRAWELEMENTSBASEVERTEXPROC is access procedure (mode       :        GLenum;
															  count      :        GLsizei;
															  typee      :        GLenum;
															  indices    : GLvoid;
															  basevertex :        GLint);

	type PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC is access procedure (mode       :        GLenum;
																   start      :        GLuint;
																   endd       :        GLuint;
																   count      :        GLsizei;
																   typee      :        GLenum;
																   indices    : GLvoid;
																   basevertex :        GLint);

	type PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC is access procedure (mode       :        GLenum;
																	   count      :        GLsizei;
																	   typee      :        GLenum;
																	   indices    : GLvoid;
																	   primcount  :        GLsizei;
																	   basevertex :        GLint);

	type PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC is access procedure (mode       :        GLenum;
																   count      : access constant GLsizei;
																   typee      :        GLenum;
																   indices    :        System.Address;
																   primcount  :        GLsizei;
																   basevertex : access constant GLint);

	type PFNGLPROVOKINGVERTEXPROC is access procedure (mode: GLenum);
	
	type PFNGLFENCESYNCPROC is access function (condition : GLenum;
												flags     : GLbitfield)
												return GLsync;

	type PFNGLISSYNCPROC is access function (sync : GLsync)
											 return GLboolean;

	type PFNGLDELETESYNCPROC is access procedure (sync : GLsync);

	type PFNGLCLIENTWAITSYNCPROC is access function (sync  : GLsync;
													 flags   : GLbitfield;
													 timeout : GLuint64)
													 return GLenum;

	type PFNGLWAITSYNCPROC is access procedure (sync  : GLsync;
												flags   : GLbitfield;
												timeout : GLuint64);

	type PFNGLGETINTEGER64VPROC is access procedure (pname  : GLenum;
													 params : access GLint64);

	type PFNGLGETSYNCIVPROC is access procedure (sync  :        GLsync;
												 pname   :        GLenum;
												 bufSize :        GLsizei;
												 length  : access GLsizei;
												 values  : access GLint);

	type PFNGLGETINTEGER64I_VPROC is access procedure (target :        GLenum;
													   index  :        GLuint;
													   data   : access GLint64);

	type PFNGLGETBUFFERPARAMETERI64VPROC is access procedure (target :        GLenum;
															  pname  :        GLenum;
															  params : access GLint64);

	type PFNGLFRAMEBUFFERTEXTUREPROC is access procedure (target     : GLenum;
														  attachment : GLenum;
														  texture    : GLuint;
														  level      : GLint);
	type PFNGLTEXIMAGE2DMULTISAMPLEPROC is access procedure (target               : GLenum;
															 samples              : GLsizei;
															 internalformat       : GLint;
															 width                : GLsizei;
															 height               : GLsizei;
															 fixedsamplelocations : GLboolean);

	type PFNGLTEXIMAGE3DMULTISAMPLEPROC is access procedure (target               : GLenum;
															 samples              : GLsizei;
															 internalformat       : GLint;
															 width                : GLsizei;
															 height               : GLsizei;
															 depth                : GLsizei;
															 fixedsamplelocations : GLboolean);

	type PFNGLGETMULTISAMPLEFVPROC is access procedure (pname :        GLenum;
														index :        GLuint;
														val   : access GLfloat);

	type PFNGLSAMPLEMASKIPROC is access procedure (index : GLuint;
												   mask  : GLbitfield);

	pragma convention (Stdcall, PFNGLDRAWELEMENTSBASEVERTEXPROC);
	pragma convention (Stdcall, PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC);
	pragma convention (Stdcall, PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC);
	pragma convention (Stdcall, PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC);
	pragma convention (Stdcall, PFNGLPROVOKINGVERTEXPROC);
	pragma convention (Stdcall, PFNGLFENCESYNCPROC);
	pragma convention (Stdcall, PFNGLISSYNCPROC);
	pragma convention (Stdcall, PFNGLDELETESYNCPROC);
	pragma convention (Stdcall, PFNGLCLIENTWAITSYNCPROC);
	pragma convention (Stdcall, PFNGLWAITSYNCPROC);
	pragma convention (Stdcall, PFNGLGETINTEGER64VPROC);
	pragma convention (Stdcall, PFNGLGETSYNCIVPROC);
	pragma convention (Stdcall, PFNGLGETINTEGER64I_VPROC);
	pragma convention (Stdcall, PFNGLGETBUFFERPARAMETERI64VPROC);
	pragma convention (Stdcall, PFNGLFRAMEBUFFERTEXTUREPROC);
	pragma convention (Stdcall, PFNGLTEXIMAGE2DMULTISAMPLEPROC);
	pragma convention (Stdcall, PFNGLTEXIMAGE3DMULTISAMPLEPROC);
	pragma convention (Stdcall, PFNGLGETMULTISAMPLEFVPROC);
	pragma convention (Stdcall, PFNGLSAMPLEMASKIPROC);

	glDrawElementsBaseVertex          : PFNGLDRAWELEMENTSBASEVERTEXPROC;
	glDrawRangeElementsBaseVertex     : PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC;
	glDrawElementsInstancedBaseVertex : PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC;
	glMultiDrawElementsBaseVertex     : PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC;
	glProvokingVertex : PFNGLPROVOKINGVERTEXPROC;
	glFenceSync      : PFNGLFENCESYNCPROC;
	glIsSync         : PFNGLISSYNCPROC;
	glDeleteSync     : PFNGLDELETESYNCPROC;
	glClientWaitSync : PFNGLCLIENTWAITSYNCPROC;
	glWaitSync       : PFNGLWAITSYNCPROC;
	glGetInteger64v  : PFNGLGETINTEGER64VPROC;
	glGetSynciv      : PFNGLGETSYNCIVPROC;
	glGetInteger64i_v        : PFNGLGETINTEGER64I_VPROC;
	glGetBufferParameteri64v : PFNGLGETBUFFERPARAMETERI64VPROC;
	glFramebufferTexture     : PFNGLFRAMEBUFFERTEXTUREPROC;
	glTexImage2DMultisample : PFNGLTEXIMAGE2DMULTISAMPLEPROC;
	glTexImage3DMultisample : PFNGLTEXIMAGE3DMULTISAMPLEPROC;
	glGetMultisamplefv      : PFNGLGETMULTISAMPLEFVPROC;
	glSampleMaski           : PFNGLSAMPLEMASKIPROC;

	----------------
	-- OpenGL 3.3 --
	----------------
	type PFNGLBINDFRAGDATALOCATIONINDEXEDPROC is access procedure (program     :        GLuint;
																   colorNumber :        GLuint;
																   index       :        GLuint;
																   name        : access constant GLchar);

	type PFNGLGETFRAGDATAINDEXPROC is access function (program :        GLuint;
													   name    : access constant GLchar)
													   return GLint;

	type PFNGLGENSAMPLERSPROC is access procedure (count       : GLsizei;
												   samplers    : access GLuint);

	type PFNGLDELETESAMPLERSPROC is access procedure (count    : GLsizei;
													  samplers : access constant GLuint);

	type PFNGLISSAMPLERPROC is access function (sampler        : GLuint)
												return GLboolean;

	type PFNGLBINDSAMPLERPROC is access procedure (unit        : GLuint;
												   sampler     : GLuint);

	type PFNGLSAMPLERPARAMETERIPROC is access procedure (sampler  : GLuint;
														 pname    : GLenum;
														 param    : GLint);

	type PFNGLSAMPLERPARAMETERIVPROC is access procedure (sampler : GLuint;
														  pname   : GLenum;
														  params  : access constant GLint);

	type PFNGLSAMPLERPARAMETERFPROC is access procedure (sampler  : GLuint;
														 pname    : GLenum;
														 param    : GLfloat);

	type PFNGLSAMPLERPARAMETERFVPROC is access procedure (sampler : GLuint;
														  pname   : GLenum;
														  params  : access constant GLfloat);

	type PFNGLSAMPLERPARAMETERIIVPROC is access procedure (sampler   : GLuint;
														   pname     : GLenum;
														   params    : access constant GLint);

	type PFNGLSAMPLERPARAMETERIUIVPROC is access procedure (sampler  : GLuint;
															pname    : GLenum;
															params   : access constant GLuint);

	type PFNGLGETSAMPLERPARAMETERIVPROC is access procedure (sampler : GLuint;
															 pname   : GLenum;
															 params  : access GLint);

	type PFNGLGETSAMPLERPARAMETERIIVPROC is access procedure (sampler  : GLuint;
															  pname    : GLenum;
															  params   : access GLint);

	type PFNGLGETSAMPLERPARAMETERFVPROC is access procedure (sampler   : GLuint;
															 pname     : GLenum;
															 params    : access GLfloat);

	type PFNGLGETSAMPLERPARAMETERIUIVPROC is access procedure (sampler : GLuint;
															   pname   : GLenum;
															   params  : access GLuint);

	type PFNGLQUERYCOUNTERPROC is access procedure (id     : GLuint;
													target : GLenum);

	type PFNGLGETQUERYOBJECTI64VPROC is access procedure (id     :        GLuint;
														  pname  :        GLenum;
														  params : access GLint64);

	type PFNGLGETQUERYOBJECTUI64VPROC is access procedure (id     :        GLuint;
														   pname  :        GLenum;
														   params : access GLuint64);

	type PFNGLVERTEXATTRIBDIVISORPROC is access procedure (index : GLuint;
														   divisiror : GLuint);

	type PFNGLVERTEXATTRIBP1UIPROC is access procedure (index       :        GLuint;
														typee       :        GLenum;
														normalized  :        GLboolean;
														value       :        GLuint);

	type PFNGLVERTEXATTRIBP1UIVPROC is access procedure (index      :        GLuint;
														 typee      :        GLenum;
														 normalized :        GLboolean;
														 value      : access constant GLuint);

	type PFNGLVERTEXATTRIBP2UIPROC is access procedure (index       :        GLuint;
														typee       :        GLenum;
														normalized  :        GLboolean;
														value       :        GLuint);

	type PFNGLVERTEXATTRIBP2UIVPROC is access procedure (index      :        GLuint;
														 typee      :        GLenum;
														 normalized :        GLboolean;
														 value      : access constant GLuint);

	type PFNGLVERTEXATTRIBP3UIPROC is access procedure (index       :        GLuint;
														typee       :        GLenum;
														normalized  :        GLboolean;
														value       :        GLuint);

	type PFNGLVERTEXATTRIBP3UIVPROC is access procedure (index      :        GLuint;
														 typee      :        GLenum;
														 normalized :        GLboolean;
														 value      : access constant GLuint);

	type PFNGLVERTEXATTRIBP4UIPROC is access procedure (index       :        GLuint;
														typee       :        GLenum;
														normalized  :        GLboolean;
														value       :        GLuint);

	type PFNGLVERTEXATTRIBP4UIVPROC is access procedure (index      :        GLuint;
														 typee      :        GLenum;
														 normalized :        GLboolean;
														 value      : access constant GLuint);

	pragma convention (Stdcall, PFNGLBINDFRAGDATALOCATIONINDEXEDPROC);
	pragma convention (Stdcall, PFNGLGETFRAGDATAINDEXPROC);
	pragma convention (Stdcall, PFNGLGENSAMPLERSPROC);
	pragma convention (Stdcall, PFNGLDELETESAMPLERSPROC);
	pragma convention (Stdcall, PFNGLISSAMPLERPROC);
	pragma convention (Stdcall, PFNGLBINDSAMPLERPROC);
	pragma convention (Stdcall, PFNGLSAMPLERPARAMETERIPROC);
	pragma convention (Stdcall, PFNGLSAMPLERPARAMETERIVPROC);
	pragma convention (Stdcall, PFNGLSAMPLERPARAMETERFPROC);
	pragma convention (Stdcall, PFNGLSAMPLERPARAMETERFVPROC);
	pragma convention (Stdcall, PFNGLSAMPLERPARAMETERIIVPROC);
	pragma convention (Stdcall, PFNGLSAMPLERPARAMETERIUIVPROC);
	pragma convention (Stdcall, PFNGLGETSAMPLERPARAMETERIVPROC);
	pragma convention (Stdcall, PFNGLGETSAMPLERPARAMETERIIVPROC);
	pragma convention (Stdcall, PFNGLGETSAMPLERPARAMETERFVPROC);
	pragma convention (Stdcall, PFNGLGETSAMPLERPARAMETERIUIVPROC);
	pragma convention (Stdcall, PFNGLQUERYCOUNTERPROC);
	pragma convention (Stdcall, PFNGLGETQUERYOBJECTI64VPROC);
	pragma convention (Stdcall, PFNGLGETQUERYOBJECTUI64VPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBDIVISORPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBP1UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBP1UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBP2UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBP2UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBP3UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBP3UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBP4UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBP4UIVPROC);

	glBindFragDataLocationIndexed   : PFNGLBINDFRAGDATALOCATIONINDEXEDPROC;
	glGetFragDataIndex              : PFNGLGETFRAGDATAINDEXPROC;
	glGenSamplers             : PFNGLGENSAMPLERSPROC;
	glDeleteSamplers          : PFNGLDELETESAMPLERSPROC;
	glIsSampler               : PFNGLISSAMPLERPROC;
	glBindSampler             : PFNGLBINDSAMPLERPROC;
	glSamplerParameteri       : PFNGLSAMPLERPARAMETERIPROC;
	glSamplerParameteriv      : PFNGLSAMPLERPARAMETERIVPROC;
	glSamplerParameterf       : PFNGLSAMPLERPARAMETERFPROC;
	glSamplerParameterfv      : PFNGLSAMPLERPARAMETERFVPROC;
	glSamplerParameterIiv     : PFNGLSAMPLERPARAMETERIIVPROC;
	glSamplerParameterIuiv    : PFNGLSAMPLERPARAMETERIUIVPROC;
	glGetSamplerParameteriv   : PFNGLGETSAMPLERPARAMETERIVPROC;
	glGetSamplerParameterIiv  : PFNGLGETSAMPLERPARAMETERIIVPROC;
	glGetSamplerParameterfv   : PFNGLGETSAMPLERPARAMETERFVPROC;
	glGetSamplerParameterIuiv : PFNGLGETSAMPLERPARAMETERIUIVPROC;
	glQueryCounter        : PFNGLQUERYCOUNTERPROC;
	glGetQueryObjecti64v  : PFNGLGETQUERYOBJECTI64VPROC;
	glGetQueryObjectui64v : PFNGLGETQUERYOBJECTUI64VPROC;
	glVertexAttribDivisor : PFNGLVERTEXATTRIBDIVISORPROC;
	glVertexAttribP1ui    : PFNGLVERTEXATTRIBP1UIPROC;
	glVertexAttribP1uiv   : PFNGLVERTEXATTRIBP1UIVPROC;
	glVertexAttribP2ui    : PFNGLVERTEXATTRIBP2UIPROC;
	glVertexAttribP2uiv   : PFNGLVERTEXATTRIBP2UIVPROC;
	glVertexAttribP3ui    : PFNGLVERTEXATTRIBP3UIPROC;
	glVertexAttribP3uiv   : PFNGLVERTEXATTRIBP3UIVPROC;
	glVertexAttribP4ui    : PFNGLVERTEXATTRIBP4UIPROC;
	glVertexAttribP4uiv   : PFNGLVERTEXATTRIBP4UIVPROC;

	----------------
	-- OpenGL 4.0 --
	----------------
	type PFNGLMINSAMPLESHADINGPROC is access procedure (value : GLfloat);

	type PFNGLBLENDEQUATIONIPROC is access procedure (buf : GLuint;
													  mode : GLenum);

	type PFNGLBLENDEQUATIONSEPARATEIPROC is access procedure (buf       : GLuint;
															  modeRGB   : GLenum;
															  modeAlpha : GLenum);

	type PFNGLBLENDFUNCIPROC is access procedure (buf : GLuint;
												  src : GLenum;
												  dst : GLenum);

	type PFNGLBLENDFUNCSEPARATEIPROC is access procedure (buf      : GLuint;
														  srcRGB   : GLenum;
														  dstRGB   : GLenum;
														  srcAlpha : GLenum;
														  dstAlpha : GLenum);

	type PFNGLDRAWARRAYSINDIRECTPROC is access procedure (mode     : GLenum;
														  indirect : GLvoid);

	type PFNGLDRAWELEMENTSINDIRECTPROC is access procedure (mode     : GLenum;
															typee    : GLenum;
															indirect : GLvoid);

	type PFNGLUNIFORM1DPROC is access procedure (location : GLint;
												 x : GLdouble);

	type PFNGLUNIFORM2DPROC is access procedure (location : GLint;
												 x : GLdouble;
												 y : GLdouble);

	type PFNGLUNIFORM3DPROC is access procedure (location : GLint;
												 x : GLdouble;
												 y : GLdouble;
												 z : GLdouble);

	type PFNGLUNIFORM4DPROC is access procedure (location : GLint;
												 x : GLdouble;
												 y : GLdouble;
												 z : GLdouble;
												 w : GLdouble);

	type PFNGLUNIFORM1DVPROC is access procedure (location : GLint;
												  count : GLsizei;
												  value : access constant GLdouble);

	type PFNGLUNIFORM2DVPROC is access procedure (location : GLint;
												  count : GLsizei;
												  value : access constant GLdouble);

	type PFNGLUNIFORM3DVPROC is access procedure (location : GLint;
												  count : GLsizei;
												  value : access constant GLdouble);

	type PFNGLUNIFORM4DVPROC is access procedure (location : GLint;
												  count : GLsizei;
												  value : access constant GLdouble);

	type PFNGLUNIFORMMATRIX2DVPROC is access procedure (location : GLint;
														count : GLsizei;
														transpose : GLboolean;
														value : access constant GLdouble);

	type PFNGLUNIFORMMATRIX3DVPROC is access procedure (location : GLint;
														count : GLsizei;
														transpose : GLboolean;
														value : access constant GLdouble);

	type PFNGLUNIFORMMATRIX4DVPROC is access procedure (location : GLint;
														count : GLsizei;
														transpose : GLboolean;
														value : access constant GLdouble);

	type PFNGLUNIFORMMATRIX2X3DVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLdouble);

	type PFNGLUNIFORMMATRIX2X4DVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLdouble);

	type PFNGLUNIFORMMATRIX3X2DVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLdouble);

	type PFNGLUNIFORMMATRIX3X4DVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLdouble);

	type PFNGLUNIFORMMATRIX4X2DVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLdouble);

	type PFNGLUNIFORMMATRIX4X3DVPROC is access procedure (location  :        GLint;
														  count     :        GLsizei;
														  transpose :        GLboolean;
														  value     : access constant GLdouble);

	type PFNGLGETUNIFORMDVPROC is access procedure (program : GLuint;
													location : GLint;
													params : access GLdouble);

	type PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC is access function (program     :        GLuint;
																   shadertypee :        GLenum;
																   name        : access constant GLchar)
																   return GLint;

	type PFNGLGETSUBROUTINEINDEXPROC is access function (program     :        GLuint;
														 shadertypee :        GLenum;
														 name        : access constant GLchar)
														 return GLuint;

	type PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC is access procedure (program     :        GLuint;
																	shadertypee :        GLenum;
																	index       :        GLuint;
																	pname       :        GLenum;
																	values      : access GLint);

	type PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC is access procedure (program     :        GLuint;
																	  shadertypee :        GLenum;
																	  index       :        GLuint;
																	  bufsize     :        GLsizei;
																	  length      : access GLsizei;
																	  name        : access GLchar);

	type PFNGLGETACTIVESUBROUTINENAMEPROC is access procedure (program     :        GLuint;
															   shadertypee :        GLenum;
															   index       :        GLuint;
															   bufsize     :        GLsizei;
															   length      : access GLsizei;
															   name        : access GLchar);

	type PFNGLUNIFORMSUBROUTINESUIVPROC is access procedure (shadertypee :        GLenum;
															 count       :        GLsizei;
															 indices     : access constant GLuint);

	type PFNGLGETUNIFORMSUBROUTINEUIVPROC is access procedure (shadertypee :        GLenum;
															   location    :        GLint;
															   params      : access GLuint);

	type PFNGLGETPROGRAMSTAGEIVPROC is access procedure (program     :        GLuint;
														 shadertypee :        GLenum;
														 pname       :        GLenum;
														 values      : access GLint);
	
	type PFNGLPATCHPARAMETERIPROC is access procedure (pname : GLenum;
													   value : GLint);

	type PFNGLPATCHPARAMETERFVPROC is access procedure (pname  :        GLenum;
														values : access constant GLfloat);

	type PFNGLBINDTRANSFORMFEEDBACKPROC is access procedure (target : GLenum;
															 id     : GLuint);

	type PFNGLDELETETRANSFORMFEEDBACKSPROC is access procedure (n   :        GLsizei;
																ids : access constant GLuint);

	type PFNGLGENTRANSFORMFEEDBACKSPROC is access procedure (n   : GLsizei;
															 ids : access GLuint);

	type PFNGLISTRANSFORMFEEDBACKPROC is access function (id : GLuint)
														  return GLboolean;

	type PFNGLPAUSETRANSFORMFEEDBACKPROC is access procedure;

	type PFNGLRESUMETRANSFORMFEEDBACKPROC is access procedure;

	type PFNGLDRAWTRANSFORMFEEDBACKPROC is access procedure (mode : GLenum;
															 id   : GLuint);

	type PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC is access procedure (mode   : GLenum;
																   id     : GLuint;
																   stream : GLuint);

	type PFNGLBEGINQUERYINDEXEDPROC is access procedure (target : GLenum;
														 index  : GLuint;
														 id     : GLuint);

	type PFNGLENDQUERYINDEXEDPROC is access procedure (target : GLenum;
													   index  : GLuint);

	type PFNGLGETQUERYINDEXEDIVPROC is access procedure (target :        GLenum;
														 index  :        GLuint;
														 pname  :        GLenum;
														 params : access GLint);

	pragma convention (Stdcall, PFNGLMINSAMPLESHADINGPROC);
	pragma convention (Stdcall, PFNGLBLENDEQUATIONIPROC);
	pragma convention (Stdcall, PFNGLBLENDEQUATIONSEPARATEIPROC);
	pragma convention (Stdcall, PFNGLBLENDFUNCIPROC);
	pragma convention (Stdcall, PFNGLBLENDFUNCSEPARATEIPROC);
	pragma convention (Stdcall, PFNGLDRAWARRAYSINDIRECTPROC);
	pragma convention (Stdcall, PFNGLDRAWELEMENTSINDIRECTPROC);
	pragma convention (Stdcall, PFNGLUNIFORM1DPROC);
	pragma convention (Stdcall, PFNGLUNIFORM2DPROC);
	pragma convention (Stdcall, PFNGLUNIFORM3DPROC);
	pragma convention (Stdcall, PFNGLUNIFORM4DPROC);
	pragma convention (Stdcall, PFNGLUNIFORM1DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORM2DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORM3DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORM4DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX2DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX3DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX4DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX2X3DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX2X4DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX3X2DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX3X4DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX4X2DVPROC);
	pragma convention (Stdcall, PFNGLUNIFORMMATRIX4X3DVPROC);
	pragma convention (Stdcall, PFNGLGETUNIFORMDVPROC);
	pragma convention (Stdcall, PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC);
	pragma convention (Stdcall, PFNGLGETSUBROUTINEINDEXPROC);
	pragma convention (Stdcall, PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC);
	pragma convention (Stdcall, PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC);
	pragma convention (Stdcall, PFNGLGETACTIVESUBROUTINENAMEPROC);
	pragma convention (Stdcall, PFNGLUNIFORMSUBROUTINESUIVPROC);
	pragma convention (Stdcall, PFNGLGETUNIFORMSUBROUTINEUIVPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMSTAGEIVPROC);
	pragma convention (Stdcall, PFNGLPATCHPARAMETERIPROC);
	pragma convention (Stdcall, PFNGLPATCHPARAMETERFVPROC);
	pragma convention (Stdcall, PFNGLBINDTRANSFORMFEEDBACKPROC);
	pragma convention (Stdcall, PFNGLDELETETRANSFORMFEEDBACKSPROC);
	pragma convention (Stdcall, PFNGLGENTRANSFORMFEEDBACKSPROC);
	pragma convention (Stdcall, PFNGLISTRANSFORMFEEDBACKPROC);
	pragma convention (Stdcall, PFNGLPAUSETRANSFORMFEEDBACKPROC);
	pragma convention (Stdcall, PFNGLRESUMETRANSFORMFEEDBACKPROC);
	pragma convention (Stdcall, PFNGLDRAWTRANSFORMFEEDBACKPROC);
	pragma convention (Stdcall, PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC);
	pragma convention (Stdcall, PFNGLBEGINQUERYINDEXEDPROC);
	pragma convention (Stdcall, PFNGLENDQUERYINDEXEDPROC);
	pragma convention (Stdcall, PFNGLGETQUERYINDEXEDIVPROC);

	glMinSampleShading       : PFNGLMINSAMPLESHADINGPROC;
	glBlendEquationi         : PFNGLBLENDEQUATIONIPROC;
	glBlendEquationSeparatei : PFNGLBLENDEQUATIONSEPARATEIPROC;
	glBlendFunci             : PFNGLBLENDFUNCIPROC;
	glBlendFuncSeparatei     : PFNGLBLENDFUNCSEPARATEIPROC;
	glDrawArraysIndirect   : PFNGLDRAWARRAYSINDIRECTPROC;
	glDrawElementsIndirect : PFNGLDRAWELEMENTSINDIRECTPROC;
	glUniform1d          : PFNGLUNIFORM1DPROC;
	glUniform2d          : PFNGLUNIFORM2DPROC;
	glUniform3d          : PFNGLUNIFORM3DPROC;
	glUniform4d          : PFNGLUNIFORM4DPROC;
	glUniform1dv         : PFNGLUNIFORM1DVPROC;
	glUniform2dv         : PFNGLUNIFORM2DVPROC;
	glUniform3dv         : PFNGLUNIFORM3DVPROC;
	glUniform4dv         : PFNGLUNIFORM4DVPROC;
	glUniformMatrix2dv   : PFNGLUNIFORMMATRIX2DVPROC;
	glUniformMatrix3dv   : PFNGLUNIFORMMATRIX2DVPROC;
	glUniformMatrix4dv   : PFNGLUNIFORMMATRIX2DVPROC;
	glUniformMatrix2x3dv : PFNGLUNIFORMMATRIX2X3DVPROC;
	glUniformMatrix2x4dv : PFNGLUNIFORMMATRIX2X4DVPROC;
	glUniformMatrix3x2dv : PFNGLUNIFORMMATRIX3X2DVPROC;
	glUniformMatrix3x4dv : PFNGLUNIFORMMATRIX3X4DVPROC;
	glUniformMatrix4x2dv : PFNGLUNIFORMMATRIX4X2DVPROC;
	glUniformMatrix4x3dv : PFNGLUNIFORMMATRIX4X3DVPROC;
	glGetUniformdv       : PFNGLGETUNIFORMDVPROC;
	glGetSubroutineUniformLocation   : PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC;
	glGetSubroutineIndex             : PFNGLGETSUBROUTINEINDEXPROC;
	glGetActiveSubroutineUniformiv   : PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC;
	glGetActiveSubroutineUniformName : PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC;
	glGetActiveSubroutineName        : PFNGLGETACTIVESUBROUTINENAMEPROC;
	glUniformSubroutinesuiv          : PFNGLUNIFORMSUBROUTINESUIVPROC;
	glGetUniformSubroutineuiv        : PFNGLGETUNIFORMSUBROUTINEUIVPROC;
	glGetProgramStageiv              : PFNGLGETPROGRAMSTAGEIVPROC;
	glPatchParameteri   : PFNGLPATCHPARAMETERIPROC;
	glPatchParameterfv  : PFNGLPATCHPARAMETERFVPROC;
	glBindTransformFeedback    : PFNGLBINDTRANSFORMFEEDBACKPROC;
	glDeleteTransformFeedbacks : PFNGLDELETETRANSFORMFEEDBACKSPROC;
	glGenTransformFeedbacks    : PFNGLGENTRANSFORMFEEDBACKSPROC;
	glIsTransformFeedback      : PFNGLISTRANSFORMFEEDBACKPROC;
	glPauseTransformFeedback   : PFNGLPAUSETRANSFORMFEEDBACKPROC;
	glResumeTransformFeedback  : PFNGLRESUMETRANSFORMFEEDBACKPROC;
	glDrawTransformFeedback    : PFNGLDRAWTRANSFORMFEEDBACKPROC;
	glDrawTransformFeedbackStream : PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC;
	glBeginQueryIndexed           : PFNGLBEGINQUERYINDEXEDPROC;
	glEndQueryIndexed             : PFNGLENDQUERYINDEXEDPROC;
	glGetQueryIndexediv           : PFNGLGETQUERYINDEXEDIVPROC;

	----------------
	-- OpenGL 4.1 --
	----------------
	type PFNGLRELEASESHADERCOMPILERPROC is access procedure;

	type PFNGLSHADERBINARYPROC is access procedure (count : GLsizei;
													shaders : access constant GLuint;
													binaryformat : GLenum;
													binary : GLvoid;
													length : GLsizei);

	type PFNGLGETSHADERPRECISIONFORMATPROC is access procedure (shadertypee : GLenum;
																precisiontype : GLenum;
																rangee : access GLint;
																precision : access GLint);

	type PFNGLDEPTHRANGEFPROC is access procedure (n : GLfloat;
												   f : GLfloat);

	type PFNGLCLEARDEPTHFPROC is access procedure (d : GLfloat);

	type PFNGLGETPROGRAMBINARYPROC is access procedure (program : GLuint;
														bufSize : GLsizei;
														length : access GLsizei;
														binaryFormat : access GLenum;
														binary : GLvoid);

	type PFNGLPROGRAMBINARYPROC is access procedure (program : GLuint;
													 binaryFormat : GLenum;
													 binary : GLvoid;
													 length : GLsizei);

	type PFNGLPROGRAMPARAMETERIPROC is access procedure (program : GLuint;
														 pname : GLenum;
														 value : GLint);

		type PFNGLUSEPROGRAMSTAGESPROC is access procedure (pipeline : GLuint;
														stages : GLbitfield;
														program : GLuint);

	type PFNGLACTIVESHADERPROGRAMPROC is access procedure (pipeline : GLuint;
														   program : GLuint);

	type PFNGLCREATESHADERPROGRAMVPROC is access function (typee : GLenum;
														   count : GLsizei;
														   strings : System.Address)
														   return GLuint;



	type PFNGLBINDPROGRAMPIPELINEPROC is access procedure (pipeline : GLuint);

	type PFNGLDELETEPROGRAMPIPELINESPROC is access procedure (n : GLsizei;
															  pipelines : access constant GLuint);

	type PFNGLGENPROGRAMPIPELINESPROC is access procedure (n : GLsizei;
														   pipelines : access constant GLuint);

	type PFNGLISPROGRAMPIPELINEPROC is access function (pipeline : GLuint)
														return GLboolean;

	type PFNGLGETPROGRAMPIPELINEIVPROC is access procedure (pipeline : GLuint;
															pname : GLenum;
															params : access GLint);

	type PFNGLPROGRAMUNIFORM1IPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLint);

	type PFNGLPROGRAMUNIFORM1IVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLint);

	type PFNGLPROGRAMUNIFORM1FPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLfloat);

	type PFNGLPROGRAMUNIFORM1FVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORM1DPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLdouble);

	type PFNGLPROGRAMUNIFORM1DVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORM1UIPROC is access procedure (program : GLuint;
														 location : GLint;
														 v0 : GLuint);

	type PFNGLPROGRAMUNIFORM1UIVPROC is access procedure (program : GLuint;
														  location : GLint;
														  count : GLsizei;
														  value : access constant GLuint);

	type PFNGLPROGRAMUNIFORM2IPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLint;
														v1 : GLint);

	type PFNGLPROGRAMUNIFORM2IVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLint);

	type PFNGLPROGRAMUNIFORM2FPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLfloat;
														v1 : GLfloat);

	type PFNGLPROGRAMUNIFORM2FVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORM2DPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLdouble;
														v1 : GLdouble);

	type PFNGLPROGRAMUNIFORM2DVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORM2UIPROC is access procedure (program : GLuint;
														 location : GLint;
														 v0 : GLuint;
														 v1 : GLuint);

	type PFNGLPROGRAMUNIFORM2UIVPROC is access procedure (program : GLuint;
														  location : GLint;
														  count : GLsizei;
														  value : access constant GLuint);

	type PFNGLPROGRAMUNIFORM3IPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLint;
														v1 : GLint;
														v2 : GLint);

	type PFNGLPROGRAMUNIFORM3IVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLint);

	type PFNGLPROGRAMUNIFORM3FPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLfloat;
														v1 : GLfloat;
														v2 : GLfloat);

	type PFNGLPROGRAMUNIFORM3FVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORM3DPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLdouble;
														v1 : GLdouble;
														v2 : GLdouble);

	type PFNGLPROGRAMUNIFORM3DVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORM3UIPROC is access procedure (program : GLuint;
														 location : GLint;
														 v0 : GLuint;
														 v1 : GLuint;
														 v2 : GLuint);

	type PFNGLPROGRAMUNIFORM3UIVPROC is access procedure (program : GLuint;
														  location : GLint;
														  count : GLsizei;
														  value : access constant GLuint);

	type PFNGLPROGRAMUNIFORM4IPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLint;
														v1 : GLint;
														v2 : GLint;
														v3 : GLint);

	type PFNGLPROGRAMUNIFORM4IVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLint);

	type PFNGLPROGRAMUNIFORM4FPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLfloat;
														v1 : GLfloat;
														v2 : GLfloat;
														v3 : GLfloat);

	type PFNGLPROGRAMUNIFORM4FVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORM4DPROC is access procedure (program : GLuint;
														location : GLint;
														v0 : GLdouble;
														v1 : GLdouble;
														v2 : GLdouble;
														v3 : GLdouble);

	type PFNGLPROGRAMUNIFORM4DVPROC is access procedure (program : GLuint;
														 location : GLint;
														 count : GLsizei;
														 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORM4UIPROC is access procedure (program : GLuint;
														 location : GLint;
														 v0 : GLuint;
														 v1 : GLuint;
														 v2 : GLuint;
														 v3 : GLuint);

	type PFNGLPROGRAMUNIFORM4UIVPROC is access procedure (program : GLuint;
														  location : GLint;
														  count : GLsizei;
														  value : access constant GLuint);

	type PFNGLPROGRAMUNIFORMMATRIX2FVPROC is access procedure (program : GLuint;
															   location : GLint;
															   count : GLsizei;
															   transpose : GLboolean;
															   value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX3FVPROC is access procedure (program : GLuint;
															   location : GLint;
															   count : GLsizei;
															   transpose : GLboolean;
															   value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX4FVPROC is access procedure (program : GLuint;
															   location : GLint;
															   count : GLsizei;
															   transpose : GLboolean;
															   value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX2DVPROC is access procedure (program : GLuint;
															   location : GLint;
															   count : GLsizei;
															   transpose : GLboolean;
															   value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORMMATRIX3DVPROC is access procedure (program : GLuint;
															   location : GLint;
															   count : GLsizei;
															   transpose : GLboolean;
															   value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORMMATRIX4DVPROC is access procedure (program : GLuint;
															   location : GLint;
															   count : GLsizei;
															   transpose : GLboolean;
															   value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLfloat);

	type PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLdouble);

	type PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC is access procedure (program : GLuint;
																 location : GLint;
																 count : GLsizei;
																 transpose : GLboolean;
																 value : access constant GLdouble);

	type PFNGLVALIDATEPROGRAMPIPELINEPROC is access procedure (pipeline : GLuint);

	type PFNGLGETPROGRAMPIPELINEINFOLOGPROC is access procedure (pipeline : GLuint;
																 bufSize : GLsizei;
																 length : access GLsizei;
																 infoLog : access GLchar);

	type PFNGLVERTEXATTRIBL1DPROC is access procedure (index : GLuint;
														x : GLdouble);

	type PFNGLVERTEXATTRIBL2DPROC is access procedure (index : GLuint;
														x : GLdouble;
														y : GLdouble);

	type PFNGLVERTEXATTRIBL3DPROC is access procedure (index : GLuint;
														x : GLdouble;
														y : GLdouble;
														z : GLdouble);

	type PFNGLVERTEXATTRIBL4DPROC is access procedure (index : GLuint;
														x : GLdouble;
														y : GLdouble;
														z : GLdouble;
														w : GLdouble);

	type PFNGLVERTEXATTRIBL1DVPROC is access procedure (index : GLuint;
														 v : access constant GLdouble);

	type PFNGLVERTEXATTRIBL2DVPROC is access procedure (index : GLuint;
														 v : access constant GLdouble);

	type PFNGLVERTEXATTRIBL3DVPROC is access procedure (index : GLuint;
														 v : access constant GLdouble);

	type PFNGLVERTEXATTRIBL4DVPROC is access procedure (index : GLuint;
														 v : access constant GLdouble);

	type PFNGLVERTEXATTRIBLPOINTERPROC is access procedure (index : GLuint;
															 size : GLint;
															 typee : GLenum;
															 stride : GLsizei;
															 pointer : GLvoid);

	type PFNGLGETVERTEXATTRIBLDVPROC is access procedure (index : GLuint;
														   pname : GLenum;
														   params : access GLdouble);

	type PFNGLVIEWPORTARRAYVPROC is access procedure (first : GLuint;
													  count : GLsizei;
													  v : access constant GLfloat);

	type PFNGLVIEWPORTINDEXEDFPROC is access procedure (index : GLuint;
														x : GLfloat;
														y : GLfloat;
														w : GLfloat;
														h : GLfloat);

	type PFNGLVIEWPORTINDEXEDFVPROC is access procedure (index : GLuint;
														 v : access constant GLfloat);

	type PFNGLSCISSORARRAYVPROC is access procedure (first : GLuint;
													 count : GLsizei;
													 v : access constant Glint);

	type PFNGLSCISSORINDEXEDPROC is access procedure (index : GLuint;
													  left : GLint;
													  bottom : GLint;
													  width : GLsizei;
													  height : GLsizei);

	type PFNGLSCISSORINDEXEDVPROC is access procedure (index : GLuint;
													   v : access constant GLint);

	type PFNGLDEPTHRANGEARRAYVPROC is access procedure (first : GLuint;
														count : GLsizei;
														v : access constant GLdouble);

	type PFNGLDEPTHRANGEINDEXEDPROC is access procedure (index : GLuint;
														 n : GLdouble;
														 f : GLdouble);

	type PFNGLGETFLOATI_VPROC is access procedure (target : GLenum;
												   index : GLuint;
												   data : access GLfloat);

	type PFNGLGETDOUBLEI_VPROC is access procedure (target : GLenum;
													index : GLuint;
													data : access GLfloat);

	pragma convention (Stdcall, PFNGLRELEASESHADERCOMPILERPROC);
	pragma convention (Stdcall, PFNGLSHADERBINARYPROC);
	pragma convention (Stdcall, PFNGLGETSHADERPRECISIONFORMATPROC);
	pragma convention (Stdcall, PFNGLDEPTHRANGEFPROC);
	pragma convention (Stdcall, PFNGLCLEARDEPTHFPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMBINARYPROC);
	pragma convention (Stdcall, PFNGLPROGRAMBINARYPROC);
	pragma convention (Stdcall, PFNGLPROGRAMPARAMETERIPROC);
	pragma convention (Stdcall, PFNGLUSEPROGRAMSTAGESPROC);
	pragma convention (Stdcall, PFNGLACTIVESHADERPROGRAMPROC);
	pragma convention (Stdcall, PFNGLCREATESHADERPROGRAMVPROC);
	pragma convention (Stdcall, PFNGLBINDPROGRAMPIPELINEPROC);
	pragma convention (Stdcall, PFNGLDELETEPROGRAMPIPELINESPROC);
	pragma convention (Stdcall, PFNGLGENPROGRAMPIPELINESPROC);
	pragma convention (Stdcall, PFNGLISPROGRAMPIPELINEPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMPIPELINEIVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM1IPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM1IVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM1FPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM1FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM1DPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM1DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM1UIPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM1UIVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM2IPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM2IVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM2FPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM2FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM2DPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM2DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM2UIPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM2UIVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM3IPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM3IVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM3FPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM3FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM3DPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM3DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM3UIPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM3UIVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM4IPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM4IVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM4FPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM4FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM4DPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM4DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM4UIPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORM4UIVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX2FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX3FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX4FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX2DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX3DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX4DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC);
	pragma convention (Stdcall, PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC);
	pragma convention (Stdcall, PFNGLVALIDATEPROGRAMPIPELINEPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMPIPELINEINFOLOGPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBL1DPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBL2DPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBL3DPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBL4DPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBL1DVPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBL2DVPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBL3DVPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBL4DVPROC);
	pragma convention (Stdcall,  PFNGLVERTEXATTRIBLPOINTERPROC);
	pragma convention (Stdcall,  PFNGLGETVERTEXATTRIBLDVPROC);
	pragma convention (Stdcall, PFNGLVIEWPORTARRAYVPROC);
	pragma convention (Stdcall, PFNGLVIEWPORTINDEXEDFPROC);
	pragma convention (Stdcall, PFNGLVIEWPORTINDEXEDFVPROC);
	pragma convention (Stdcall, PFNGLSCISSORARRAYVPROC);
	pragma convention (Stdcall, PFNGLSCISSORINDEXEDPROC);
	pragma convention (Stdcall, PFNGLSCISSORINDEXEDVPROC);
	pragma convention (Stdcall, PFNGLDEPTHRANGEARRAYVPROC);
	pragma convention (Stdcall, PFNGLDEPTHRANGEINDEXEDPROC);
	pragma convention (Stdcall, PFNGLGETFLOATI_VPROC);
	pragma convention (Stdcall, PFNGLGETDOUBLEI_VPROC);

	glReleaseShaderCompiler    : PFNGLRELEASESHADERCOMPILERPROC;
	glShaderBinary             : PFNGLSHADERBINARYPROC;
	glGetShaderPrecisionFormat : PFNGLGETSHADERPRECISIONFORMATPROC;
	glDepthRangef              : PFNGLDEPTHRANGEFPROC;
	glClearDepthf              : PFNGLCLEARDEPTHFPROC;
	glGetProgramBinary  : PFNGLGETPROGRAMBINARYPROC;
	glProgramBinary     : PFNGLPROGRAMBINARYPROC;
	glProgramParameteri : PFNGLPROGRAMPARAMETERIPROC;
	glUseProgramStages          : PFNGLUSEPROGRAMSTAGESPROC;
	glActiveShaderProgram       : PFNGLACTIVESHADERPROGRAMPROC;
	glCreateShaderProgramv      : PFNGLCREATESHADERPROGRAMVPROC;
	glBindProgramPipeline       : PFNGLBINDPROGRAMPIPELINEPROC;
	glDeleteProgramPipelines    : PFNGLDELETEPROGRAMPIPELINESPROC;
	glGenProgramPipelines       : PFNGLGENPROGRAMPIPELINESPROC;
	glIsProgramPipeline         : PFNGLISPROGRAMPIPELINEPROC;
	glGetProgramPipelineiv      : PFNGLGETPROGRAMPIPELINEIVPROC;
	glProgramUniform1i          : PFNGLPROGRAMUNIFORM1IPROC;
	glProgramUniform1iv         : PFNGLPROGRAMUNIFORM1IVPROC;
	glProgramUniform1f          : PFNGLPROGRAMUNIFORM1FPROC;
	glProgramUniform1fv         : PFNGLPROGRAMUNIFORM1FVPROC;
	glProgramUniform1d          : PFNGLPROGRAMUNIFORM1DPROC;
	glProgramUniform1dv         : PFNGLPROGRAMUNIFORM1DVPROC;
	glProgramUniform1ui         : PFNGLPROGRAMUNIFORM1UIPROC;
	glProgramUniform1uiv        : PFNGLPROGRAMUNIFORM1UIVPROC;
	glProgramUniform2i          : PFNGLPROGRAMUNIFORM2IPROC;
	glProgramUniform2iv         : PFNGLPROGRAMUNIFORM2IVPROC;
	glProgramUniform2f          : PFNGLPROGRAMUNIFORM2FPROC;
	glProgramUniform2fv         : PFNGLPROGRAMUNIFORM2FVPROC;
	glProgramUniform2d          : PFNGLPROGRAMUNIFORM2DPROC;
	glProgramUniform2dv         : PFNGLPROGRAMUNIFORM2DVPROC;
	glProgramUniform2ui         : PFNGLPROGRAMUNIFORM2UIPROC;
	glProgramUniform2uiv        : PFNGLPROGRAMUNIFORM2UIVPROC;
	glProgramUniform3i          : PFNGLPROGRAMUNIFORM3IPROC;
	glProgramUniform3iv         : PFNGLPROGRAMUNIFORM3IVPROC;
	glProgramUniform3f          : PFNGLPROGRAMUNIFORM3FPROC;
	glProgramUniform3fv         : PFNGLPROGRAMUNIFORM3FVPROC;
	glProgramUniform3d          : PFNGLPROGRAMUNIFORM3DPROC;
	glProgramUniform3dv         : PFNGLPROGRAMUNIFORM3DVPROC;
	glProgramUniform3ui         : PFNGLPROGRAMUNIFORM3UIPROC;
	glProgramUniform3uiv        : PFNGLPROGRAMUNIFORM3UIVPROC;
	glProgramUniform4i          : PFNGLPROGRAMUNIFORM4IPROC;
	glProgramUniform4iv         : PFNGLPROGRAMUNIFORM4IVPROC;
	glProgramUniform4f          : PFNGLPROGRAMUNIFORM4FPROC;
	glProgramUniform4fv         : PFNGLPROGRAMUNIFORM4FVPROC;
	glProgramUniform4d          : PFNGLPROGRAMUNIFORM4DPROC;
	glProgramUniform4dv         : PFNGLPROGRAMUNIFORM4DVPROC;
	glProgramUniform4ui         : PFNGLPROGRAMUNIFORM4UIPROC;
	glProgramUniform4uiv        : PFNGLPROGRAMUNIFORM4UIVPROC;
	glProgramUniformMatrix2fv   : PFNGLPROGRAMUNIFORMMATRIX2FVPROC;
	glProgramUniformMatrix3fv   : PFNGLPROGRAMUNIFORMMATRIX3FVPROC;
	glProgramUniformMatrix4fv   : PFNGLPROGRAMUNIFORMMATRIX4FVPROC;
	glProgramUniformMatrix2dv   : PFNGLPROGRAMUNIFORMMATRIX2DVPROC;
	glProgramUniformMatrix3dv   : PFNGLPROGRAMUNIFORMMATRIX3DVPROC;
	glProgramUniformMatrix4dv   : PFNGLPROGRAMUNIFORMMATRIX4DVPROC;
	glProgramUniformMatrix2x3fv : PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC;
	glProgramUniformMatrix3x2fv : PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC;
	glProgramUniformMatrix2x4fv : PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC;
	glProgramUniformMatrix4x2fv : PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC;
	glProgramUniformMatrix3x4fv : PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC;
	glProgramUniformMatrix4x3fv : PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC;
	glProgramUniformMatrix2x3dv : PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC;
	glProgramUniformMatrix3x2dv : PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC;
	glProgramUniformMatrix2x4dv : PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC;
	glProgramUniformMatrix4x2dv : PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC;
	glProgramUniformMatrix3x4dv : PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC;
	glProgramUniformMatrix4x3dv : PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC;
	glValidateProgramPipeline   : PFNGLVALIDATEPROGRAMPIPELINEPROC;
	glGetProgramPipelineInfoLog : PFNGLGETPROGRAMPIPELINEINFOLOGPROC;
	glVertexAttribL1d      : PFNGLVERTEXATTRIBL1DPROC;
	glVertexAttribL2d      : PFNGLVERTEXATTRIBL2DPROC;
	glVertexAttribL3d      : PFNGLVERTEXATTRIBL3DPROC;
	glVertexAttribL4d      : PFNGLVERTEXATTRIBL4DPROC;
	glVertexAttribL1dv     : PFNGLVERTEXATTRIBL1DVPROC;
	glVertexAttribL2dv     : PFNGLVERTEXATTRIBL2DVPROC;
	glVertexAttribL3dv     : PFNGLVERTEXATTRIBL3DVPROC;
	glVertexAttribL4dv     : PFNGLVERTEXATTRIBL4DVPROC;
	glVertexAttribLPointer : PFNGLVERTEXATTRIBLPOINTERPROC;
	glGetVertexAttribLdv   : PFNGLGETVERTEXATTRIBLDVPROC;
	glViewportArrayv    : PFNGLVIEWPORTARRAYVPROC;
	glViewportIndexedf  : PFNGLVIEWPORTINDEXEDFPROC;
	glViewportIndexedfv : PFNGLVIEWPORTINDEXEDFVPROC;
	glScissorArrayv     : PFNGLSCISSORARRAYVPROC;
	glScissorIndexed    : PFNGLSCISSORINDEXEDPROC;
	glScissorIndexedv   : PFNGLSCISSORINDEXEDVPROC;
	glDepthRangeArrayv  : PFNGLDEPTHRANGEARRAYVPROC;
	glDepthRangeIndexed : PFNGLDEPTHRANGEINDEXEDPROC;
	glGetFloati_v       : PFNGLGETFLOATI_VPROC;
	glGetDoublei_v      : PFNGLGETDOUBLEI_VPROC;

	----------------
	-- OpenGL 4.2 --
	----------------
	type PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC is access procedure (mode : Glenum;
																	   first : GLint;
																	   count : GLsizei;
																	   primcount : GLsizei;
																	   baseinstance : GLuint);

	type PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC is access procedure (mode : Glenum;
																		 count : GLsizei;
																		 typee : GLenum;
																		 indices : GLvoid;
																		 primcount : GLsizei;
																		 baseinstance : GLuint);

	type PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC is access procedure (mode : Glenum;
																				   count : GLsizei;
																				   typee : GLenum;
																				   indices : GLvoid;
																				   primcount : GLsizei;
																				   basevertex : GLint;
																				   baseinstance : GLuint);
	
	type PFNGLGETINTERNALFORMATIVPROC is access procedure (target : GLenum;
														   internalformat : GLenum;
														   pname : GLenum;
														   bufSize : GLsizei;
														   params : access GLint);

	type PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC is access procedure (program : GLuint;
																	  bufferIndex : GLuint;
																	  pname : GLenum;
																	  params : access GLint);

	type PFNGLBINDIMAGETEXTUREPROC is access procedure (unit : GLuint;
														texture : GLuint;
														level : GLint;
														layered : GLboolean;
														layer : GLint;
														accesss : GLenum;
														format : GLenum);

	type PFNGLMEMORYBARRIERPROC is access procedure (barriers : GLbitfield);
	
	type PFNGLTEXSTORAGE1DPROC is access procedure (target : GLenum;
													levels : GLsizei;
													internalformat : GLenum;
													width : GLsizei);

	type PFNGLTEXSTORAGE2DPROC is access procedure (target : GLenum;
													levels : GLsizei;
													internalformat : GLenum;
													width : GLsizei;
													height : GLsizei);

	type PFNGLTEXSTORAGE3DPROC is access procedure (target : GLenum;
													levels : GLsizei;
													internalformat : GLenum;
													width : GLsizei;
													height : GLsizei;
													depth : GLsizei);

	type PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC is access procedure (mode : GLenum;
																	  id : GLuint;
																	  primcount : GLsizei);

	type PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC is access procedure (mode : GLenum;
																			id : GLuint;
																			steam : GLuint;
																			primcount : GLsizei);


	pragma convention (Stdcall, PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC);
	pragma convention (Stdcall, PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC);
	pragma convention (Stdcall, PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC);
	pragma convention (Stdcall, PFNGLGETINTERNALFORMATIVPROC);
	pragma convention (Stdcall, PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC);
	pragma convention (Stdcall, PFNGLBINDIMAGETEXTUREPROC);
	pragma convention (Stdcall, PFNGLMEMORYBARRIERPROC);
	pragma convention (Stdcall, PFNGLTEXSTORAGE1DPROC);
	pragma convention (Stdcall, PFNGLTEXSTORAGE2DPROC);
	pragma convention (Stdcall, PFNGLTEXSTORAGE3DPROC);
	pragma convention (Stdcall, PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC);
	pragma convention (Stdcall, PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC);

	glDrawArraysInstancedBaseInstance             : PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC;
	glDrawElementsInstancedBaseInstance           : PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC;
	glDrawElementsInstancedBaseVertexBaseInstance : PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC;
	glGetInternalformativ : PFNGLGETINTERNALFORMATIVPROC;
	glGetActiveAtomicCounterBufferiv : PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC;
	glBindImageTexture : PFNGLBINDIMAGETEXTUREPROC;
	glMemoryBarrier    : PFNGLMEMORYBARRIERPROC;
	glTexStorage1D        : PFNGLTEXSTORAGE1DPROC;
	glTexStorage2D        : PFNGLTEXSTORAGE2DPROC;
	glTexStorage3D        : PFNGLTEXSTORAGE3DPROC;
	glDrawTransformFeedbackInstanced        : PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC;
	glDrawTransformFeedbackStreamInstanced  : PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC;
	
	----------------
	-- OpenGL 4.3 --
	----------------
	type PFNGLCLEARBUFFERDATAPROC is access procedure (target : GLenum;
														  internalformat : GLenum;
														  format : GLenum;
														  typee : GLenum;
														  data : System.Address);

	type PFNGLCLEARBUFFERSUBDATAPROC is access procedure (target : GLenum;
														  internalformat : GLenum;
														  offset : GLintptr;
														  size : GLsizeiptr;
														  format : GLenum;
														  typee : GLenum;
														  data : System.Address);

	type PFNGLDISPATCHCOMPUTEPROC is access procedure (num_groups_x : GLuint;
													   num_groups_y : GLuint;
													   num_groups_z : GLuint);

	type PFNGLDISPATCHCOMPUTEINDIRECTPROC is access procedure (indirect : GLintptr);

	type PFNGLCOPYIMAGESUBDATAPROC is access procedure (srcName : GLuint;
														srcTarget : GLenum;
														srcLevel : GLint;
														srcX : GLint;
														srcY : GLint;
														srcZ : GLint;
														dstName : GLuint;
														dstTarget : GLenum;
														dstLevel : GLint;
														dstX : GLint;
														dstY : GLint;
														dstZ : GLint;
														srcWidth : GLsizei;
														srcHeight : GLsizei;
														srcDepth : GLsizei);
	
	type PFNGLFRAMEBUFFERPARAMETERIPROC is access procedure (target : GLenum;
															 pname : GLenum;
															 param : GLint);

	type PFNGLGETFRAMEBUFFERPARAMETERIVPROC is access procedure (target : GLenum;
																 pname : GLenum;
																 params : access GLint);
	
	type PFNGLGETINTERNALFORMATI64VPROC is access procedure (target : GLenum;
															 pname : GLenum;
															 bufSize : GLsizei;
															 params : access GLint64);
	
	type PFNGLINVALIDATETEXSUBIMAGEPROC is access procedure (texture : GLuint;
															 level : GLint;
															 xoffset : GLint;
															 yoffset : GLint;
															 zoffset : GLint;
															 width : GLsizei;
															 height : GLsizei;
															 depth : GLsizei);

	type PFNGLINVALIDATETEXIMAGEPROC is access procedure (texture : GLuint;
														  level : GLint);

	type PFNGLINVALIDATEBUFFERSUBDATAPROC is access procedure (buffer : GLuint;
															   offset : GLintptr;
															   length : GLsizeiptr);

	type PFNGLINVALIDATEBUFFERDATAPROC is access procedure (buffer : GLuint);

	type PFNGLINVALIDATEFRAMEBUFFERPROC is access procedure (target : GLenum;
															 numAttachments : GLsizei;
															 attachments : access constant GLenum);

	type PFNGLINVALIDATESUBFRAMEBUFFERPROC is access procedure (target : GLenum;
																numAttachments : GLsizei;
																attachments : access constant GLenum;
																x : GLint;
																y : GLint;
																width : GLsizei;
																height : GLsizei);
	
	type PFNGLMULTIDRAWARRAYSINDIRECTPROC is access procedure (mode : GLenum;
															   indirect : access System.Address;
															   drawcount : GLsizei;
															   stride : GLsizei);

	type PFNGLMULTIDRAWELEMENTSINDIRECTPROC is access procedure (mode : GLenum;
																 typee : GLenum;
																 indirect : access System.Address;
																 drawcount : GLsizei;
																 stride : GLsizei);

	type PFNGLGETPROGRAMINTERFACEIVPROC is access procedure (program : GLuint;
															 programInterface : GLenum;
															 pname : GLenum;
															 params : access GLint);

	type PFNGLGETPROGRAMRESOURCEINDEXPROC is access procedure (program : GLuint;
															   programInterface : GLenum;
															   name : access constant GLchar);

	type PFNGLGETPROGRAMRESOURCENAMEPROC is access procedure (program : GLuint;
															  programInterface : GLenum;
															  index : GLuint;
															  bufSize : GLsizei;
															  length : access GLsizei;
															  name : access GLchar);

	type PFNGLGETPROGRAMRESOURCEIVPROC is access procedure (program : GLuint;
															programInterface : GLenum;
															index : GLuint;
															propCount : GLsizei;
															props : access constant GLenum;
															bufSize : GLsizei;
															length : access GLsizei;
															params : access GLint);

	type PFNGLGETPROGRAMRESOURCELOCATIONPROC is access procedure (program : GLuint;
																  programInterface : GLenum;
																  name : access constant GLchar);

	type PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC is access procedure (program : GLuint;
																	   programInterface : GLenum;
																	   name : access constant GLchar);
	
	type PFNGLSHADERSTORAGEBLOCKBINDINGPROC is access procedure (program : GLuint;
																 storageBlockIndex : GLuint;
																 storageBlockBinding : GLuint);
	
	type PFNGLTEXBUFFERRANGEPROC is access procedure (target : GLenum;
													  internalformat : GLenum;
													  buffer : GLuint;
													  offset : GLintptr;
													  size : GLsizeiptr);

	type PFNGLTEXSTORAGE2DMULTISAMPLEPROC is access procedure (target : GLenum;
															   samples : GLsizei;
															   internalformat : GLenum;
															   width : GLsizei;
															   height : GLsizei;
															   fixedsamplelocations : GLboolean);

	type PFNGLTEXSTORAGE3DMULTISAMPLEPROC is access procedure (target : GLenum;
															   samples : GLsizei;
															   internalformat : GLenum;
															   width : GLsizei;
															   height : GLsizei;
															   depth : GLsizei;
															   fixedsamplelocations : GLboolean);

	type PFNGLTEXTUREVIEWPROC is access procedure (texture : GLuint;
												   target : GLenum;
												   origtexture : GLuint;
												   internalformat : GLenum;
												   minlevel : GLuint;
												   numlevels : GLuint;
												   minlayer : GLuint;
												   numlayers : GLuint);

	type PFNGLBINDVERTEXBUFFERPROC is access procedure (bindingindex : GLuint;
														buffer : GLuint;
														offset : GLintptr;
														stride : GLsizei);

	type PFNGLVERTEXATTRIBFORMATPROC is access procedure (attribindex : GLuint;
														  size : GLint;
														  typee : GLenum;
														  normalized : GLboolean;
														  relativeoffset : GLuint);

	type PFNGLVERTEXATTRIBIFORMATPROC is access procedure (attribindex : GLuint;
														   size : GLint;
														   typee : GLenum;
														   relativeoffset : GLuint);

	type PFNGLVERTEXATTRIBLFORMATPROC is access procedure (attribindex : GLuint;
														   size : GLint;
														   typee : GLenum;
														   relativeoffset : GLuint);

	type PFNGLVERTEXATTRIBBINDINGPROC is access procedure (attribindex : GLuint;
														   bindingindex : GLuint);

	type PFNGLVERTEXBINDINGDIVISORPROC is access procedure (bindingindex : GLuint;
															divisor : GLuint);

	type PFNGLDEBUGMESSAGECONTROLPROC is access procedure (source : GLenum;
															  typee : GLenum;
															  severity : GLenum;
															  count : GLsizei;
															  ids : access constant GLuint;
															  enabled : GLboolean);

	type PFNGLDEBUGMESSAGEINSERTPROC is access procedure (source : GLenum;
															 typee : GLenum;
															 id : GLuint;
															 severity : GLenum;
															 length : GLsizei;
															 buf : access constant GLchar);

	type PFNGLDEBUGMESSAGECALLBACKPROC is access procedure (callback : GLDEBUGPROCARB;
															   userParam : GLvoid);

	type PFNGLGETDEBUGMESSAGELOGPROC is access function (count : GLuint;
															bufsize : GLsizei;
															sources : access GLenum;
															types : access  GLenum;
															ids : access GLuint;
															severities : access GLenum;
															lengths : access GLsizei;
															messageLog : access GLchar)
															return GLuint;

	type PFNGLPUSHDEBUGGROUPPROC is access procedure (source : GLenum;
													  id : GLuint;
													  length : GLsizei;
													  message : access constant GLchar);

	type PFNGLPOPDEBUGGROUPPROC is access procedure;

	type PFNGLOBJECTLABELPROC is access procedure (identifier : GLenum;
												   name : GLuint;
												   length : GLsizei;
												   label : access constant GLchar);

	type PFNGLGETOBJECTLABELPROC is access procedure (identifier : GLenum;
													  name : GLuint;
													  bufSize : GLsizei;
													  length : access GLsizei;
													  label : access GLchar);

	type PFNGLOBJECTPTRLABELPROC is access procedure (ptr : System.Address;
													  length : GLsizei;
													  label : access constant GLchar);

	type PFNGLGETOBJECTPTRLABELPROC is access procedure (ptr : System.Address;
														 bufSize : GLsizei;
														 length : access GLsizei;
														 laber : access GLchar);

	pragma convention (Stdcall, PFNGLCLEARBUFFERDATAPROC);
	pragma convention (Stdcall, PFNGLCLEARBUFFERSUBDATAPROC);
	pragma convention (Stdcall, PFNGLDISPATCHCOMPUTEPROC);
	pragma convention (Stdcall, PFNGLDISPATCHCOMPUTEINDIRECTPROC);
	pragma convention (Stdcall, PFNGLCOPYIMAGESUBDATAPROC);
	pragma convention (Stdcall, PFNGLFRAMEBUFFERPARAMETERIPROC);
	pragma convention (Stdcall, PFNGLGETFRAMEBUFFERPARAMETERIVPROC);
	pragma convention (Stdcall, PFNGLGETINTERNALFORMATI64VPROC);
	pragma convention (Stdcall, PFNGLINVALIDATETEXSUBIMAGEPROC);
	pragma convention (Stdcall, PFNGLINVALIDATETEXIMAGEPROC);
	pragma convention (Stdcall, PFNGLINVALIDATEBUFFERSUBDATAPROC);
	pragma convention (Stdcall, PFNGLINVALIDATEBUFFERDATAPROC);
	pragma convention (Stdcall, PFNGLINVALIDATEFRAMEBUFFERPROC);
	pragma convention (Stdcall, PFNGLINVALIDATESUBFRAMEBUFFERPROC);
	pragma convention (Stdcall, PFNGLMULTIDRAWARRAYSINDIRECTPROC);
	pragma convention (Stdcall, PFNGLMULTIDRAWELEMENTSINDIRECTPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMINTERFACEIVPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMRESOURCEINDEXPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMRESOURCENAMEPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMRESOURCEIVPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMRESOURCELOCATIONPROC);
	pragma convention (Stdcall, PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC);
	pragma convention (Stdcall, PFNGLSHADERSTORAGEBLOCKBINDINGPROC);
	pragma convention (Stdcall, PFNGLTEXBUFFERRANGEPROC);
	pragma convention (Stdcall, PFNGLTEXSTORAGE2DMULTISAMPLEPROC);
	pragma convention (Stdcall, PFNGLTEXSTORAGE3DMULTISAMPLEPROC);
	pragma convention (Stdcall, PFNGLTEXTUREVIEWPROC);
	pragma convention (Stdcall, PFNGLBINDVERTEXBUFFERPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBFORMATPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBIFORMATPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBLFORMATPROC);
	pragma convention (Stdcall, PFNGLVERTEXATTRIBBINDINGPROC);
	pragma convention (Stdcall, PFNGLVERTEXBINDINGDIVISORPROC);
	pragma convention (Stdcall, PFNGLDEBUGMESSAGECONTROLPROC);
	pragma convention (Stdcall, PFNGLDEBUGMESSAGEINSERTPROC);
	pragma convention (Stdcall, PFNGLDEBUGMESSAGECALLBACKPROC);
	pragma convention (Stdcall, PFNGLGETDEBUGMESSAGELOGPROC);
	pragma convention (Stdcall, PFNGLPUSHDEBUGGROUPPROC);
	pragma convention (Stdcall, PFNGLPOPDEBUGGROUPPROC);
	pragma convention (Stdcall, PFNGLOBJECTLABELPROC);
	pragma convention (Stdcall, PFNGLGETOBJECTLABELPROC);
	pragma convention (Stdcall, PFNGLOBJECTPTRLABELPROC);
	pragma convention (Stdcall, PFNGLGETOBJECTPTRLABELPROC);

	glClearBufferData         : PFNGLCLEARBUFFERDATAPROC;
	glClearBufferSubData      : PFNGLCLEARBUFFERSUBDATAPROC;
	glDispatchCompute         : PFNGLDISPATCHCOMPUTEPROC;
	glDispatchComputeIndirect : PFNGLDISPATCHCOMPUTEINDIRECTPROC;
	glCopyImageSubData : PFNGLCOPYIMAGESUBDATAPROC;
	glFramebufferParameteri    : PFNGLFRAMEBUFFERPARAMETERIPROC;
	glGetFramebufferParameteriv : PFNGLGETFRAMEBUFFERPARAMETERIVPROC;
	glGetInternalformati64v : PFNGLGETINTERNALFORMATI64VPROC;
	glInvalidateTexSubImage    : PFNGLINVALIDATETEXSUBIMAGEPROC;
	glInvalidateTexImage       : PFNGLINVALIDATETEXIMAGEPROC;
	glInvalidateBufferSubData  : PFNGLINVALIDATEBUFFERSUBDATAPROC;
	glInvalidateBufferData     : PFNGLINVALIDATEBUFFERDATAPROC;
	glInvalidateFramebuffer    : PFNGLINVALIDATEFRAMEBUFFERPROC;
	glInvalidateSubFramebuffer : PFNGLINVALIDATESUBFRAMEBUFFERPROC;
	glMultiDrawArraysIndirect   : PFNGLMULTIDRAWARRAYSINDIRECTPROC;
	glMultiDrawElementsIndirect : PFNGLMULTIDRAWELEMENTSINDIRECTPROC;
	glGetProgramInterfaceiv           : PFNGLGETPROGRAMINTERFACEIVPROC;
	glGetProgramResourceIndex         : PFNGLGETPROGRAMRESOURCEINDEXPROC;
	glGetProgramResourceName          : PFNGLGETPROGRAMRESOURCENAMEPROC;
	glGetProgramResourceiv            : PFNGLGETPROGRAMRESOURCEIVPROC;
	glGetProgramResourceLocation      : PFNGLGETPROGRAMRESOURCELOCATIONPROC;
	glGetProgramResourceLocationIndex : PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC;
	glShaderStorageBlockBinding : PFNGLSHADERSTORAGEBLOCKBINDINGPROC;
	glTexBufferRange        : PFNGLTEXBUFFERRANGEPROC;
	glTexStorage2DMultisample        : PFNGLTEXSTORAGE2DMULTISAMPLEPROC;
	glTexStorage3DMultisample        : PFNGLTEXSTORAGE3DMULTISAMPLEPROC;
	glTextureView : PFNGLTEXTUREVIEWPROC;
	glBindVertexBuffer                   : PFNGLBINDVERTEXBUFFERPROC;
	glVertexAttribFormat                 : PFNGLVERTEXATTRIBFORMATPROC;
	glVertexAttribIFormat                : PFNGLVERTEXATTRIBIFORMATPROC;
	glVertexAttribLFormat                : PFNGLVERTEXATTRIBLFORMATPROC;
	glVertexAttribBinding                : PFNGLVERTEXATTRIBBINDINGPROC;
	glVertexBindingDivisor               : PFNGLVERTEXBINDINGDIVISORPROC;
	glDebugMessageControl  : PFNGLDEBUGMESSAGECONTROLPROC;
	glDebugMessageInsert   : PFNGLDEBUGMESSAGEINSERTPROC;
	glDebugMessageCallback : PFNGLDEBUGMESSAGECALLBACKPROC;
	glGetDebugMessageLog   : PFNGLGETDEBUGMESSAGELOGPROC;
	glPushDebugGroup       : PFNGLPUSHDEBUGGROUPPROC;
	glPopDebugGroup        : PFNGLPOPDEBUGGROUPPROC;
	glObjectLabel          : PFNGLOBJECTLABELPROC;
	glGetObjectLabel       : PFNGLGETOBJECTLABELPROC;
	glObjectPtrLabel       : PFNGLOBJECTPTRLABELPROC;
	glGetObjectPtrLabel    : PFNGLGETOBJECTPTRLABELPROC;

	----------------
	-- OpenGL 4.4 --
	----------------
	type PFNGLBUFFERSTORAGEPROC is access procedure (target : GLenum;
	                                                 size : GLsizeiptr;
	                                                 data : GLvoid;
	                                                 flags : GLbitfield);

	type PFNGLCLEARTEXIMAGEPROC is access procedure (texture : GLuint;
	                                                 level : GLint;
	                                                 format : GLenum;
	                                                 typee : GLenum;
	                                                 data : GLvoid);

	type PFNGLCLEARTEXSUBIMAGEPROC is access procedure (texture : GLuint;
	                                                    level : GLint;
	                                                    xoffset : GLint;
	                                                    yoffset : GLint;
	                                                    zoffset : GLint;
	                                                    width : GLsizei;
	                                                    height : GLsizei;
	                                                    depth : GLsizei;
	                                                    format : GLenum;
	                                                    typee : GLenum;
	                                                    data : GLvoid);

	type PFNGLBINDBUFFERSBASEPROC is access procedure (target : GLenum;
	                                                   first : GLuint;
	                                                   count : GLsizei;
	                                                   buffers : access constant GLuint);

	type PFNGLBINDBUFFERSRANGEPROC is access procedure (target :  GLenum;
	                                                    first : GLuint;
	                                                    count : GLsizei;
	                                                    buffers : access constant GLuint;
	                                                    offsets : access constant GLintptr;
	                                                    sizes : access constant GLsizeiptr);

	type PFNGLBINDTEXTURESPROC is access procedure (first : GLuint;
	                                                count : GLsizei;
	                                                textures : access constant GLuint);

	type PFNGLBINDSAMPLERSPROC is access procedure (first : GLuint;
	                                                count : GLsizei;
	                                                samplers : access constant GLuint);

	type PFNGLBINDIMAGETEXTURESPROC is access procedure (first : GLuint;
	                                                     count : GLsizei;
	                                                     textures : access constant Gluint);

	type PFNGLBINDVERTEXBUFFERSPROC is access procedure (first : GLuint;
	                                                     count : GLsizei;
	                                                     buffers : access constant GLuint;
	                                                     offsets : access constant GLintptr;
	                                                     strides : access constant GLsizei);
	pragma convention (Stdcall, PFNGLBUFFERSTORAGEPROC);
	pragma convention (Stdcall, PFNGLCLEARTEXIMAGEPROC);
	pragma convention (Stdcall, PFNGLCLEARTEXSUBIMAGEPROC);
	pragma convention (Stdcall, PFNGLBINDBUFFERSBASEPROC);
	pragma convention (Stdcall, PFNGLBINDBUFFERSRANGEPROC);
	pragma convention (Stdcall, PFNGLBINDTEXTURESPROC);
	pragma convention (Stdcall, PFNGLBINDSAMPLERSPROC);
	pragma convention (Stdcall, PFNGLBINDIMAGETEXTURESPROC);
	pragma convention (Stdcall, PFNGLBINDVERTEXBUFFERSPROC);

	glBufferStorage : PFNGLBUFFERSTORAGEPROC; 
	glClearTexImage : PFNGLCLEARTEXIMAGEPROC;
	glClearTexSubImage : PFNGLCLEARTEXSUBIMAGEPROC;
	glBindBuffersBase : PFNGLBINDBUFFERSBASEPROC;
	glBindBuffersRange : PFNGLBINDBUFFERSRANGEPROC;
	glBindTextures : PFNGLBINDTEXTURESPROC;
	glBindSamplers : PFNGLBINDSAMPLERSPROC;
	glBindImageTextures : PFNGLBINDIMAGETEXTURESPROC;
	glBindVertexBuffers : PFNGLBINDVERTEXBUFFERSPROC;
	
	-------------------------------------
	-- GL_ARB_shading_language_include --
	-------------------------------------
	type PFNGLNAMEDSTRINGARBPROC is access procedure (typ       :        GLenum;
													  namelen   :        GLint;
													  name      : access constant GLchar;
													  stringlen :        GLint;
													  string    : access constant GLchar);

	type PFNGLDELETENAMEDSTRINGARBPROC is access procedure (namelen :        GLint;
															name    : access constant GLchar);

	type PFNGLCOMPILESHADERINCLUDEARBPROC is access procedure (shader :        GLuint;
															   count  :        GLsizei;
															   path   :        System.Address;
															   length : access constant GLint);

	type PFNGLISNAMEDSTRINGARBPROC is access function (namelen :        GLint;
													   name    : access constant GLchar)
													   return GLboolean;

	type PFNGLGETNAMEDSTRINGARBPROC is access procedure (namelen   :        GLint;
														 name      : access constant GLchar;
														 bufSize   :        GLsizei;
														 stringlen : access GLint;
														 string    : access GLchar);

	type PFNGLGETNAMEDSTRINGIVARBPROC is access procedure (namelen :        GLint;
														   name    : access constant GLchar;
														   pname   :        GLenum;
														   params  : access GLint);

	pragma convention (Stdcall, PFNGLNAMEDSTRINGARBPROC);
	pragma convention (Stdcall, PFNGLDELETENAMEDSTRINGARBPROC);
	pragma convention (Stdcall, PFNGLCOMPILESHADERINCLUDEARBPROC);
	pragma convention (Stdcall, PFNGLISNAMEDSTRINGARBPROC);
	pragma convention (Stdcall, PFNGLGETNAMEDSTRINGARBPROC);
	pragma convention (Stdcall, PFNGLGETNAMEDSTRINGIVARBPROC);

	glNamedStringARB          : PFNGLNAMEDSTRINGARBPROC;
	glDeleteNamedStringARB    : PFNGLDELETENAMEDSTRINGARBPROC;
	glCompileShaderIncludeARB : PFNGLCOMPILESHADERINCLUDEARBPROC;
	glIsNamedStringARB        : PFNGLISNAMEDSTRINGARBPROC;
	glGetNamedStringARB       : PFNGLGETNAMEDSTRINGARBPROC;
	glGetNamedStringivARB     : PFNGLGETNAMEDSTRINGIVARBPROC;

	---------------------------------------
	-- GL_ARB_vertex_type_2_10_10_10_rev --
	---------------------------------------
	type PFNGLVERTEXP2UIPROC is access procedure (typee     :        GLenum;
												  value     :        GLuint);

	type PFNGLVERTEXP2UIVPROC is access procedure (typee    :        GLenum;
												   value    : access constant GLuint);

	type PFNGLVERTEXP3UIPROC is access procedure (typee     :        GLenum;
												  value     :        GLuint);

	type PFNGLVERTEXP3UIVPROC is access procedure (typee    :        GLenum;
												   value    : access constant GLuint);

	type PFNGLVERTEXP4UIPROC is access procedure (typee     :        GLenum;
												  value     :        GLuint);

	type PFNGLVERTEXP4UIVPROC is access procedure (typee    :        GLenum;
												   value    : access constant GLuint);

	type PFNGLTEXCOORDP1UIPROC is access procedure (typee   :        GLenum;
													coords  :        GLuint);

	type PFNGLTEXCOORDP1UIVPROC is access procedure (typee  :        GLenum;
													 coords : access constant GLuint);

	type PFNGLTEXCOORDP2UIPROC is access procedure (typee   :        GLenum;
													coords  :        GLuint);

	type PFNGLTEXCOORDP2UIVPROC is access procedure (typee  :        GLenum;
													 coords : access constant GLuint);

	type PFNGLTEXCOORDP3UIPROC is access procedure (typee   :        GLenum;
													coords  :        GLuint);

	type PFNGLTEXCOORDP3UIVPROC is access procedure (typee  :        GLenum;
													 coords : access constant GLuint);

	type PFNGLTEXCOORDP4UIPROC is access procedure (typee   :        GLenum;
													coords  :        GLuint);

	type PFNGLTEXCOORDP4UIVPROC is access procedure (typee  :        GLenum;
													 coords : access constant GLuint);

	type PFNGLMULTITEXCOORDP1UIPROC is access procedure (texture  :        GLenum;
														 typee    :        GLenum;
														 coords   :        GLuint);

	type PFNGLMULTITEXCOORDP1UIVPROC is access procedure (texture :        GLenum;
														  typee   :        GLenum;
														  coords  : access constant GLuint);

	type PFNGLMULTITEXCOORDP2UIPROC is access procedure (texture  :        GLenum;
														 typee    :        GLenum;
														 coords   :        GLuint);

	type PFNGLMULTITEXCOORDP2UIVPROC is access procedure (texture :        GLenum;
														  typee   :        GLenum;
														  coords  : access constant GLuint);

	type PFNGLMULTITEXCOORDP3UIPROC is access procedure (texture  :        GLenum;
														 typee    :        GLenum;
														 coords   :        GLuint);

	type PFNGLMULTITEXCOORDP3UIVPROC is access procedure (texture :        GLenum;
														  typee   :        GLenum;
														  coords  : access constant GLuint);

	type PFNGLMULTITEXCOORDP4UIPROC is access procedure (texture  :        GLenum;
														 typee    :        GLenum;
														 coords   :        GLuint);

	type PFNGLMULTITEXCOORDP4UIVPROC is access procedure (texture :        GLenum;
														  typee   :        GLenum;
														  coords  : access constant GLuint);

	type PFNGLNORMALP3UIPROC is access procedure (typee    :        GLenum;
												  coords  :        GLuint);

	type PFNGLNORMALP3UIVPROC is access procedure (typee   :        GLenum;
												   coords  : access constant GLuint);

	type PFNGLCOLORP3UIPROC is access procedure (typee  :        GLenum;
												 color  : GLuint);

	type PFNGLCOLORP3UIVPROC is access procedure (typee :        GLenum;
												  color : access constant GLuint);

	type PFNGLCOLORP4UIPROC is access procedure (typee  :        GLenum; color: GLuint);

	type PFNGLCOLORP4UIVPROC is access procedure (typee :        GLenum;
												  color : access constant GLuint);

	type PFNGLSECONDARYCOLORP3UIPROC is access procedure (typee  :        GLenum;
														  color  :        GLuint);

	type PFNGLSECONDARYCOLORP3UIVPROC is access procedure (typee :        GLenum;
														   color : access constant GLuint);

	pragma convention (Stdcall, PFNGLVERTEXP2UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXP2UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXP3UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXP3UIVPROC);
	pragma convention (Stdcall, PFNGLVERTEXP4UIPROC);
	pragma convention (Stdcall, PFNGLVERTEXP4UIVPROC);
	pragma convention (Stdcall, PFNGLTEXCOORDP1UIPROC);
	pragma convention (Stdcall, PFNGLTEXCOORDP1UIVPROC);
	pragma convention (Stdcall, PFNGLTEXCOORDP2UIPROC);
	pragma convention (Stdcall, PFNGLTEXCOORDP2UIVPROC);
	pragma convention (Stdcall, PFNGLTEXCOORDP3UIPROC);
	pragma convention (Stdcall, PFNGLTEXCOORDP3UIVPROC);
	pragma convention (Stdcall, PFNGLTEXCOORDP4UIPROC);
	pragma convention (Stdcall, PFNGLTEXCOORDP4UIVPROC);
	pragma convention (Stdcall, PFNGLMULTITEXCOORDP1UIPROC);
	pragma convention (Stdcall, PFNGLMULTITEXCOORDP1UIVPROC);
	pragma convention (Stdcall, PFNGLMULTITEXCOORDP2UIPROC);
	pragma convention (Stdcall, PFNGLMULTITEXCOORDP2UIVPROC);
	pragma convention (Stdcall, PFNGLMULTITEXCOORDP3UIPROC);
	pragma convention (Stdcall, PFNGLMULTITEXCOORDP3UIVPROC);
	pragma convention (Stdcall, PFNGLMULTITEXCOORDP4UIPROC);
	pragma convention (Stdcall, PFNGLMULTITEXCOORDP4UIVPROC);
	pragma convention (Stdcall, PFNGLNORMALP3UIPROC);
	pragma convention (Stdcall, PFNGLNORMALP3UIVPROC);
	pragma convention (Stdcall, PFNGLCOLORP3UIPROC);
	pragma convention (Stdcall, PFNGLCOLORP3UIVPROC);
	pragma convention (Stdcall, PFNGLCOLORP4UIPROC);
	pragma convention (Stdcall, PFNGLCOLORP4UIVPROC);
	pragma convention (Stdcall, PFNGLSECONDARYCOLORP3UIPROC);
	pragma convention (Stdcall, PFNGLSECONDARYCOLORP3UIVPROC);

	glVertexP2ui          : PFNGLVERTEXP2UIPROC;
	glVertexP2uiv         : PFNGLVERTEXP2UIVPROC;
	glVertexP3ui          : PFNGLVERTEXP3UIPROC;
	glVertexP3uiv         : PFNGLVERTEXP3UIVPROC;
	glVertexP4ui          : PFNGLVERTEXP4UIPROC;
	glVertexP4uiv         : PFNGLVERTEXP4UIVPROC;
	glTexCoordP1ui        : PFNGLTEXCOORDP1UIPROC;
	glTexCoordP1uiv       : PFNGLTEXCOORDP1UIVPROC;
	glTexCoordP2ui        : PFNGLTEXCOORDP2UIPROC;
	glTexCoordP2uiv       : PFNGLTEXCOORDP2UIVPROC;
	glTexCoordP3ui        : PFNGLTEXCOORDP3UIPROC;
	glTexCoordP3uiv       : PFNGLTEXCOORDP3UIVPROC;
	glTexCoordP4ui        : PFNGLTEXCOORDP4UIPROC;
	glTexCoordP4uiv       : PFNGLTEXCOORDP4UIVPROC;
	glMultiTexCoordP1ui   : PFNGLMULTITEXCOORDP1UIPROC;
	glMultiTexCoordP1uiv  : PFNGLMULTITEXCOORDP1UIVPROC;
	glMultiTexCoordP2ui   : PFNGLMULTITEXCOORDP2UIPROC;
	glMultiTexCoordP2uiv  : PFNGLMULTITEXCOORDP2UIVPROC;
	glMultiTexCoordP3ui   : PFNGLMULTITEXCOORDP3UIPROC;
	glMultiTexCoordP3uiv  : PFNGLMULTITEXCOORDP3UIVPROC;
	glMultiTexCoordP4ui   : PFNGLMULTITEXCOORDP4UIPROC;
	glMultiTexCoordP4uiv  : PFNGLMULTITEXCOORDP4UIVPROC;
	glNormalP3ui          : PFNGLNORMALP3UIPROC;
	glNormalP3uiv         : PFNGLNORMALP3UIVPROC;
	glColorP3ui           : PFNGLCOLORP3UIPROC;
	glColorP3uiv          : PFNGLCOLORP3UIVPROC;
	glColorP4ui           : PFNGLCOLORP4UIPROC;
	glColorP4uiv          : PFNGLCOLORP4UIVPROC;
	glSecondaryColorP3ui  : PFNGLSECONDARYCOLORP3UIPROC;
	glSecondaryColorP3uiv : PFNGLSECONDARYCOLORP3UIVPROC;

	---------------------
	-- GL_ARB_cl_event --
	---------------------
	type PFNGLCREATESYNCFROMCLEVENTARBPROC is access function (context : System.Address;
															   event : System.Address;
															   flags : GLbitfield)
															   return GLsync;

	pragma convention (Stdcall, PFNGLCREATESYNCFROMCLEVENTARBPROC);

	glCreateSyncFromCLeventARB : PFNGLCREATESYNCFROMCLEVENTARBPROC;

	-----------------------
	-- GL_ARB_robustness --
	-----------------------
	type PFNGLGETGRAPHICSRESETSTATUSARBPROC is access function return GLenum;


	type PFNGLGETNMAPDVARBPROC is access procedure (target : GLenum;
													query : GLenum;
													bufSize : GLsizei;
													v : access GLdouble);

	type PFNGLGETNMAPFVARBPROC is access procedure (target : GLenum;
													query : GLenum;
													bufSize : GLsizei;
													v : access GLfloat);

	type PFNGLGETNMAPIVARBPROC is access procedure (target : GLenum;
													query : GLenum;
													bufSize : GLsizei;
													v : access Glint);

	type PFNGLGETNPIXELMAPFVARBPROC is access procedure (map : GLenum;
														 bufSize : GLsizei;
														 values : access GLfloat);

	type PFNGLGETNPIXELMAPUIVARBPROC is access procedure (map : GLenum;
														  bufSize : GLsizei;
														  values : access GLuint);

	type PFNGLGETNPIXELMAPUSVARBPROC is access procedure (map : GLenum;
														  bufSize : GLsizei;
														  values : access GLushort);

	type PFNGLGETNPOLYGONSTIPPLEARBPROC is access procedure (bufSize : GLsizei;
															 pattern : access GLubyte);

	type PFNGLGETNCOLORTABLEARBPROC is access procedure (target : GLenum;
														 format : GLenum;
														 typee : GLenum;
														 bufSize : GLsizei;
														 table : GLvoid);

	type PFNGLGETNCONVOLUTIONFILTERARBPROC is access procedure (target : GLenum;
																format : GLenum;
																typee : GLenum;
																bufSize : GLsizei;
																image : GLvoid);

	type PFNGLGETNSEPARABLEFILTERARBPROC is access procedure (target : GLenum;
															  format : GLenum;
															  typee : GLenum;
															  rowBufSize : GLsizei;
															  row : GLvoid;
															  columnBufSize : GLsizei;
															  column : GLvoid;
															  span : GLvoid);

	type PFNGLGETNHISTOGRAMARBPROC is access procedure (target : GLenum;
														reset : GLboolean ;
														format : GLenum;
														typee : GLenum;
														bufSize : GLsizei;
														values : GLvoid);

	type PFNGLGETNMINMAXARBPROC is access procedure (target : GLenum;
													 reset : GLboolean ;
													 format : GLenum;
													 typee : GLenum;
													 bufSize : GLsizei;
													 values : GLvoid);

	type PFNGLGETNTEXIMAGEARBPROC is access procedure (target : GLenum;
													   level : GLint;
													   format : GLenum;
													   typee : GLenum;
													   bufSize : GLsizei;
													   img : GLvoid);

	type PFNGLREADNPIXELSARBPROC is access procedure (x : GLint;
													  y : GLint;
													  width : GLsizei;
													  height : GLsizei;
													  format : GLenum;
													  typee : GLenum;
													  bufSize : GLsizei;
													  data : GLvoid);

	type PFNGLGETNCOMPRESSEDTEXIMAGEARBPROC is access procedure (target : GLenum;
																 lod : GLint;
																 bufSize : GLsizei;
																 img : GLvoid);

	type PFNGLGETNUNIFORMFVARBPROC is access procedure (program : GLuint;
														location : GLint;
														bufSize : GLsizei;
														params : access GLfloat);

	type PFNGLGETNUNIFORMIVARBPROC is access procedure (program : GLuint;
														location : GLint;
														bufSize : GLsizei;
														params : access GLint);

	type PFNGLGETNUNIFORMUIVARBPROC is access procedure (program : GLuint;
														 location : GLint;
														 bufSize : GLsizei;
														 params : access GLuint);

	type PFNGLGETNUNIFORMDVARBPROC is access procedure (program : GLuint;
														location : GLint;
														bufSize : GLsizei;
														params : access GLdouble);

	pragma convention (Stdcall, PFNGLGETGRAPHICSRESETSTATUSARBPROC);
	pragma convention (Stdcall, PFNGLGETNMAPDVARBPROC);
	pragma convention (Stdcall, PFNGLGETNMAPFVARBPROC);
	pragma convention (Stdcall, PFNGLGETNMAPIVARBPROC);
	pragma convention (Stdcall, PFNGLGETNPIXELMAPFVARBPROC);
	pragma convention (Stdcall, PFNGLGETNPIXELMAPUIVARBPROC);
	pragma convention (Stdcall, PFNGLGETNPIXELMAPUSVARBPROC);
	pragma convention (Stdcall, PFNGLGETNPOLYGONSTIPPLEARBPROC);
	pragma convention (Stdcall, PFNGLGETNCOLORTABLEARBPROC);
	pragma convention (Stdcall, PFNGLGETNCONVOLUTIONFILTERARBPROC);
	pragma convention (Stdcall, PFNGLGETNSEPARABLEFILTERARBPROC);
	pragma convention (Stdcall, PFNGLGETNHISTOGRAMARBPROC);
	pragma convention (Stdcall, PFNGLGETNMINMAXARBPROC);
	pragma convention (Stdcall, PFNGLGETNTEXIMAGEARBPROC);
	pragma convention (Stdcall, PFNGLREADNPIXELSARBPROC);
	pragma convention (Stdcall, PFNGLGETNCOMPRESSEDTEXIMAGEARBPROC);
	pragma convention (Stdcall, PFNGLGETNUNIFORMFVARBPROC);
	pragma convention (Stdcall, PFNGLGETNUNIFORMIVARBPROC);
	pragma convention (Stdcall, PFNGLGETNUNIFORMUIVARBPROC);
	pragma convention (Stdcall, PFNGLGETNUNIFORMDVARBPROC);

	glGetGraphicsResetStatusARB : PFNGLGETGRAPHICSRESETSTATUSARBPROC;
	glGetnMapdvARB              : PFNGLGETNMAPDVARBPROC;
	glGetnMapfvARB              : PFNGLGETNMAPFVARBPROC;
	glGetnMapivARB              : PFNGLGETNMAPIVARBPROC;
	glGetnPixelMapfvARB         : PFNGLGETNPIXELMAPFVARBPROC;
	glGetnPixelMapuivARB        : PFNGLGETNPIXELMAPUIVARBPROC;
	glGetnPixelMapusvARB        : PFNGLGETNPIXELMAPUSVARBPROC;
	glGetnPolygonStippleARB     : PFNGLGETNPOLYGONSTIPPLEARBPROC;
	glGetnColorTableARB         : PFNGLGETNCOLORTABLEARBPROC;
	glGetnConvolutionFilterARB  : PFNGLGETNCONVOLUTIONFILTERARBPROC;
	glGetnSeparableFilterARB    : PFNGLGETNSEPARABLEFILTERARBPROC;
	glGetnHistogramARB          : PFNGLGETNHISTOGRAMARBPROC;
	glGetnMinmaxARB             : PFNGLGETNMINMAXARBPROC;
	glGetnTexImageARB           : PFNGLGETNTEXIMAGEARBPROC;
	glReadnPixelsARB            : PFNGLREADNPIXELSARBPROC;
	glGetnCompressedTexImageARB : PFNGLGETNCOMPRESSEDTEXIMAGEARBPROC;
	glGetnUniformfvARB          : PFNGLGETNUNIFORMFVARBPROC;
	glGetnUniformivARB          : PFNGLGETNUNIFORMIVARBPROC;
	glGetnUniformuivARB         : PFNGLGETNUNIFORMUIVARBPROC;
	glGetnUniformdvARB          : PFNGLGETNUNIFORMDVARBPROC;	
private
	pragma Import (Stdcall, glCullFace,             "glCullFace");
	pragma Import (Stdcall, glFrontFace,            "glFrontFace");
	pragma Import (Stdcall, glHint,                 "glHint");
	pragma Import (Stdcall, glLineWidth,            "glLineWidth");
	pragma Import (Stdcall, glPointSize,            "glPointSize");
	pragma Import (Stdcall, glPolygonMode,          "glPolygonMode");
	pragma Import (Stdcall, glScissor,              "glScissor");
	pragma Import (Stdcall, glTexParameterf,        "glTexParameterf");
	pragma Import (Stdcall, glTexParameterfv,       "glTexParameterfv");
	pragma Import (Stdcall, glTexParameteri,        "glTexParameteri");
	pragma Import (Stdcall, glTexParameteriv,       "glTexParameteriv");
	pragma Import (Stdcall, glTexImage1D,           "glTexImage1D");
	pragma Import (Stdcall, glTexImage2D,           "glTexImage2D");
	pragma Import (Stdcall, glDrawBuffer,           "glDrawBuffer");
	pragma Import (Stdcall, glClear,                "glClear");
	pragma Import (Stdcall, glClearColor,           "glClearColor");
	pragma Import (Stdcall, glClearStencil,         "glClearStencil");
	pragma Import (Stdcall, glClearDepth,           "glClearDepth");
	pragma Import (Stdcall, glStencilMask,          "glStencilMask");
	pragma Import (Stdcall, glColorMask,            "glColorMask");
	pragma Import (Stdcall, glDepthMask,            "glDepthMask");
	pragma Import (Stdcall, glDisable,              "glDisable");
	pragma Import (Stdcall, glEnable,               "glEnable");
	pragma Import (Stdcall, glFinish,               "glFinish");
	pragma Import (Stdcall, glFlush,                "glFush");
	pragma Import (Stdcall, glBlendFunc,            "glBlendFunc");
	pragma Import (Stdcall, glLogicOp,              "glLogicOp");
	pragma Import (Stdcall, glStencilFunc,          "glStencilFunc");
	pragma Import (Stdcall, glStencilOp,            "glStencilOp");
	pragma Import (Stdcall, glDepthFunc,            "glDepthFunc");
	pragma Import (Stdcall, glPixelStoref,          "glPixelStoref");
	pragma Import (Stdcall, glPixelStorei,          "glPixelStorei");
	pragma Import (Stdcall, glReadBuffer,           "glReadBuffer");
	pragma Import (Stdcall, glReadPixels,           "glReadPixels");
	pragma Import (Stdcall, glGetBooleanv,          "glGetBooleanv");
	pragma Import (Stdcall, glGetDoublev,           "glGetDoublev");
	pragma Import (Stdcall, glGetError,             "glGetError");
	pragma Import (Stdcall, glGetFloatv,            "glGetFloatv");
	pragma Import (Stdcall, glGetIntegerv,          "glGetIntegerv");
	pragma Import (Stdcall, glGetString,            "glGetString");
	pragma Import (Stdcall, glGetTexImage,          "glGetTexImage");
	pragma Import (Stdcall, glGetTexParameterfv,    "glGetTexParameterfv");
	pragma Import (Stdcall, glGetTexParameteriv,    "glGetTexParameteriv");
	pragma Import (Stdcall, glGetTexLevelParameterfv, "glGetTexLevelParameterfv");
	pragma Import (Stdcall, glGetTexLevelParameteriv, "glGetTexLevelParameteriv");
	pragma Import (Stdcall, glIsEnabled,            "glIsEnabled");
	pragma Import (Stdcall, glDepthRange,           "glDepthRange");
	pragma Import (Stdcall, glViewport,             "glViewport");
	pragma Import (Stdcall, glDrawArrays,           "glDrawArrays");
	pragma Import (Stdcall, glDrawElements,         "glDrawElements");
	pragma Import (Stdcall, glGetPointerv,          "glGetPointerv");
	pragma Import (Stdcall, glPolygonOffset,        "glPolygonOffset");
	pragma Import (Stdcall, glCopyTexImage1D,       "glCopyTexImage1D");
	pragma Import (Stdcall, glCopyTexImage2D,       "glCopyTexImage2D");
	pragma Import (Stdcall, glCopyTexSubImage1D,    "glCopyTexSubImage1D");
	pragma Import (Stdcall, glCopyTexSubImage2D,    "glCopyTexSubImage2D");
	pragma Import (Stdcall, glTexSubImage1D,        "glTexSubImage1D");
	pragma Import (Stdcall, glTexSubImage2D,        "glTexSubImage2D");
	pragma Import (Stdcall, glBindTexture,          "glBindTexture");
	pragma Import (Stdcall, glDeleteTextures,       "glDeleteTextures");
	pragma Import (Stdcall, glGenTextures,          "glGenTextures");
	pragma Import (Stdcall, glIsTexture,            "glIsTexture");
end Sogla.OpenGL;
