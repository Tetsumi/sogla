------------------------------------------------------------------------------------------
-- Ada GLFW 2.7.7 bind
-- Tetsumi
-- 07 Feb. 2013
-- contact Tetsumi <tetsumi@vmail.me>
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
-- GLFW is licensed under an unmodified zlib/libpng license, which is an
-- OSI-certified, BSD-like license that allows static linking with closed
-- source software. It is reproduced in its entirety below.
--
-- Copyright © 2002-2006 Marcus Geelnard
-- Copyright © 2006-2011 Camilla Berglund
--
-- This software is provided 'as-is', without any express or implied warranty.
-- In no event will the authors be held liable for any damages arising from the
-- use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including
-- commercial applications, and to alter it and redistribute it freely, subject to
-- the following restrictions:
--
-- 1. The origin of this software must not be misrepresented; you must not claim that
-- you wrote the original software. If you use this software in a product, an
-- acknowledgment in the product documentation would be appreciated but is not required.
--
-- 2. Altered source versions must be plainly marked as such, and must not be
-- misrepresented as being the original software.
--
-- 3. This notice may not be removed or altered from any source distribution.
------------------------------------------------------------------------------------------

with
	Interfaces.C,
	Interfaces.C.Strings,
	Interfaces.C.Extensions,
	System;

package Sogla.Glfw is

	-- GLFW version
	GLFW_VERSION_MAJOR       : constant := 2;
	GLFW_VERSION_MINOR       : constant := 7;
	GLFW_VERSION_REVISION    : constant := 7;

	-- Key and button state/action definitions
	GLFW_RELEASE    : constant := 0;
	GLFW_PRESS      : constant := 1;

	-- Keyboard key definitions: 8-bit ISO-8859-1 (Latin 1) encoding is used
	-- for printable keys (such as A-Z, 0-9 etc), and values above 256
	-- represent special (non-printable) keys (e.g. F1, Page Up etc).
	GLFW_KEY_UNKNOWN      : constant := -1;
	GLFW_KEY_SPACE        : constant := 32;
	GLFW_KEY_SPECIAL      : constant := 256;
	GLFW_KEY_ESC          : constant := GLFW_KEY_SPECIAL+1;
	GLFW_KEY_F1           : constant := GLFW_KEY_SPECIAL+2;
	GLFW_KEY_F2           : constant := GLFW_KEY_SPECIAL+3;
	GLFW_KEY_F3           : constant := GLFW_KEY_SPECIAL+4;
	GLFW_KEY_F4           : constant := GLFW_KEY_SPECIAL+5;
	GLFW_KEY_F5           : constant := GLFW_KEY_SPECIAL+6;
	GLFW_KEY_F6           : constant := GLFW_KEY_SPECIAL+7;
	GLFW_KEY_F7           : constant := GLFW_KEY_SPECIAL+8;
	GLFW_KEY_F8           : constant := GLFW_KEY_SPECIAL+9;
	GLFW_KEY_F9           : constant := GLFW_KEY_SPECIAL+10;
	GLFW_KEY_F10          : constant := GLFW_KEY_SPECIAL+11;
	GLFW_KEY_F11          : constant := GLFW_KEY_SPECIAL+12;
	GLFW_KEY_F12          : constant := GLFW_KEY_SPECIAL+13;
	GLFW_KEY_F13          : constant := GLFW_KEY_SPECIAL+14;
	GLFW_KEY_F14          : constant := GLFW_KEY_SPECIAL+15;
	GLFW_KEY_F15          : constant := GLFW_KEY_SPECIAL+16;
	GLFW_KEY_F16          : constant := GLFW_KEY_SPECIAL+17;
	GLFW_KEY_F17          : constant := GLFW_KEY_SPECIAL+18;
	GLFW_KEY_F18          : constant := GLFW_KEY_SPECIAL+19;
	GLFW_KEY_F19          : constant := GLFW_KEY_SPECIAL+20;
	GLFW_KEY_F20          : constant := GLFW_KEY_SPECIAL+21;
	GLFW_KEY_F21          : constant := GLFW_KEY_SPECIAL+22;
	GLFW_KEY_F22          : constant := GLFW_KEY_SPECIAL+23;
	GLFW_KEY_F23          : constant := GLFW_KEY_SPECIAL+24;
	GLFW_KEY_F24          : constant := GLFW_KEY_SPECIAL+25;
	GLFW_KEY_F25          : constant := GLFW_KEY_SPECIAL+26;
	GLFW_KEY_UP           : constant := GLFW_KEY_SPECIAL+27;
	GLFW_KEY_DOWN         : constant := GLFW_KEY_SPECIAL+28;
	GLFW_KEY_LEFT         : constant := GLFW_KEY_SPECIAL+29;
	GLFW_KEY_RIGHT        : constant := GLFW_KEY_SPECIAL+30;
	GLFW_KEY_LSHIFT       : constant := GLFW_KEY_SPECIAL+31;
	GLFW_KEY_RSHIFT       : constant := GLFW_KEY_SPECIAL+32;
	GLFW_KEY_LCTRL        : constant := GLFW_KEY_SPECIAL+33;
	GLFW_KEY_RCTRL        : constant := GLFW_KEY_SPECIAL+34;
	GLFW_KEY_LALT         : constant := GLFW_KEY_SPECIAL+35;
	GLFW_KEY_RALT         : constant := GLFW_KEY_SPECIAL+36;
	GLFW_KEY_TAB          : constant := GLFW_KEY_SPECIAL+37;
	GLFW_KEY_ENTER        : constant := GLFW_KEY_SPECIAL+38;
	GLFW_KEY_BACKSPACE    : constant := GLFW_KEY_SPECIAL+39;
	GLFW_KEY_INSERT       : constant := GLFW_KEY_SPECIAL+40;
	GLFW_KEY_DEL          : constant := GLFW_KEY_SPECIAL+41;
	GLFW_KEY_PAGEUP       : constant := GLFW_KEY_SPECIAL+42;
	GLFW_KEY_PAGEDOWN     : constant := GLFW_KEY_SPECIAL+43;
	GLFW_KEY_HOME         : constant := GLFW_KEY_SPECIAL+44;
	GLFW_KEY_END          : constant := GLFW_KEY_SPECIAL+45;
	GLFW_KEY_KP_0         : constant := GLFW_KEY_SPECIAL+46;
	GLFW_KEY_KP_1         : constant := GLFW_KEY_SPECIAL+47;
	GLFW_KEY_KP_2         : constant := GLFW_KEY_SPECIAL+48;
	GLFW_KEY_KP_3         : constant := GLFW_KEY_SPECIAL+49;
	GLFW_KEY_KP_4         : constant := GLFW_KEY_SPECIAL+50;
	GLFW_KEY_KP_5         : constant := GLFW_KEY_SPECIAL+51;
	GLFW_KEY_KP_6         : constant := GLFW_KEY_SPECIAL+52;
	GLFW_KEY_KP_7         : constant := GLFW_KEY_SPECIAL+53;
	GLFW_KEY_KP_8         : constant := GLFW_KEY_SPECIAL+54;
	GLFW_KEY_KP_9         : constant := GLFW_KEY_SPECIAL+55;
	GLFW_KEY_KP_DIVIDE    : constant := GLFW_KEY_SPECIAL+56;
	GLFW_KEY_KP_MULTIPLY  : constant := GLFW_KEY_SPECIAL+57;
	GLFW_KEY_KP_SUBTRACT  : constant := GLFW_KEY_SPECIAL+58;
	GLFW_KEY_KP_ADD       : constant := GLFW_KEY_SPECIAL+59;
	GLFW_KEY_KP_DECIMAL   : constant := GLFW_KEY_SPECIAL+60;
	GLFW_KEY_KP_EQUAL     : constant := GLFW_KEY_SPECIAL+61;
	GLFW_KEY_KP_ENTER     : constant := GLFW_KEY_SPECIAL+62;
	GLFW_KEY_KP_NUM_LOCK  : constant := GLFW_KEY_SPECIAL+63;
	GLFW_KEY_CAPS_LOCK    : constant := GLFW_KEY_SPECIAL+64;
	GLFW_KEY_SCROLL_LOCK  : constant := GLFW_KEY_SPECIAL+65;
	GLFW_KEY_PAUSE        : constant := GLFW_KEY_SPECIAL+66;
	GLFW_KEY_LSUPER       : constant := GLFW_KEY_SPECIAL+67;
	GLFW_KEY_RSUPER       : constant := GLFW_KEY_SPECIAL+68;
	GLFW_KEY_MENU         : constant := GLFW_KEY_SPECIAL+69;
	GLFW_KEY_LAST         : constant := GLFW_KEY_MENU;

	-- Mouse button definitions
	GLFW_MOUSE_BUTTON_1      : constant := 0;
	GLFW_MOUSE_BUTTON_2      : constant := 1;
	GLFW_MOUSE_BUTTON_3      : constant := 2;
	GLFW_MOUSE_BUTTON_4      : constant := 3;
	GLFW_MOUSE_BUTTON_5      : constant := 4;
	GLFW_MOUSE_BUTTON_6      : constant := 5;
	GLFW_MOUSE_BUTTON_7      : constant := 6;
	GLFW_MOUSE_BUTTON_8      : constant := 7;
	GLFW_MOUSE_BUTTON_LAST   : constant := GLFW_MOUSE_BUTTON_8;

	-- Mouse button aliase
	GLFW_MOUSE_BUTTON_LEFT   : constant := GLFW_MOUSE_BUTTON_1;
	GLFW_MOUSE_BUTTON_RIGHT  : constant := GLFW_MOUSE_BUTTON_2;
	GLFW_MOUSE_BUTTON_MIDDLE : constant := GLFW_MOUSE_BUTTON_3;

	-- Joystick identifiers
	GLFW_JOYSTICK_1          : constant := 0;
	GLFW_JOYSTICK_2          : constant := 1;
	GLFW_JOYSTICK_3          : constant := 2;
	GLFW_JOYSTICK_4          : constant := 3;
	GLFW_JOYSTICK_5          : constant := 4;
	GLFW_JOYSTICK_6          : constant := 5;
	GLFW_JOYSTICK_7          : constant := 6;
	GLFW_JOYSTICK_8          : constant := 7;
	GLFW_JOYSTICK_9          : constant := 8;
	GLFW_JOYSTICK_10         : constant := 9;
	GLFW_JOYSTICK_11         : constant := 10;
	GLFW_JOYSTICK_12         : constant := 11;
	GLFW_JOYSTICK_13         : constant := 12;
	GLFW_JOYSTICK_14         : constant := 13;
	GLFW_JOYSTICK_15         : constant := 14;
	GLFW_JOYSTICK_16         : constant := 15;
	GLFW_JOYSTICK_LAST       : constant := GLFW_JOYSTICK_16;

	-- glfwOpenWindow modes
	GLFW_WINDOW               : constant := 16#00010001#;
	GLFW_FULLSCREEN           : constant := 16#00010002#;

	-- glfwGetWindowParam tokens
	GLFW_OPENED               : constant := 16#00020001#;
	GLFW_ACTIVE               : constant := 16#00020002#;
	GLFW_ICONIFIED            : constant := 16#00020003#;
	GLFW_ACCELERATED          : constant := 16#00020004#;
	GLFW_RED_BITS             : constant := 16#00020005#;
	GLFW_GREEN_BITS           : constant := 16#00020006#;
	GLFW_BLUE_BITS            : constant := 16#00020007#;
	GLFW_ALPHA_BITS           : constant := 16#00020008#;
	GLFW_DEPTH_BITS           : constant := 16#00020009#;
	GLFW_STENCIL_BITS         : constant := 16#0002000A#;

	-- The following constants are used for both glfwGetWindowParam
	-- and glfwOpenWindowHint
	GLFW_REFRESH_RATE         : constant := 16#0002000B#;
	GLFW_ACCUM_RED_BITS       : constant := 16#0002000C#;
	GLFW_ACCUM_GREEN_BITS     : constant := 16#0002000D#;
	GLFW_ACCUM_BLUE_BITS      : constant := 16#0002000E#;
	GLFW_ACCUM_ALPHA_BITS     : constant := 16#0002000F#;
	GLFW_AUX_BUFFERS          : constant := 16#00020010#;
	GLFW_STEREO               : constant := 16#00020011#;
	GLFW_WINDOW_NO_RESIZE     : constant := 16#00020012#;
	GLFW_FSAA_SAMPLES         : constant := 16#00020013#;
	GLFW_OPENGL_VERSION_MAJOR : constant := 16#00020014#;
	GLFW_OPENGL_VERSION_MINOR : constant := 16#00020015#;
	GLFW_OPENGL_FORWARD_COMPAT : constant := 16#00020016#;
	GLFW_OPENGL_DEBUG_CONTEXT : constant := 16#00020017#;
	GLFW_OPENGL_PROFILE       : constant := 16#00020018#;

	-- GLFW_OPENGL_PROFILE tokens
	GLFW_OPENGL_CORE_PROFILE  : constant := 16#00050001#;
	GLFW_OPENGL_COMPAT_PROFILE : constant := 16#00050002#;

	-- glfwEnable/glfwDisable tokens
	GLFW_MOUSE_CURSOR         : constant := 16#00030001#;
	GLFW_STICKY_KEYS          : constant := 16#00030002#;
	GLFW_STICKY_MOUSE_BUTTONS : constant := 16#00030003#;
	GLFW_SYSTEM_KEYS          : constant := 16#00030004#;
	GLFW_KEY_REPEAT           : constant := 16#00030005#;
	GLFW_AUTO_POLL_EVENTS     : constant := 16#00030006#;

	-- glfwWaitThread wait modes
	GLFW_WAIT                 : constant := 16#00040001#;
	GLFW_NOWAIT               : constant := 16#00040002#;

	-- glfwGetJoystickParam tokens
	GLFW_PRESENT              : constant := 16#00050001#;
	GLFW_AXES                 : constant := 16#00050002#;
	GLFW_BUTTONS              : constant := 16#00050003#;

	-- glfwReadImage/glfwLoadTexture2D flags
	GLFW_NO_RESCALE_BIT       : constant := 16#00000001#; --  Only for glfwReadImage
	GLFW_ORIGIN_UL_BIT        : constant := 16#00000002#;
	GLFW_BUILD_MIPMAPS_BIT    : constant := 16#00000004#; --  Only for glfwLoadTexture2D
	GLFW_ALPHA_MAP_BIT        : constant := 16#00000008#;

	-- Time spans longer than this (seconds) are considered to be infinity
	GLFW_INFINITY : constant :=  100000.0;

	-----------------------------------------------------------
	-- Typedefs
	-----------------------------------------------------------

	-- The video mode structure used by glfwGetVideoModes()
	type GLFWvidmode is
	   record
		   Width : Interfaces.C.int;
		   Height : Interfaces.C.int;
		   RedBits : Interfaces.C.int;
		   BlueBits : Interfaces.C.int;
		   GreenBits : Interfaces.C.int;
	   end record;

	pragma Convention (C, GLFWvidmode);

	-- Image/texture information
	type GLFWimage is
	   record
		   Width         : Interfaces.C.int;
		   Height        : Interfaces.C.int;
		   Format        : Interfaces.C.int;
		   BytesPerPixel : Interfaces.C.int;
		   Data          : Interfaces.C.Extensions.void_ptr;
	   end record;

	pragma Convention (C, GLFWimage);

	-- Thread ID
	subtype GLFWthread is Interfaces.C.int;

	-- Mutex object
	subtype GLFWmutex is Interfaces.C.Extensions.void_ptr;

	-- Condition variable object
	subtype GLFWcond is Interfaces.C.Extensions.void_ptr;

	-- Function pointer types
	type GLFWwindowsizefun is access procedure (width   : Interfaces.C.int;
												height  : Interfaces.C.int);
	type GLFWwindowclosefun is access procedure;

	type GLFWwindowrefreshfun is access procedure;

	type GLFWmousebuttonfun is access procedure (button : Interfaces.C.int;
												 action : Interfaces.C.int);

	type GLFWmouseposfun is access procedure (MousePosX : Interfaces.C.int;
											  MousePosY : Interfaces.C.int);

	type GLFWmousewheelfun is access procedure (WheelPos : Interfaces.C.int);

	type GLFWkeyfun is access procedure (key    : Interfaces.C.int;
										 action : Interfaces.C.int);

	type GLFWcharfun is access procedure (character : Interfaces.C.int;
										  action    : Interfaces.C.int);

	type GLFWthreadfun is access procedure (arg : System.Address);

	-----------------------------------------------------------
	-- Prototypes
	-----------------------------------------------------------

	-- GLFW initialization, termination and version querying
	function glfwInit return Interfaces.C.int;

	procedure glfwTerminate;

	procedure glfwGetVersion (major     : access Interfaces.C.int;
							  minor     : access Interfaces.C.int;
							  rev       : access Interfaces.C.int);

	-- Window handling
	function glfwOpenWindow (width          : Interfaces.C.int;
							 height         : Interfaces.C.int;
							 redbits        : Interfaces.C.int;
							 greenbits      : Interfaces.C.int;
							 bluebits       : Interfaces.C.int;
							 alphabits      : Interfaces.C.int;
							 depthbits      : Interfaces.C.int;
							 stencilbits    : Interfaces.C.int;
							 mode           : Interfaces.C.int)
							 return Interfaces.C.int;

	procedure glfwOpenWindowHint (target   : Interfaces.C.int;
								  hint      : Interfaces.C.int);

	procedure glfwCloseWindow;

	procedure glfwSetWindowTitle (title : Interfaces.C.Strings.chars_ptr);

	procedure glfwGetWindowSize (width  : access Interfaces.C.int;
								 height : access Interfaces.C.int);

	procedure glfwSetWindowSize (width  : Interfaces.C.int;
								 height : Interfaces.C.int);

	procedure glfwSetWindowPos (x : Interfaces.C.int;
								y : Interfaces.C.int);

	procedure glfwIconifyWindow;

	procedure glfwRestoreWindow;

	procedure glfwSwapBuffers;

	procedure glfwSwapInterval (interval : Interfaces.C.int);

	function glfwGetWindowParam (param : Interfaces.C.int)
								 return Interfaces.C.int;

	procedure glfwSetWindowSizeCallback (cbfun : GLFWwindowsizefun);

	procedure glfwSetWindowCloseCallback (cbfun : GLFWwindowclosefun);

	procedure glfwSetWindowRefreshCallback (cbfun : GLFWwindowrefreshfun);

	-- Video mode functions
	function glfwGetVideoModes (list        : GLFWvidmode;
								maxcount    : Interfaces.C.int)
								return Interfaces.C.int;

	procedure glfwGetDesktopMode (mode : GLFWvidmode);

	-- Input handling
	procedure glfwPollEvents;

	procedure glfwWaitEvents;

	function glfwGetKey (key : Interfaces.C.int)
						 return Interfaces.C.int;

	function glfwGetMouseButton (button : Interfaces.C.int)
								 return Interfaces.C.int;

	procedure glfwGetMousePos (ypos : access Interfaces.C.int;
							   xpos : access Interfaces.C.int);

	procedure glfwSetMousePos (xpos : Interfaces.C.int;
							   ypos : Interfaces.C.int);

	function glfwGetMouseWheel return Interfaces.C.int;

	procedure glfwSetMouseWheel (pos : Interfaces.C.int);

	procedure glfwSetKeyCallback (Cbfun : GLFWkeyfun);

	procedure glfwSetCharCallback (Cbfun : GLFWcharfun);

	procedure glfwSetMouseButtonCallback (Cbfun : GLFWmousebuttonfun);

	procedure glfwSetMousePosCallback (Cbfun : GLFWmouseposfun);

	procedure glfwSetMouseWheelCallback (Cbfun : GLFWmousewheelfun);

	-- Joystick input
	function glfwGetJoystickParam (joy      : Interfaces.C.int;
								   param    : Interfaces.C.int)
								   return Interfaces.C.int;

	function glfwGetJoystickPos (joy        : Interfaces.C.int;
								 pos        : access Interfaces.C.C_float;
								 numaxes    : Interfaces.C.int)
								 return Interfaces.C.int;

	function glfwGetJoystickButtons (joy        : Interfaces.C.int;
									 buttons    : access Interfaces.C.char;
									 numbuttons : Interfaces.C.int)
									 return Interfaces.C.int;

	-- Time
	function glfwGetTime return Interfaces.C.double;

	procedure glfwSetTime (time : Interfaces.C.double);

	procedure glfwSleep (time : Interfaces.C.double);

	-- Extension support
	function glfwExtensionSupported (extension : Interfaces.C.Strings.chars_ptr)
									 return Interfaces.C.int;

	function glfwGetProcAddress (procname : Interfaces.C.Strings.chars_ptr)
								 return System.Address;

	procedure glfwGetGLVersion (major   : access Interfaces.C.int;
								minor   : access Interfaces.C.int;
								rev     : access Interfaces.C.int);

	-- Threading support
	function glfwCreateThread (fun : GLFWthreadfun;
							   arg : System.Address)
							   return GLFWthread;

	procedure glfwDestroyThread (ID : GLFWthread);

	function glfwWaitThread (ID         : GLFWthread;
							 waitmode   : Interfaces.C.int)
							 return Interfaces.C.int;

	function glfwGetThreadID return GLFWthread;

	function glfwCreateMutex return GLFWmutex;

	procedure glfwDestroyMutex (mutex : GLFWmutex);

	procedure glfwLockMutex (mutex : GLFWmutex);

	procedure glfwUnlockMutex (mutex : GLFWmutex);

	function glfwCreateCond return GLFWcond;

	procedure glfwDestroyCond (cond : GLFWcond);

	procedure glfwWaitCond (cond    : GLFWcond;
							mutex   : GLFWmutex;
							timeout : Interfaces.C.double);

	procedure glfwSignalCond (cond : GLFWcond);

	procedure glfwBroadcastCond (cond : GLFWcond);

	function glfwGetNumberOfProcessors return Interfaces.C.int;

	-- Enable/disable functions
	procedure glfwEnable (token : Interfaces.C.int);

	procedure glfwDisable (token : Interfaces.C.int);

	-- Image/texture I/O support
	function glfwReadImage (name    : Interfaces.C.Strings.chars_ptr;
							img     : GLFWimage;
							flags   : Interfaces.C.int)
							return Interfaces.C.Int;

	function glfwReadMemoryImage (data  : Interfaces.C.Extensions.void_ptr;
								  size  : Interfaces.C.long;
								  img   : GLFWimage;
								  flags : Interfaces.C.int)
								  return Interfaces.C.int;

	procedure glfwFreeImage (img : GLFWimage);

	function glfwLoadTexture2D (name    : Interfaces.C.Strings.chars_ptr;
								flags   : Interfaces.C.int)
								return Interfaces.C.int;

	function glfwLoadMemoryTexture2D (data  : Interfaces.C.Extensions.void_ptr;
									  size  : Interfaces.C.long;
									  Flags : Interfaces.C.Int)
									  return Interfaces.C.int;

	function glfwLoadTextureImage2D (img    : GLFWimage;
									 flags  : Interfaces.C.int)
									 return Interfaces.C.int;

private
	pragma Convention (C, GLFWwindowsizefun);
	pragma Convention (C, GLFWwindowclosefun);
	pragma Convention (C, GLFWwindowrefreshfun);
	pragma Convention (C, GLFWmousebuttonfun);
	pragma Convention (C, GLFWmouseposfun);
	pragma Convention (C, GLFWmousewheelfun);
	pragma Convention (C, GLFWkeyfun);
	pragma Convention (C, GLFWcharfun);
	pragma Convention (C, GLFWthreadfun);

	pragma Import (C, glfwInit,                     "glfwInit");
	pragma Import (C, glfwTerminate,                "glfwTerminate");
	pragma Import (C, glfwGetVersion,               "glfwGetVersion");
	pragma Import (C, glfwOpenWindow,               "glfwOpenWindow");
	pragma Import (C, glfwOpenWindowHint,           "glfwOpenWindowHint");
	pragma Import (C, glfwCloseWindow,              "glfwCloseWindow");
	pragma Import (C, glfwSetWindowTitle,           "glfwSetWindowTitle");
	pragma Import (C, glfwGetWindowSize,            "glfwGetWindowSize");
	pragma Import (C, glfwSetWindowSize,            "glfwSetWindowSize");
	pragma Import (C, glfwSetWindowPos,             "glfwSetWindowPos");
	pragma Import (C, glfwIconifyWindow,            "glfwIconifyWindow");
	pragma Import (C, glfwRestoreWindow,            "glfwRestoreWindow");
	pragma Import (C, glfwSwapBuffers,              "glfwSwapBuffers");
	pragma Import (C, glfwSwapInterval,             "glfwSwapInterval");
	pragma Import (C, glfwGetWindowParam,           "glfwGetWindowParam");
	pragma Import (C, glfwSetWindowSizeCallback,    "glfwSetWindowSizeCallback");
	pragma Import (C, glfwSetWindowCloseCallback,   "glfwSetWindowCloseCallback");
	pragma Import (C, glfwSetWindowRefreshCallback, "glfwSetWindowRefreshCallback");
	pragma Import (C, glfwGetVideoModes,            "glfwGetVideoModes");
	pragma Import (C, glfwGetDesktopMode,           "glfwGetDesktopMode");
	pragma Import (C, glfwPollEvents,               "glfwPollEvents");
	pragma Import (C, glfwWaitEvents,               "glfwWaitEvents");
	pragma Import (C, glfwGetKey,                   "glfwGetKey");
	pragma Import (C, glfwGetMouseButton,           "glfwGetMouseButton");
	pragma Import (C, glfwGetMousePos,              "glfwGetMousePos");
	pragma Import (C, glfwSetMousePos,              "glfwSetMousePos");
	pragma Import (C, glfwGetMouseWheel,            "glfwGetMouseWheel");
	pragma Import (C, glfwSetMouseWheel,            "glfwSetMouseWheel");
	pragma Import (C, glfwSetKeyCallback,           "glfwSetKeyCallback");
	pragma Import (C, glfwSetCharCallback,          "glfwSetCharCallback");
	pragma Import (C, glfwSetMouseButtonCallback,   "glfwSetMouseButtonCallback");
	pragma Import (C, glfwSetMousePosCallback,      "glfwSetMousePosCallback");
	pragma Import (C, glfwSetMouseWheelCallback,    "glfwSetMouseWheelCallback");
	pragma Import (C, glfwGetJoystickParam,         "glfwGetJoystickParam");
	pragma Import (C, glfwGetJoystickPos,           "glfwGetJoystickPos");
	pragma Import (C, glfwGetJoystickButtons,       "glfwGetJoystickButtons");
	pragma Import (C, glfwGetTime,                  "glfwGetTime");
	pragma Import (C, glfwSetTime,                  "glfwSetTime");
	pragma Import (C, glfwSleep,                    "glfwSleep");
	pragma Import (C, glfwExtensionSupported,       "glfwExtensionSupported");
	pragma Import (C, glfwGetProcAddress,           "glfwGetProcAddress");
	pragma Import (C, glfwGetGLVersion,             "glfwGetGLVersion");
	pragma Import (C, glfwCreateThread,             "glfwCreateThread");
	pragma Import (C, glfwDestroyThread,            "glfwDestroyThread");
	pragma Import (C, glfwWaitThread,               "glfwWaitThread");
	pragma Import (C, glfwGetThreadID,              "glfwGetThreadID");
	pragma Import (C, glfwCreateMutex,              "glfwCreateMutex");
	pragma Import (C, glfwDestroyMutex,             "glfwDestroyMutex");
	pragma Import (C, glfwLockMutex,                "glfwLockMutex");
	pragma Import (C, glfwUnlockMutex,              "glfwUnlockMutex");
	pragma Import (C, glfwCreateCond,               "glfwCreateCond");
	pragma Import (C, glfwDestroyCond,              "glfwDestroyCond");
	pragma Import (C, glfwWaitCond,                 "glfwWaitCond");
	pragma Import (C, glfwSignalCond,               "glfwSignalCond");
	pragma Import (C, glfwBroadcastCond,            "glfwBroadcastCond");
	pragma Import (C, glfwGetNumberOfProcessors,    "glfwGetNumberOfProcessors");
	pragma Import (C, glfwEnable,                   "glfwEnable");
	pragma Import (C, glfwDisable,                  "glfwDisable");
	pragma Import (C, glfwReadImage,                "glfwReadImage");
	pragma Import (C, glfwReadMemoryImage,          "glfwReadMemoryImage");
	pragma Import (C, glfwFreeImage,                "glfwFreeImage");
	pragma Import (C, glfwLoadTexture2D,            "glfwLoadTexture2D");
	pragma Import (C, glfwLoadMemoryTexture2D,      "glfwLoadMemoryTexture2D");
	pragma Import (C, glfwLoadTextureImage2D,       "glfwLoadTextureImage2D");
end Sogla.Glfw;
