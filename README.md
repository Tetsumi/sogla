SOGLA 
=============

Description:
-------------
SOGLA is a simple OpenGL framework for Ada.  
SOGLA fully support OpenGL 4.x

Requirements:
-------------
- GNAT 4.7.x
- GLFW 2.7.X <http://www.glfw.org>

Licence:
---------------
>Licensed under the Apache License, Version 2.0 (the "License"); you may  
>not use this file except in compliance with the License. You may obtain  
>a copy of the License at
>  

><http://www.apache.org/licenses/LICENSE-2.0.html>  
><http://www.apache.org/licenses/LICENSE-2.0.txt>  

>Unless required by applicable law or agreed to in writing, software  
>distributed under the License is distributed on an "AS IS" BASIS, WITHOUT  
>WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the  
>License for the specific language governing permissions and limitations  
>under the License.  

Example:
---------------
    :::ada
    with 
        Ada.Text_IO,
        Interfaces,
        Interfaces.C,
        Interfaces.C.Strings,
        Sogla.OpenGL,
        Sogla.Glfw,
        Sogla.Log_IO;
    
    use 
        Ada.Text_IO,
        Interfaces,
        Interfaces.C,
        Sogla.OpenGL,
        Sogla.Glfw;
    
    procedure sogla_example is
    begin
        -- init GLFW
        if (glfwInit = 0) then
            Ada.Text_IO.Put_Line("glfwInit failed.");
            return;
        end if;
    
        -- init the log module
        Sogla.Log_IO.Initialize;
    
        -- open a window
        if (glfwOpenWindow(800, 600, 8, 8, 8, 8, 24, 8, GLFW_WINDOW) = 0) then
            Ada.Text_IO.Put_Line("glfwOpenWindow failed.");
            return;
        end if;
    
        -- init the opengl module (a window must be open)
        if (Sogla.OpenGL.Initialize) then
            Put_Line("Initialized!");
            New_Line;
        end if;
    
        -- set opengl clear color
        glClearColor(0.3, 0.3, 0.3, 1.0);
    
        -- main loop
        while (glfwGetWindowParam(GLFW_OPENED) = 1) loop
            glClear(GL_DEPTH_BUFFER_BIT or GL_COLOR_BUFFER_BIT);
            glfwSwapBuffers;
        end loop;
    
        -- finalize the log module
        Sogla.Log_IO.Finalize;
    
        -- finalize GLFW
        glfwTerminate;
    end opengl_gputime;