--
--
-- Print hello from a GLFW thread.
--
--
-- Author: Tetsumi <tetsumi@vmail.me>
--
--

with 
    Ada.Text_IO,
    Interfaces.C,
    Interfaces.C.Strings,
    Sogla.Glfw,
    Glfw_Test,
    System;

use 
    Interfaces,
    Interfaces.C,
    Sogla.Glfw;

procedure glfw_thread is
    Thread : GLFWthread;
    Is_Thread_Dead : C.int;
begin
    Ada.Text_IO.Put_Line("Sogla v" & Sogla.Version_String);
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("---- TEST: Print hello from a GLFW thread");

    if (glfwInit = 0) then
        Ada.Text_IO.Put_Line("glfwInit failed.");
    end if;

    if (glfwOpenWindow(800, 600, 8, 8, 8, 8, 24, 8, GLFW_WINDOW) = 0) then
        Ada.Text_IO.Put_Line("glfwOpenWindow failed.");
    end if;

    GlfwSetWindowTitle(C.Strings.New_String("Hello Ada."));
    Ada.Text_IO.Put_Line("-- Start thread. --");
    Thread := glfwCreateThread(Glfw_Test.Hello_Thread_Proc'Access, System.Null_Address);
    Is_Thread_Dead := glfwWaitThread(Thread, GLFW_WAIT);
    Ada.Text_IO.Put_Line("-- Thread is done. --");

    while (glfwGetWindowParam(GLFW_OPENED) = 1) loop
        glfwSwapBuffers;
    end loop;

    Ada.Text_IO.Put_Line("Done.");

    GlfwTerminate;
end glfw_thread;
