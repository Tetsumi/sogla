--
--
-- Ouput the internal GPU time.
--
-- The OpenGL extension GL_ARB_sync is needed.
--
-- Author: Tetsumi <tetsumi@vmail.me>
--
--

with 
    Ada.Text_IO,
    Interfaces,
    Interfaces.C,
    Interfaces.C.Strings,
    Sogla.OpenGL,
    Sogla.Glfw,
    Sogla.Log_IO;

use 
    Ada.Text_IO,
    Interfaces,
    Interfaces.C,
    Sogla.OpenGL,
    Sogla.Glfw;

procedure opengl_gputime is
    package Float_IO is new Ada.Text_IO.Float_IO(Float);

    Gpu_Time : aliased GLint64;
begin
    Ada.Text_IO.Put_Line("Sogla v" & Sogla.Version_String);
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("---- TEST: Ouput the internal GPU time");

    if (glfwInit = 0) then
        Ada.Text_IO.Put_Line("glfwInit failed.");
        return;
    end if;

    Sogla.Log_IO.Initialize;

    if (glfwOpenWindow(800, 600, 8, 8, 8, 8, 24, 8, GLFW_WINDOW) = 0) then
        Ada.Text_IO.Put_Line("glfwOpenWindow failed.");
        return;
    end if;

    glfwSetWindowTitle(C.Strings.New_String("Hello Ada."));
    Put_Line(Get_GL_String(GL_VENDOR));
    Put_Line(Get_GL_String(GL_RENDERER));
    Put_Line(Get_GL_String(GL_VERSION));
    Put_Line(Get_GL_String(GL_SHADING_LANGUAGE_VERSION));
    New_Line;

    if (Sogla.OpenGL.Initialize) then
        Put_Line("Initialized!");
        New_Line;

        if (glGetInteger64v = null) then
            Put_Line("ERROR: glGetInteger64v has NULL address !!! ");
            Put_Line("Check if your GPU support the OpenGL extension GL_ARB_sync.");

            return;
        end if;
    end if;

    glClearColor(0.3, 0.3, 0.3, 1.0);

    while (glfwGetWindowParam(GLFW_OPENED) = 1) loop
        glClear(GL_DEPTH_BUFFER_BIT or GL_COLOR_BUFFER_BIT);
        glGetInteger64v(GL_TIMESTAMP, Gpu_Time'Access);
        Put_Line("GPU time: " & Gpu_Time'Img);
        glfwSwapBuffers;
    end loop;


    Sogla.Log_IO.Finalize;
    glfwTerminate;
end opengl_gputime;

