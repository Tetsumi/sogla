--
-- Draw a white triangle on a blue background.
--
--
-- Author: Tetsumi <tetsumi@vmail.me>
--
--

with 
	Ada.Text_IO,
	Interfaces,
	Interfaces.C,
	Interfaces.C.Strings,
	Sogla.Log_IO,
	Sogla.Glfw,
	Sogla.OpenGL;

use 
	Ada.Text_IO,
	Interfaces,
	Interfaces.C,
	Sogla.Glfw,
	Sogla.OpenGL;

procedure opengl_triangle
is
	Vertex_Array_Id : aliased GLuint;
	Vertex_Buffer : aliased GLuint;
	Vertex_Buffer_Data : aliased array (0..2, 0..2) of GLfloat := ((-1.0, -1.0, 0.0),
                                                                       (1.0, -1.0, 0.0),
                                                                       (0.0,  1.0, 0.0));
begin
	Ada.Text_IO.Put_Line("Sogla v" & Sogla.Version_String);
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("---- TEST: Draw a white triangle on a blue background");

    if (glfwInit = 0) then
        Ada.Text_IO.Put_Line("glfwInit failed.");
        return;
    end if;

    Sogla.Log_IO.Initialize;

	if (glfwOpenWindow(800, 600, 8, 8, 8, 8, 24, 8, GLFW_WINDOW) = 0) then
		Ada.Text_IO.Put_Line("glfwOpenWindow failed.");
		return;
	end if;

	glfwSetWindowTitle(C.Strings.New_String("Hello Ada."));
	Put_Line(Get_GL_String(GL_VENDOR));
	Put_Line(Get_GL_String(GL_RENDERER));
	Put_Line(Get_GL_String(GL_VERSION));
	Put_Line(Get_GL_String(GL_SHADING_LANGUAGE_VERSION));
	New_Line;

	if (Sogla.OpenGL.Initialize) then
		Ada.Text_IO.Put_Line("OpenGL Initialized!");
		Ada.Text_IO.New_Line;
	end if;

	glClearColor(0.0, 0.0, 0.3, 1.0);
	glGenVertexArrays(1, Vertex_Array_Id'Access);
	glBindVertexArray(Vertex_Array_Id);
	glGenBuffers(1, Vertex_Buffer'Access);
	glBindBuffer(GL_ARRAY_BUFFER, Vertex_Buffer);

	glBufferData(GL_ARRAY_BUFFER,
				 Vertex_Buffer_Data'Size,
				 Vertex_Buffer_Data'Address,
				 GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL_VOID);

	while (glfwGetWindowParam(GLFW_OPENED) = 1) loop
		glClear(GL_DEPTH_BUFFER_BIT or GL_COLOR_BUFFER_BIT);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glfwSwapBuffers;
	end loop;

	Ada.Text_IO.Put_Line("Done.");
	Sogla.Log_IO.Finalize;
	glfwTerminate;
end opengl_triangle;
