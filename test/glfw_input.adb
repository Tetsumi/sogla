--
-- Test mouse & keyboard input.
--
--
-- Author: Tetsumi <tetsumi@vmail.me>
--
--

with 
	Ada.Text_IO,
	Interfaces.C,
	Interfaces.C.Strings,
	Sogla.Glfw,
	Sogla;

use 
	Interfaces,
	Interfaces.C,
	Sogla,
	Sogla.Glfw;

procedure glfw_input is
begin
    Ada.Text_IO.Put_Line("Sogla v" & Sogla.Version_String);
    Ada.Text_IO.New_Line;
	Ada.Text_IO.Put_Line("---- TEST: mouse & keyboard input");
	Ada.Text_IO.Put_Line("           Click mouse boutton or press H key.");

    if (glfwInit = 0) then
        Ada.Text_IO.Put_Line("glfwInit failed.");
    end if;

    if (glfwOpenWindow(800, 600, 8, 8, 8, 8, 24, 8, GLFW_WINDOW) = 0) then
        Ada.Text_IO.Put_Line("glfwOpenWindow failed.");
    end if;

    glfwSetWindowTitle(C.Strings.New_String("Hello Ada."));

    while (glfwGetWindowParam(GLFW_OPENED) = 1) loop
        glfwSwapBuffers;

        if (glfwGetKey(Character'Pos('H')) = GLFW_PRESS) then
            Ada.Text_IO.Put_Line("'H' key pressed!");
        end if;

        if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT) = GLFW_PRESS) then
            Ada.Text_IO.Put_Line("Left mouse button pressed!");
        end if;
    end loop;

    glfwTerminate;
end glfw_input;





