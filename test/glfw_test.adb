--
-- GLFW tests utilities.
--

with 
    Ada.Text_IO,
    Interfaces.C,
    Interfaces.C.Strings,
    Sogla.Glfw,
    System;

use 
    Interfaces,
    Interfaces.C,
    Sogla.Glfw;

package body Glfw_Test is

    -- keyboard input callback
    procedure Key_Callback (Key : C.Int;
                            Action : C.Int)
    is
    begin
        if (Action = GLFW_PRESS) then
            case Key is
            when 0 .. 256 =>
                Ada.Text_IO.Put_Line("ASCII Key pressed: char is " & Character'Val(Key));
            when others =>
                Ada.Text_IO.Put_Line("Unknown key pressed: id is" & C.Int'Image(Key));
            end case;
        end if;
    end Key_Callback;

    ----------------------------------------------------------------

    -- Mouse input callback
    procedure Mouse_Callback (Button : C.Int;
                              Action : C.Int)
    is
    begin
        case Action is
        when GLFW_PRESS =>
            Ada.Text_IO.Put_Line("mouse button pressed: id is " & C.Int'Image(Button));
        when GLFW_RELEASE =>
            Ada.Text_IO.Put_Line("mouse button released: id is " & C.Int'Image(Button));
        when others =>
                return;
        end case;
    end Mouse_Callback;

    ----------------------------------------------------------------

    -- Test Thread procedure
    procedure Hello_Thread_Proc (Arg : System.Address) is
    begin
        Ada.Text_IO.Put_Line("Hello from thread");
    end Hello_Thread_Proc;

end glfw_test;
