--
-- Test input with a callback.
--
--
-- Author: Tetsumi <tetsumi@vmail.me>
--
--

with 
    Ada.Text_IO,
    Interfaces.C,
    Interfaces.C.Strings,
    Sogla.Glfw,
    glfw_test;

use 
    Interfaces,
    Interfaces.C,
    Sogla.Glfw;

procedure glfw_inputcallback

is

begin
    Ada.Text_IO.Put_Line("Sogla v" & Sogla.Version_String);
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("---- TEST: input with a callback");
    Ada.Text_IO.Put_Line("           Click mouse boutton or press keyboard key.");

    if (glfwInit = 0) then
        Ada.Text_IO.Put_Line("glfwInit failed.");
    end if;

    if (glfwOpenWindow(800, 600, 8, 8, 8, 8, 24, 8, GLFW_WINDOW) = 0) then
        Ada.Text_IO.Put_Line("glfwOpenWindow failed.");
    end if;

    -- Callback must be set after glfwOpenWindow.
    glfwSetKeyCallback(Glfw_Test.Key_Callback'Access);
    glfwSetMouseButtonCallback(Glfw_Test.Mouse_Callback'Access);

    glfwSetWindowTitle(Interfaces.C.Strings.New_String("Hello Ada."));

    while (glfwGetWindowParam(GLFW_OPENED) = 1) loop
        glfwSwapBuffers;
    end loop;

    glfwTerminate;
end glfw_inputcallback;
