--
--
-- Ouput timestamp with glfwGetTime
--
--
-- Author: Tetsumi <tetsumi@vmail.me>
--
--

with 
    Ada.Text_IO,
    Interfaces.C,
    Interfaces.C.Strings,
    Sogla.Glfw;

use 
    Interfaces,
    Interfaces.C,
    Sogla.Glfw;

procedure glfw_time is
    Time        : Double;
    time_old    : Double := 0.0;
    Time_Delta  : Double;

    package Double_IO is new Ada.Text_IO.Float_IO(Double);
begin
    Ada.Text_IO.Put_Line("Sogla v" & Sogla.Version_String);
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("---- TEST: Ouput timestamp with glfwGetTime");

    if (glfwInit = 0) then
        Ada.Text_IO.Put_Line("glfwInit failed.");
    end if;

    if (glfwOpenWindow(800, 600, 8, 8, 8, 8, 24, 8, GLFW_WINDOW) = 0) then
        Ada.Text_IO.Put_Line("glfwOpenWindow failed.");
    end if;

    glfwSetWindowTitle(C.Strings.New_String("Hello Ada."));

    while (glfwGetWindowParam(GLFW_OPENED) = 1) loop
        Time := glfwGetTime;
        Time_Delta := Time - Time_Old;
        Time_Old := Time;

        Ada.Text_IO.Put("delta is");
        Double_IO.Put(Time_Delta, Exp => 0);
        Ada.Text_IO.New_Line;

        glfwSleep(0.5);
        glfwSwapBuffers;
    end loop;

    Ada.Text_IO.Put_Line("Done.");

    glfwTerminate;
end glfw_time;
