--
--
-- Clear the screen with a random color.
--
--
-- Author: Tetsumi <tetsumi@vmail.me>
--
--

with 
	Ada.Text_IO,
	Ada.Numerics.Float_Random,
	Interfaces.C,
	Interfaces.C.Strings,
	Sogla.Glfw,
	Sogla.Log_IO,
	Sogla.OpenGL;


use 
	Ada.Numerics,
	Interfaces,
	Interfaces.C,
	Sogla.Glfw,
	Sogla.OpenGL;

procedure opengl_randclrcolor is
	package Float_IO is new Ada.Text_IO.Float_IO(GLclampf);

	Seed  : Float_Random.Generator;
	Red   : GLclampf;
	Green : GLclampf;
	Blue  : GLclampf;

begin
	Ada.Text_IO.Put_Line("Sogla v" & Sogla.Version_String);
    Ada.Text_IO.New_Line;
    Ada.Text_IO.Put_Line("---- TEST: Clear the screen with a random color");

    if (glfwInit = 0) then
        Ada.Text_IO.Put_Line("glfwInit failed.");
        return;
    end if;

    Sogla.Log_IO.Initialize;

	if (glfwOpenWindow(800, 600, 8, 8, 8, 8, 24, 8, GLFW_WINDOW) = 0) then
		Ada.Text_IO.Put_Line("glfwOpenWindow failed.");
		return;
	end if;

	if (Sogla.OpenGL.Initialize) then
		Ada.Text_IO.Put_Line("OpenGL Initialized!");
		Ada.Text_IO.New_Line;
	end if;

	glfwSetWindowTitle(C.Strings.New_String("Hello Ada."));
	Ada.Numerics.Float_Random.Reset(Seed);

	while (glfwGetWindowParam(GLFW_OPENED) = 1) loop
		Red     :=  Float_Random.Random(Seed);
		Green   :=  Float_Random.Random(Seed);
		Blue    :=  Float_Random.Random(Seed);

		Ada.Text_IO.Put("Colors:");
		Float_IO.Put(Red, Exp => 0);
		Float_IO.Put(Green, Exp => 0);
		Float_IO.Put(Blue, Exp => 0);
		Ada.Text_IO.New_Line;
		glClearColor(Red, Green, Blue, 1.0);
		glClear(GL_DEPTH_BUFFER_BIT or GL_COLOR_BUFFER_BIT);
		glfwSwapBuffers;
		glfwSleep(0.250);
	end loop;

	Ada.Text_IO.Put_Line("Done.");
	Sogla.Log_IO.Finalize;
	glfwTerminate;
end opengl_randclrcolor;
