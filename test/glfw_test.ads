with Interfaces.C;
with Interfaces.C.Strings;
with System;

use Interfaces;
use Interfaces.C;

package glfw_test is
    procedure Key_Callback (Key : C.Int;
                            Action : C.Int);

    procedure Mouse_Callback (Button : C.Int;
                              Action : C.Int);

    procedure Hello_Thread_Proc (Arg : System.Address);
private
    pragma Convention(C, Key_Callback);
    pragma Convention(C, Mouse_Callback);
    pragma Convention(C, Hello_Thread_Proc);
end glfw_test;

